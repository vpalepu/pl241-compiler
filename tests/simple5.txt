main 

procedure foo(i); 
{ 
  while i < 10 do
    let i <- i + 1;
    call OutputNum(i * 90);
    call OutputNewLine()  
  od
};

{
  call foo(0-4)
}.