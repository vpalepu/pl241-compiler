package self.vpalepu.pl241;

public class PhiInstruction extends Instruction {

  private final int varId;
  private final Instruction originalDefInsn;
  
  public PhiInstruction(Opcodes op, Value a, Value b, int _varId) {
    super(op, a, b);
    varId = _varId;
    originalDefInsn = (Instruction) a;
  }
  
  public Instruction operand(BasicBlock b) {
    throw new UnsupportedOperationException();
  }
  
  public Instruction origDefInsn() {
    return originalDefInsn;
  }
  
  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append(super.toString())
    .append(" (").append(Scanner.get(null).Id2String(varId)).append(")");
    return str.toString();
  }
  
  public void updatePhi(Instruction varDefInsn) {
    if(this.ownerBlock().edge() == false) {
      this.operand1Is(varDefInsn);
    } else {
      this.operand2Is(varDefInsn);
    }
  }
  
  public int varId() {
    return varId;
  }
  
  public BasicBlock blockA() {
    BasicBlock owner = this.ownerBlock();
    return owner.previous().get(0);
  }
  
  public BasicBlock blockB() {
    BasicBlock owner = this.ownerBlock();
    return owner.previous().get(1);
  }
}
