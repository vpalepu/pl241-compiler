package self.vpalepu.pl241;

public enum Opcodes {
	
	NEG (1), // neg x unary minus  
	ADD (2), // add x y addition  
	SUB (3), // sub x y subtraction  
	MUL (4), // mul x y multiplication  
	DIV (5), // div x y division  
	CMP (6), // cmp x y comparison      
	ADDA (7), // adda x y add two addresses x und y (used only with arrays)  
	LOAD (8), // load y load from memory address y  
	STORE (9), // store y x store y to memory address x  
	MOVE (10), // move y x assign x := y  
	PHI (11), // phi x x1 x2 ... x := Phi(x1, x2, x3, ...) 
	END (12), // end end of program 
	BRA (13), // bra y branch to y  
	BNE (14), // bne x y branch to y on x not equal  
	BEQ (15), // beq x y branch to y on x equal  
	BLE (16), // ble x y branch to y on x less or equal  
	BLT (17), // blt x y branch to y on x less  
	BGE (18), // bge x y branch to y on x greater or equal  
	BGT (19), // bgt x y branch to y on x greater  
	READ (20), // read read  
	WRITE (21), // write write  
	WLN (22), // wln writeLn
	MU (23),
	NOP (24), // No operation
	RET (25),
	MUSTART (26),
	MUEND (27); 
	
	@SuppressWarnings("unused")
  private int op;
	private Opcodes(int _op) {
		this.op = _op;
	}
	
	
	public static Opcodes inverse(int cc) {
	  switch(cc) {
	  case Scanner.neqlToken:
      return BEQ; 
    case Scanner.eqlToken:
      return BNE;
    case Scanner.leqToken:
      return BGT;
    case Scanner.geqToken:
      return BLT;
    case Scanner.lssToken:
      return BGE;
    case Scanner.gtrToken:
      return BLE;
    default:
      throw new RuntimeException("does not make sense for " + cc);  
	  }
	}
}
