package self.vpalepu.pl241.utils;

public class Debug {
  


  public static class StackTraceHandler {

    public static final void printStackTrace(StackTraceElement[] stackTrace) {
      if(stackTrace == null || stackTrace.length == 0) {
        return;
      }

      StringBuffer buffer = new StringBuffer(); 
      
      for(StackTraceElement element : stackTrace) {
        if(element == null )  {
          buffer.append("null,");
          continue;
        }
        
        String className = element.getClassName();
        String[] classNameParts = className.split("\\.");
        String classLastName = classNameParts[classNameParts.length - 1];
        
        
        buffer.append(classLastName)
              .append("/")
              .append(element.getMethodName()).append(",");
      }
      
      System.out.println(buffer.toString());
    }

    public static final void printStackTrace() {
      printStackTrace(Thread.currentThread().getStackTrace());
    }

  }

}