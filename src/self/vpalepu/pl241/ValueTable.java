package self.vpalepu.pl241;

import static self.vpalepu.pl241.utils.Debug.StackTraceHandler.*;

import java.util.ArrayList;
import java.util.HashMap;

import static self.vpalepu.pl241.InstructionList.ZERO;

/**
 * Maintains a table between symbolIds and a list of Instructions, where the
 * respective symbol was defined or used.
 * @author vijay
 */

public class ValueTable {
  private HashMap<Integer, ValueList> table;
  
  private static ValueTable global;
  public static ValueTable valueTable() {
    if(global == null) global = new ValueTable();
    return global; 
  }
  
  private ValueTable() {
    table = new HashMap<Integer, ValueList>();
  }
  
//  public void put(int symbolId, Value insnId) {
//    if(!table.containsKey(symbolId)) {
//      table.put(symbolId, new ValueList(insnId));
//      return;
//    }
//    ValueList currentValue = table.get(symbolId);
//    ValueList newValue = new ValueList(insnId);
//    newValue.nextIs(currentValue);
//    table.put(symbolId, newValue);
//  }
  
  public void put(int symbolId, Value insnId, BasicBlock ownerBlock) {
    if(!table.containsKey(symbolId)) {
      NumberedValue numberedValue = new NumberedValue(insnId, ownerBlock);
      table.put(symbolId, new ValueList(numberedValue));
      return;
    }
    ValueList currentValue = table.get(symbolId);
    NumberedValue numberedValue = new NumberedValue(insnId, ownerBlock);
    ValueList newValue = new ValueList(numberedValue);
    newValue.nextIs(currentValue);
    table.put(symbolId, newValue);
  }
  
  public Value get(int symbolId, BasicBlock block) {
    while(block != null) {
      ValueList currentValue = table.get(symbolId);
      while(currentValue != null) {
        NumberedValue numberedValue = currentValue.numberedValue();
        if(numberedValue.ownerBlock == block) {
          return numberedValue.value;
        }
        currentValue = currentValue.next();
      }
      block = block.immDom();
    }
    
//    printStackTrace();
    
    return ZERO;
  }
  
//  public Value get(int symbolId) {
//    ValueList valueList = table.get(symbolId);
//    if(valueList == null) {
//      return null;
////      this.put(symbolId, ZERO);
//    }
//    
////    valueList = table.get(symbolId);
//    return valueList.insn();
//  }
  
//  public Instruction oldValueWithOwner(int symbolId, BasicBlock immDomBlock) {
//    while(immDomBlock != null) {
//      ValueList currentValue = table.get(symbolId);
//      ValueList valListPointer = currentValue;
//      while(valListPointer != null) {
//        Value pointerInsn = valListPointer.insn();
//        if(pointerInsn.getClass() == Instruction.class
//            && ((Instruction) pointerInsn).ownerBlock() == immDomBlock) {
//          return (Instruction) pointerInsn;
//        }
//        valListPointer = valListPointer.next();
//      }
//      immDomBlock = immDomBlock.immDom();
//    }
//    return ZERO;
//  }
  
  private static class ValueList {
    private NumberedValue numberedValue;
    private ValueList next;
    
    public ValueList(NumberedValue _numberedValue) {
      numberedValue = _numberedValue;
      next = null;
    }
    
    public NumberedValue numberedValue() {
      return numberedValue;
    }
    
    @SuppressWarnings("unused")
    public void valueIs(NumberedValue _numberedValue) {
      numberedValue = _numberedValue;
    }
    
    public ValueList next() {
      return next;
    }
    
    public void nextIs(ValueList _next) {
      next = _next;
    }
  }
  
  private static class NumberedValue {
    public final Value value;
    public final BasicBlock ownerBlock;
    
    public NumberedValue(Value _value, BasicBlock _block) {
      value = _value;
      ownerBlock = _block;   
    }
    
  }
}
