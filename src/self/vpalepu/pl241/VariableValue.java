package self.vpalepu.pl241;

import static self.vpalepu.pl241.InstructionList.ZERO;
import static self.vpalepu.pl241.ValueTable.valueTable;
import static self.vpalepu.pl241.SymbolTable.symbolTable;

public class VariableValue extends Value {

  private int value;
  private int valueNumber;
  
  public VariableValue(int _value) {
    value = _value;
    valueNumber = -1;
  }
  
  public VariableValue(int _value, int _valueNumber) {
    value = _value;
    valueNumber = _valueNumber;
  }
  
  public int valueNumber() {
    return valueNumber;
  }
  
  public void valueNumberIs(int _index) {
    valueNumber = _index;
  }
  
  @Override
  public int value() {
    return value;
  }

  @Override
  public void valueIs(int _value) {
    value = _value;
  }
  
  public Value getNumberedValue(BasicBlock ownerBlock) {
    Value numberedValue = valueTable().get(value, ownerBlock);
    if(numberedValue == null) {
      String errorMessage = symbolTable().get(value) + " is not defined.";
      Parser.throwCompilationError(errorMessage);
    }
    
    if(numberedValue == ZERO) {
      numberedValue = VariableZero.get(value);
    }
    
    return numberedValue;
  }
  
  public void setNumberedValue(Instruction insn, BasicBlock ownerBlock) {
    valueTable().put(value, insn, ownerBlock);
    this.valueNumberIs(insn.id());
  }

  @Override
  public Value combine(Opcodes op, Value a, BasicBlock ownerBlock) {
    Instruction insn = new Instruction(op, this, a, ownerBlock);
    
    if(op == Opcodes.MOVE) {
      ((VariableValue) a).setNumberedValue(insn, ownerBlock);
    } else {
      if(a != null && a.getClass() == VariableValue.class) {
        Value numValue = ((VariableValue) a).getNumberedValue(ownerBlock);
        if(numValue.getClass() == VariableZero.class) {
          insn.zeroSymId(2, VariableZero.id(numValue));
          numValue = ZERO;
        }
        insn.operand2Is(numValue);
      }
    }
    
    Value numValue = this.getNumberedValue(ownerBlock);
    if(numValue.getClass() == VariableZero.class) {
      insn.zeroSymId(1, VariableZero.id(numValue));
      numValue = ZERO;
    }
    insn.operand1Is(numValue);
       
    AuxillaryProcs.addInsn(insn);
    insn.pointTo(ownerBlock);
    return insn;
  }
  
  @Override
  public String toString() {
    if(valueNumber == -1) {
      //System.err.println(Scanner.get(null).Id2String(value));
    }
    return "var " + Scanner.get(null).Id2String(value) + "." + valueNumber;
  }


}
