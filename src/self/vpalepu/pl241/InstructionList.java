package self.vpalepu.pl241;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.base.Function;

public class InstructionList {
  
  private static InstructionList list = null;
  public static InstructionList singleton() {
    if(list == null) {
      list = new InstructionList();
    }
    return list;
  }
  
  public static InstructionList newlist() {
    return new InstructionList();
  }
 
  public static final Instruction ZERO = new Instruction(null, null, null);
  
  private Instruction last;
  private Instruction head;
  
  private InstructionList() {
    last = ZERO;
    head = ZERO;
  }
  
  public void addToFront(Instruction insn) {
    if(head == ZERO) {
      checkState(last == ZERO);
      head = insn;
      last = insn;
      return;
    }
    insn.nextIs(head);
    head = insn;
  }
  
  public void addToLast(Instruction insn) {
    if(head == ZERO) {
      checkState(last == ZERO);
      head = insn;
      last = insn;
      return;
    }
    last.nextIs(insn);
    lastIs(insn);
  }
  
  public void addInsnBefore(Instruction insn, Instruction before) {
    if(insn == null) {
      return;
    }
    
    Instruction beforePrevious = before.previous();
    insn.nextIs(before);
    insn.previousIs(beforePrevious);
    updateHead();
    updateLast();
  }
  
  public void addInsnAfter(Instruction insn, Instruction after) {
    if(insn == null) {
      return;
    }
    
    Instruction afterNext = after.next();
    insn.previousIs(after);
    insn.nextIs(afterNext);
    updateHead();
    updateLast();
  }
  
    private void updateHead() {
      Instruction pointer = head;
      while(pointer.previous() != null) {
        pointer = pointer.previous();
      }
      head = pointer;
    }
    
    private void updateLast() {
      Instruction pointer = last;
      while(pointer.next() != null) {
        pointer = pointer.next();
      }
      last = pointer;
    }
  
  public boolean contains(Instruction insn) {
    Instruction pointer = head;
    while(pointer != null) {
      if(pointer == insn) return true;
      pointer = pointer.next();
    }
    return false;
  }
  
  public Instruction get(int id) {
    Instruction pointer = head;
    while(pointer != null) {
      if(pointer.id() == id) {
        return pointer;
      }
      pointer = pointer.next();
    }
    return null;
  }
  
  public Instruction head() {
    return head;
  }
  
  public Instruction last() {
    return last;
  }
  
  public void lastIs(Instruction _last) {
    last = _last;
  }
  
  public boolean isEmpty() {
    if(head == ZERO) return true;
    return false;
  }
  
  public void remove(Instruction insn) {
    if(insn == null) return;
    if(head == insn) head = insn.next();
    if(head == null) head = ZERO;
    if(last == insn) last = insn.previous();
    if(last == null) last = ZERO;
    insn.delete();
  }

  public String traverseAnd(Function<Instruction, String> doSomething) {
    StringBuffer str = new StringBuffer();
    Instruction pointer = head;
    while(pointer != null) {
      str.append(doSomething.apply(pointer));
      pointer = pointer.next();
    }
    return str.toString();
  }
  
  public String traverseAndPrint() {
    StringBuffer str = new StringBuffer();
    Instruction pointer = head;
    while(pointer != null) {
      str.append(pointer.toString()).append("\n");
      pointer = pointer.next();
    }
    return str.toString();
  }
}