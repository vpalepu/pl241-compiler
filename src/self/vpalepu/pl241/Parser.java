package self.vpalepu.pl241;

import static self.vpalepu.pl241.InstructionList.ZERO;
import static self.vpalepu.pl241.SymbolTable.symbolTable;
import static self.vpalepu.pl241.ValueTable.valueTable;
import static self.vpalepu.pl241.BasicBlock.currentBlock;
import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import self.vpalepu.pl241.Registers.RegisterValue;
import self.vpalepu.pl241.SymbolTable.Symbol;
import self.vpalepu.pl241.SymbolTable.SymbolType;
import self.vpalepu.pl241.runtime.ActivationRecord;


public class Parser {
	
	public static boolean debug = false;
	private Scanner scanner;
	private int token;
	
	public Parser(Scanner scanner) {
		this.scanner = scanner;
		this.token = Scanner.errorToken;
	}
	
	public void parse() {
		nextToken();
	}
	
	public Scanner scanner() {
	  return scanner;
	}
	
	private void nextToken() {
		do {
			scanner.next();
			token = scanner.getSym();
		} while(token == Scanner.errorToken);
		if(debug) {
			System.err.println(scanner.printToken());
		}
	}
	
	private int latestDesigId = -1;
	
	// designator = ident{ "[" expression "]" }
	private Value designator(long id) {
	  String scope = FlowGraph.current().name();
	  Ident desig = ident(designator + id, "designator starts with an identifier.");
	  String desigName = desig.name;
	  
	  SymbolType symbolType = SymbolType.VAR;
    if(this.token == Scanner.openbracketToken) {
      symbolType = SymbolType.ARR;
    }
	      
	  Symbol symbol = symbolTable().fetch(scope, desigName, symbolType);
	      
    if(symbol == null) {
      throwCompilationError(symbolType + " " + desigName + " not declared in " + scope);
    }
		
    boolean isDesignatorGlobal = symbol.scope().equals(SymbolTable.MAIN);
    int dId = symbol.id();
    
    Value numberedValue = valueTable().get(dId, currentBlock());  
    if((symbol.isParam()
        || (isDesignatorGlobal 
            && symbolType == SymbolType.VAR 
            && !FlowGraph.current().name().equals(SymbolTable.MAIN))) 
        
        && numberedValue == ZERO) {
      ActivationRecord stackFrame = isDesignatorGlobal ? 
                                    FlowGraph.get(SymbolTable.MAIN).activation() 
                                    : FlowGraph.current().activation();
      int offset = stackFrame.paramOffset(dId);
      BasicBlock entry = FlowGraph.current().entryBlock();
      Instruction[] loadInsns = 
          Instruction.load(symbol.baseAddress(), offset * -1, entry);
      placeLoad(FlowGraph.current(), loadInsns);
//      AuxillaryProcs.addInsn(loadInsns);
      Instruction load = loadInsns[loadInsns.length - 1];
      valueTable().put(dId, load, load.ownerBlock());
    }
    
		VariableValue x = new VariableValue(dId);
		if(symbolType == SymbolType.VAR) {
		  return x;
		}
		
//		if(symbolType == SymbolType.VAR && isDesignatorGlobal) {
//		  ActivationRecord mainFrame = FlowGraph.get(SymbolTable.MAIN).activation();
//		  int gOffset = mainFrame.paramOffset(dId);
//		  ConstantValue offset = new ConstantValue(gOffset * -4);
//		  Instruction globalAddress = new Instruction(Opcodes.ADD, 
//		                                              Registers.GP(), 
//		                                              offset);
//		  AuxillaryProcs.addInsn(globalAddress);
//		  return globalAddress;
//		}
		
		ArrayList<Value> indicies = new ArrayList<>();
		while(terminal(Scanner.openbracketToken, designator+id)) {
			dotEmmit(designator+id, expression + ++nodeCounter);
			Value y = expression(nodeCounter);
			Instruction yInsn = Parser.value2Insn(y, currentBlock());
			if(y.getClass() == VariableValue.class && yInsn == ZERO) {
			  yInsn = VariableZero.get(y.value());
			}
			indicies.add(yInsn);
			terminal(Scanner.closebracketToken, designator+id, "designator missing closing bracket.");
		}
		
		ActivationRecord stackFrame = isDesignatorGlobal ? 
		                              FlowGraph.get(SymbolTable.MAIN).activation() 
		                              : FlowGraph.current().activation();
		ArrayList<Instruction> addaSeq = 
		    Instruction.loadArray(symbol.baseAddress(), 
                              stackFrame.offset(dId), 
                              BasicBlock.currentBlock(), 
                              indicies,
                              symbol);
		Instruction[] arrayAddaSeq = addaSeq.toArray(new Instruction[0]);
		AuxillaryProcs.addInsn(arrayAddaSeq);
//		if(symbol.scope() == SymbolTable.MAIN) {
//		  return ZERO.combine(Opcodes.MUL, new ConstantValue(4)); // TODO why?
//		}
		latestDesigId = dId;
		return arrayAddaSeq[arrayAddaSeq.length - 1];
	}
	
	  private void placeLoad(FlowGraph flow, Instruction[] loadInsns) {
	    BasicBlock entry = flow.entryBlock();
	    InstructionList instructions = entry.instructions();
	    Instruction muStart = flow.muStart();
	    if(muStart == null) {
	      Instruction pointer = loadInsns[0];
	      instructions.addToFront(pointer);
	      for(int i = 1; i < loadInsns.length; i += 1) {
	        instructions.addInsnAfter(loadInsns[i], pointer);
	        pointer = loadInsns[i];
	      }
	    } else {
	      Instruction pointer = muStart;
	      for(Instruction insn : loadInsns) {
	        instructions.addInsnAfter(insn, pointer);
	        pointer = insn;
	      }
	    }
	  }
	  
	  private static Instruction value2Insn(Value value, BasicBlock ownerBlock) {
	    Class<?> valueClass = value.getClass();
	    if(valueClass == Instruction.class) {
	      return (Instruction) value;
	    } else if(valueClass == VariableValue.class) {
	      Instruction valueInsn = (Instruction) valueTable().get(value.value(), ownerBlock);
	      return valueInsn;
	    } else if(valueClass == ConstantValue.class && value.value() == 0) { 
	      return ZERO;
	    } else {
	      return (Instruction) ZERO.combine(Opcodes.ADD, value, ownerBlock);
	    }
	  }
	
	/**
	 * expression = term {(“+” | “-”) term}.
	 * @param id
	 * @return
	 */
	private Value expression(long id) {
	  Value x, y;
		
		nodeCounter++;
		dotEmmit(expression+id, term+nodeCounter);
		x = term(nodeCounter);
		
		while(this.token == Scanner.plusToken 
				|| this.token == Scanner.minusToken) {
			boolean plus = false;
			if(this.token == Scanner.plusToken) {
				plus = true;
			} // else plus = false;
			
			dotEmmit(expression+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
			
			nodeCounter++;
			dotEmmit(expression+id, term+nodeCounter);
			y = term(nodeCounter);
			
			if(plus) {
				x = x.combine(Opcodes.ADD, y, currentBlock());
			} else {
				x = x.combine(Opcodes.SUB, y, currentBlock());
			}
		}
		return x;
	}
	
	/**
	 * term = factor { (“*” | “/”) factor}.
	 * @param id
	 * @return
	 */
	private Value term(long id) {
	  Value x, y;
		
		nodeCounter++;
		dotEmmit(term+id, factor+nodeCounter);
		x = factor(nodeCounter);
		
		while(this.token == Scanner.timesToken 
				|| this.token == Scanner.divToken) {
			boolean times = false;
			if(this.token == Scanner.timesToken) {
				times = true;
			} // else times = false;
			
			dotEmmit(term+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
			
			nodeCounter++;
			dotEmmit(term+id, factor+nodeCounter);
			y = factor(nodeCounter);
			
			if(times) {
				x = x.combine(Opcodes.MUL, y, currentBlock());
			}
			else {
				x = x.combine(Opcodes.DIV, y, currentBlock());
			}
		}
		return x;
	}

	/**
	 * factor = designator | number | “(” expression “)” | funcCall .
	 * @param id
	 * @return
	 */
	private Value factor(long id) {
	  Value x = null;
		switch(this.token) {
		case Scanner.number:
			x = new ConstantValue(scanner.latestVal);
			dotEmmit(factor+id, ++nodeCounter+":"+scanner.latestVal);
			nextToken();
			break;
		case Scanner.openparenToken:
			dotEmmit(factor+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
			dotEmmit(factor+id, expression + ++nodeCounter);
			x = expression(nodeCounter);
			if(token == Scanner.closeparenToken) {
				dotEmmit(factor+id, ++nodeCounter + ":" + scanner.Id2String(token));
				nextToken();
			} else {
				throwSyntaxError("missing closing parenthesis.");
			}
			break;
		case Scanner.callToken:
			dotEmmit(factor+id, funcCall+ ++nodeCounter);
			x = funcCall(nodeCounter);
			break;
		case Scanner.ident:
			dotEmmit(factor+id, designator + ++nodeCounter);
			String identifier = scanner.Id2String(scanner.latestId);
			if(identifier.equals(Parser.read)) {
			  throwSyntaxError("use `call` keyword to invoke InputNum");
			}
			
			x = designator(nodeCounter);
			if(x.getClass() == VariableValue.class) {
			  int xId = x.value();
			  Value designatorValue = valueTable().get(xId, currentBlock());
			  if(designatorValue == null) {
          throwCompilationError(symbolTable().get(xId) + " not defined.");
        }
			} else if(x.getClass() == Instruction.class) {
			  x = x.combine(Opcodes.LOAD, ZERO, currentBlock());
			  ((Instruction) x).loadStoreSymIs(latestDesigId);
			}
			break;
		default:
			throwSyntaxError("factor not right.");
		}
		return x;
	}

	
	/**
	 * relation = expression relOp expression .
	 * @param id
	 * @return
	 */
	private Relation relation(long id, BasicBlock loopHeader) {
		Value x, y;
		
		nodeCounter++;
		dotEmmit(relation+id, expression+nodeCounter);
		x = preProcess(expression(nodeCounter));
		int relationToken = token;
		switch(token) {
		case Scanner.geqToken:
		case Scanner.gtrToken:
		case Scanner.leqToken:
		case Scanner.lssToken:
		case Scanner.eqlToken:
		case Scanner.neqlToken:
			dotEmmit(relation+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
			break;
		default:
			throwSyntaxError("invalid relOp.");
		}
		
		nodeCounter++;
		dotEmmit(relation+id, expression+nodeCounter);
		y = expression(nodeCounter);
		
		Value compareValue = null;
		if(loopHeader != null) {
		  BasicBlock.currentBlockIs(loopHeader);
		}
		
		
		if(x.getClass() == VariableZero.class) {
		  int varZeroXId = VariableZero.id(x);
		  x = ZERO;
		  compareValue = ZERO.combine(Opcodes.CMP, y, currentBlock());
		  ((Instruction) compareValue).zeroSymId(1, varZeroXId);
		} else {
		  compareValue = x.combine(Opcodes.CMP, y, currentBlock());
		}
		
		
		
		return new Relation(compareValue, relationToken);
	}
	
	private Instruction inputNum(long id) {
	  String identifier = scanner.Id2String(scanner.latestId);
	  if(!identifier.equals(Parser.read)) {
      throwSyntaxError("InputNum func has the wrong identifier:" + identifier);
    }
	  
	  terminal(Scanner.openparenToken, funcCall + id, "InputNum func call missing opening parenthesis.");
	  
	  terminal(Scanner.closeparenToken, funcCall + id, "InputNum func call missing closing parenthesis.");
	  
	  Instruction read = new Instruction(Opcodes.READ, null, null);
	  AuxillaryProcs.addInsn(read);
	  return read;
	}
	
	private Instruction outputNum(long id) {
    String identifier = scanner.Id2String(scanner.latestId);
    if(!identifier.equals(Parser.write)) {
      throwSyntaxError("OutputNum func has the wrong identifier:" + identifier);
    }
    
    terminal(Scanner.openparenToken, funcCall+id, "OutputNum func call missing opening parenthesis.");
    
    dotEmmit(funcCall+id, expression+ ++nodeCounter);
    Value expression = preProcess(expression(nodeCounter));
    
    if(expression.getClass() == VariableZero.class) {
      expression = ZERO;
    }
    
    terminal(Scanner.closeparenToken, funcCall+id, "OutputNum func call missing closing parenthesis.");
    
    Instruction write = 
        (Instruction) expression.combine(Opcodes.WRITE, null, currentBlock());
    return write;
	}
	
	private Instruction outputNewLine(long id) {
    String identifier = scanner.Id2String(scanner.latestId);
    if(!identifier.equals(Parser.writeln)) {
      throwSyntaxError("OutputNewLine func has the wrong identifier:" + identifier);
    }
    
    if(token == Scanner.openparenToken) {
      dotEmmit(funcCall+id, ++nodeCounter+":"+scanner.Id2String(token));
      nextToken();
    } else {
      throwSyntaxError("OutputNewLine func call missing opening parenthesis.");
    }
    
    if(token == Scanner.closeparenToken) {
      dotEmmit(funcCall+id, ++nodeCounter+":"+scanner.Id2String(token));
      nextToken();
    } else {
      throwSyntaxError("OutputNewLine func call missing closing parenthesis.");
    }
    
    Instruction writeln = new Instruction(Opcodes.WLN, null, null);
    AuxillaryProcs.addInsn(writeln);
    return writeln;
  }

	private Value funcCall(long id) {
		terminal(Scanner.callToken, funcCall+id, "func call starts with call keyword.");
		
		Ident funcIdent = ident(funcCall+id, "func call missing identifier.");
		String funcName = funcIdent.name;
		
		Value predefinedCallValue = predefinedCall(funcName);
		if(predefinedCallValue != null)  {
		  return predefinedCallValue;
		}
		
		
		SymbolTable table = symbolTable();
		if(!table.contains(funcName, SymbolType.FUNC, SymbolTable.MAIN)
		    && !table.contains(funcName, SymbolType.PROC, SymbolTable.MAIN)) {
		  throwCompilationError("func/proc " + funcName + ", being invoked needs "
		                        + "to be declared and defined first.");
		}
		
		
		if(terminal(Scanner.openparenToken, funcCall+id)) {
		  //  TODO: check if i need this... my guess is that i do not.
		  //  a simple integrated test with a real function 
		  //  call compilation should be enough to test it.
		  //		  nodeCounter++;
		  //			dotEmmit(funcCall+id, nodeCounter+":"+scanner.Id2String(token));
		  //			nextToken();
			
		  ArrayList<Value> paramValues = new ArrayList<>();
			boolean expressionAvailable = false;
			if(this.token != Scanner.closeparenToken) {
				parseFuncParams(paramValues, id);
				expressionAvailable = true;
			}
			
			if(expressionAvailable) {
			  while(terminal(Scanner.commaToken, funcCall+id)) {
					parseFuncParams(paramValues, id);
				}
			}
			
			int paramOffset = 1;
			BasicBlock current = BasicBlock.currentBlock();
			RegisterValue scratch = Registers.SCRATCH3();
			for(Value param : paramValues) {
			  ConstantValue offset = new ConstantValue(-4 * paramOffset);
			  Instruction[] loadParamSeq = 
			      Instruction.store(Registers.SP(), offset, param, current, scratch);
			  AuxillaryProcs.addInsn(loadParamSeq);
			  paramOffset += 1;
			}
			
			terminal(Scanner.closeparenToken, funcCall+id, "func call missing closing parenthesis.");
		}
		
		String callerFunctionName = FlowGraph.current().name();
		MuInstruction funcCall = new MuInstruction(funcName, callerFunctionName);
		AuxillaryProcs.addInsn(funcCall);
		
		mu1(funcCall, FlowGraph.current());
		mu2(funcCall);

		BasicBlock currentBlock = BasicBlock.currentBlock();
		BasicBlock newBlock = BasicBlock.createNew();
		currentBlock.addNext(newBlock);
		BasicBlock.currentBlockIs(newBlock);
		
		return Registers.RET();
	}
	
    	private void mu1(MuInstruction mu, FlowGraph currentFlow) {
        ArrayList<Symbol> defdGlobals = currentFlow.getDefinedGlobals(); 
        ArrayList<Instruction> storeGlobals =
            FlowGraph.storeGlobals(defdGlobals, mu.ownerBlock());
        for(Instruction store : storeGlobals) {
          AuxillaryProcs.addInsnBefore(store, mu);
        }
      }
    	
    	private void mu2(MuInstruction mu) {
    	  FlowGraph mainFlow = FlowGraph.get(SymbolTable.MAIN);
    	  BasicBlock muBlock = mu.ownerBlock();
        ActivationRecord frame = mainFlow.activation();
        ArrayList<Symbol> globals = frame.getParams();

        HashMap<Symbol, Instruction[]> loadGlobals =
            FlowGraph.loadGlobals(globals, mu.ownerBlock());
        Instruction after = mu;
        for(Symbol global : loadGlobals.keySet()) {
          Instruction[] loads = loadGlobals.get(global);
          for(Instruction load : loads) {
            AuxillaryProcs.addInsnAfter(load, after);
            after = load;
          }
          
          Instruction load = loads[loads.length - 1];
          int gId = global.id();
          load.loadStoreSymIs(gId);
          valueTable().put(gId, load, muBlock);
        }
      }

      private void parseFuncParams(ArrayList<Value> paramValues, long id) {
        dotEmmit(funcCall+id, expression + ++nodeCounter);
        Value x = preProcess(expression(nodeCounter));
                
        checkState(x.getClass() == Instruction.class 
                    || x.getClass()  == RegisterValue.class
                    || x.getClass() == VariableZero.class,
                    "funcCall parameter. expecting either " 
                    + "constant or instruction in IR; instead got: " 
                        + x.toString());
        paramValues.add(x);
      }
	
	private void assignment(long id) {
	  
	  terminal(Scanner.letToken, assignment+id, "assignment starting w/o let keyword.");
		
		dotEmmit(assignment+id, designator+ ++nodeCounter);
		Value designator = designator(nodeCounter);
		
		terminal(Scanner.becomeToken, assignment+id, "becomeToken missing after designator in assignment");
		
		dotEmmit(assignment+id, expression+ ++nodeCounter);
		Value expression = expression(nodeCounter);
		
		if(designator.toString().equals(expression.toString())) { // let a <- a;
		  return;
		}
		
		if(designator.getClass() == Instruction.class) {
		  Instruction store;
		  if(expression.getClass() == ConstantValue.class) {
		    Instruction constant = 
		        (Instruction) ZERO.combine(Opcodes.ADD, expression, currentBlock());
		    store = new Instruction(Opcodes.STORE, constant, designator);
	      store.resultIs(Registers.zero());
	      AuxillaryProcs.addInsn(store);
		  } else {
		    store = (Instruction) expression.combine(Opcodes.STORE, designator, currentBlock());
	      store.resultIs(Registers.zero());
		  }
		  
		  store.loadStoreSymIs(latestDesigId);
		  
		  BasicBlock join = BasicBlock.currentBlock().joinBlock();
		  if(join != null) {
		    InstructionList insns = join.instructions();
		    Instruction kill = new Instruction(Opcodes.STORE, null, null, join);
		    kill.loadStoreSymIs(latestDesigId);
		    insns.addToFront(kill);
		  }

		  return;
		}
		
		int symbolId = designator.value();
		Symbol symbol = symbolTable().get(symbolId);
		if(symbol.isGlobal()) {
		  FlowGraph currentFlow = FlowGraph.current(); 
		  currentFlow.addDefinedGlobal(symbol);
		}
		
		
//		BasicBlock immCurrentDom = BasicBlock.currentBlock().immDom();
		Value designatorOldValue = valueTable().get(designator.value(), currentBlock());
		if(designatorOldValue == ZERO) {
		  designatorOldValue = VariableZero.get(symbolId);
		}
		Instruction move = 
		    (Instruction) expression.combine(Opcodes.MOVE, designator, currentBlock());
		
		BasicBlock join = BasicBlock.currentBlock().joinBlock();
		PhiInstruction phi = null;
		if(join != null) {
		  phi = join.getPhi(move, designatorOldValue);
		  phi.updatePhi(move);
		} 
		
	}

	private void ifStatement(long id) {
	  BasicBlock branchBlock = BasicBlock.currentBlock();
	  BasicBlock currentJoinBlock = BasicBlock.createNew();
	  BasicBlock previousJoinBlock = branchBlock.joinBlock();
	  currentJoinBlock.joinAndImmDomAre(previousJoinBlock, branchBlock);
	  
    BasicBlock trueBlock = null;
    BasicBlock falseBlock = null;
	  
	  if(token == Scanner.ifToken) {
			dotEmmit(ifStatement+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken(); 
		} else {
			throwSyntaxError("if statement starting w/o if keyword.");
		}
		
		dotEmmit(ifStatement+id, relation + ++nodeCounter);
		Relation relation = relation(nodeCounter, null);
		Opcodes branchOpcode = Opcodes.inverse(relation.ccCode());
    Instruction on = branchBlock.instructions().last();
    Instruction to = null;
		Instruction branchInsn = 
		    (Instruction) on.combine(branchOpcode, to, currentBlock());
		
		if(token == Scanner.thenToken) {
			dotEmmit(ifStatement+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
		} else {
			throwSyntaxError("if statement missing then keyword.");
		}
		
		dotEmmit(ifStatement+id, statSequence+ ++nodeCounter);
		trueBlock = BasicBlock.createNew();
		BasicBlock.currentBlockIs(trueBlock);
		trueBlock.joinAndImmDomAre(currentJoinBlock, branchBlock);
		branchBlock.addNext(trueBlock);
		
		statSequence(nodeCounter, currentJoinBlock);
		
		BasicBlock.currentBlock().addNext(currentJoinBlock);
		Instruction uBranchInsn = 
		    (Instruction) ZERO.combine(Opcodes.BRA, null, currentBlock());
		
		if(token == Scanner.elseToken) {
			dotEmmit(ifStatement+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
			
			dotEmmit(ifStatement+id, statSequence+ ++nodeCounter);
			falseBlock = BasicBlock.createNew();
			BasicBlock.currentBlockIs(falseBlock);
			falseBlock.joinAndImmDomAre(currentJoinBlock, branchBlock);
			branchBlock.addNext(falseBlock);
			
			currentJoinBlock.edgeIs(true);
//			for(PhiInstruction phi : currentJoinBlock.phis()) {
//			  Instruction origDef = phi.origDefInsn();
//			  ValueTable.global().put(phi.varId(), origDef);
//			}
			
			statSequence(nodeCounter, currentJoinBlock);
			
			falseBlock.instructions().addToFront(new Instruction(Opcodes.NOP, null, null, falseBlock));
			if(branchInsn.operand2() == null)
        branchInsn.operand2Is(falseBlock.instructions().head());
			
			BasicBlock.currentBlock().addNext(currentJoinBlock);
		}
		
		if(token == Scanner.fiToken) {
		  BasicBlock.currentBlockIs(currentJoinBlock);
		  BasicBlock joinBlockJoin = currentJoinBlock.joinBlock();
		  propogatePhisToOuterJoin(joinBlockJoin, currentJoinBlock.phis());
		  
		  currentJoinBlock.instructions().addToFront(new Instruction(Opcodes.NOP, null, null, currentJoinBlock));
		  uBranchInsn.operand1Is(currentJoinBlock.instructions().head());
		  if(branchInsn.operand2() == null) {
		    branchInsn.operand2Is(currentJoinBlock.instructions().head());
		    branchBlock.addNext(currentJoinBlock);
		  }
			dotEmmit(ifStatement+id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken(); 
		} else {
			throwSyntaxError("if statement ending w/ fi keyword.");
		}
	}
	
	private void whileStatement(long id) {
	  BasicBlock loopBodyBlock = BasicBlock.createNew();
	  BasicBlock loopHeaderBlock = BasicBlock.createNew();
		
	  terminal(Scanner.whileToken, whileStatement+id, "while statement starting w/o while keyword.");

	  { 
	    BasicBlock currentBlock = BasicBlock.currentBlock();
      currentBlock.addNext(loopHeaderBlock);
      loopHeaderBlock.joinAndImmDomAre(currentBlock.joinBlock(), currentBlock);
    }

		dotEmmit(whileStatement+id, relation+ ++nodeCounter);		
		Relation relation = relation(nodeCounter, loopHeaderBlock);
		BasicBlock.currentBlockIs(loopHeaderBlock);
		
		Opcodes branchOp = Opcodes.inverse(relation.ccCode());
		Value relationValue = relation.value();
		Instruction to = null;
		Instruction branchInsn = 
		    (Instruction) relationValue.combine(branchOp, to, currentBlock());
		
		loopHeaderBlock.edgeIs(true);
		
		terminal(Scanner.doToken, whileStatement+id, "while statement missing do keyword.");
		{
		  loopHeaderBlock.addNext(loopBodyBlock);
		  loopBodyBlock.joinAndImmDomAre(loopHeaderBlock, loopHeaderBlock);
		  BasicBlock.currentBlockIs(loopBodyBlock);
		}
		
		dotEmmit(whileStatement+id, statSequence+ ++nodeCounter);
		statSequence(nodeCounter, loopHeaderBlock);
		loopHeaderBlock.instructions().addToFront(new Instruction(Opcodes.NOP, null, null, loopHeaderBlock));
		Instruction target = loopHeaderBlock.instructions().head();
		target.combine(Opcodes.BRA, null, currentBlock());
		BasicBlock statSeqLastBlock = BasicBlock.currentBlock();
		
		statSeqLastBlock.addNext(loopHeaderBlock);
		
		terminal(Scanner.odToken, whileStatement+id, "while statement missing od keyword.");
		
		ArrayList<PhiInstruction> loopPhis = loopHeaderBlock.phis();
		if(relationValue instanceof Instruction) {
		  Instruction compareInsn = (Instruction) relationValue;
		  for(PhiInstruction phi : loopPhis) {
		    if(phi.operand1() == compareInsn.operand1()) {
		      compareInsn.operand1Is(phi);
		      if(phi.operand1() == compareInsn.operand2()) {
	          compareInsn.operand2Is(phi);
	        }
		      continue;
		    }
		    if(phi.operand1() == compareInsn.operand2()) {
		      compareInsn.operand2Is(phi);
		      if(phi.operand1() == compareInsn.operand1()) {
	          compareInsn.operand1Is(phi);
	        }
		      continue;
		    }
		  }
		}
		
		updateForPhiDownStream(loopBodyBlock, loopHeaderBlock, loopPhis);
		for(PhiInstruction phi : loopPhis) {
		  ValueTable.valueTable().put(phi.varId(), phi, phi.ownerBlock());
		}
		
		BasicBlock joinBlockJoin = loopHeaderBlock.joinBlock();
		propogatePhisToOuterJoin(joinBlockJoin, loopPhis);
		
		BasicBlock follow = BasicBlock.createNew();
		
		loopHeaderBlock.addNext(follow);
		
		follow.joinAndImmDomAre(joinBlockJoin, loopHeaderBlock);
		Instruction nop = new Instruction(Opcodes.NOP, null, null, follow);
		follow.instructions().addToFront(nop);
		
		branchInsn.operand2Is(follow.instructions().head());
		BasicBlock.currentBlockIs(follow);
	}
	
    	private void updateForPhiDownStream(BasicBlock start, BasicBlock stop, 
    	    ArrayList<PhiInstruction> phis) {
    	  HashMap<Instruction, PhiInstruction> map = new HashMap<>();
    	  HashMap<Integer, PhiInstruction> zeroMap = new HashMap<>();
    	  HashMap<Integer, PhiInstruction> varPhiMap = new HashMap<>();
    	  for(PhiInstruction phi : phis) {
    	    varPhiMap.put(phi.varId(), phi);
    	    Value oper1 = phi.operand1(); 
    	    if(!(oper1 instanceof Instruction)) {
    	      throw new RuntimeException("phi operand is not an instruction. @ " 
    	          + scanner.getCurrentLine());
    	    }
    	    
    	    Instruction phiOperand1 = (Instruction) oper1;
    	    
    	    if(phiOperand1 == ZERO) {
            continue;
          }
    	    
    	    if(phiOperand1.getClass() == VariableZero.class) {
    	      int id = ((VariableZero) phiOperand1).variableId();
    	      zeroMap.put(id, phi);
    	    }
    	    
    	    map.put(phiOperand1, phi);
    	  }
    	  
    	  ArrayList<BasicBlock> visited = new ArrayList<BasicBlock>();
        Stack<BasicBlock> blocks = new Stack<BasicBlock>();
        blocks.add(start);
        visited.add(stop);
        while(!blocks.isEmpty()) {
          BasicBlock block = blocks.pop();
          visited.add(block);
          Instruction insn = block.instructions().head();
          while(insn != null) {
            Value operand1 = insn.operand1();
            Value operand2 = insn.operand2();
            
            if(operand1 == ZERO) {
              int zeroSymId = insn.zeroSymId(1);
              if(zeroSymId > 0) {
                VariableZero zero = VariableZero.get(zeroSymId);
                insn.operand1Is(map.get(zero));
              } // else operand1 continues to be zero.
            } else if(map.keySet().contains(operand1)) {
              insn.operand1Is(map.get(operand1));
            }
            
            if(operand1 == ZERO) {
              int zeroSymId = insn.zeroSymId(2);
              if(zeroSymId > 0) {
                VariableZero zero = VariableZero.get(zeroSymId);
                insn.operand2Is(map.get(zero));
              } // else operand2 continues to be zero.
            } else if(map.keySet().contains(operand2)) {
              insn.operand2Is(map.get(operand2));
            }
            
            if(insn.getClass() == PhiInstruction.class) {
              PhiInstruction phi = (PhiInstruction) insn;
              int phiVarId = phi.varId();
              if(phi.operand1() == ZERO && varPhiMap.containsKey(phiVarId)) {
                PhiInstruction phi2 = varPhiMap.get(phiVarId);
                phi.operand1Is(phi2);
              }
            }
            
            insn = insn.next();
          }
          for(BasicBlock next : block.next()) {
            if(visited.contains(next)) continue;
            blocks.push(next);
          }
        }
      }
	
    	private void propogatePhisToOuterJoin(BasicBlock joinBlockJoin, 
    	                                      ArrayList<PhiInstruction> phis) {
    	  for(PhiInstruction phi : phis) {
          ValueTable.valueTable().put(phi.varId(), phi, phi.ownerBlock());
          if(joinBlockJoin == null) continue;
          Value oldValue = valueTable().get(phi.varId(), joinBlockJoin.immDom());
          PhiInstruction joinBlockJoinPhi = joinBlockJoin.getPhi(phi, oldValue);
          joinBlockJoinPhi.updatePhi(phi);
        }
    	}
	
	private void returnStatement(long id) {
	  Value x = null;
		
	  terminal(Scanner.returnToken, returnStatement+id, "return statement starting w/o return keyword.");
		
		try {
			nodeCounter++;
			dotEmmit(returnStatement+id, expression+nodeCounter);
			x = preProcess(expression(nodeCounter));
		} catch(RuntimeException runEx) { 
		  throw runEx;
		}
		
		Instruction end = null;
		
		if(x.getClass() == VariableZero.class) {
		  end = new Instruction(Opcodes.MUEND, ZERO, null);
		  end.zeroSymId(1, VariableZero.id(x));
		} else {
		  end = new Instruction(Opcodes.MUEND, x, null);
		}
		
		 
		muend(end, FlowGraph.current());
	}
	
	  private Value preProcess(Value expression) {
	    Class<?> expClass = expression.getClass();
	    if(expClass == Instruction.class
	        || expClass == PhiInstruction.class
	        || expClass == RegisterValue.class) {
	      return expression;
	    }
	    
	    if(expClass == ConstantValue.class) {
	      if(expression.value() == 0) {
	        return ZERO;
	      } else {
	        return ZERO.combine(Opcodes.ADD, expression, currentBlock());
	      }
	    }
	    
	    VariableValue variable = (VariableValue) expression;
	    Value numberedValue = valueTable().get(variable.value(), currentBlock());
	    if(numberedValue == ZERO) {
	      numberedValue = VariableZero.get(variable.value());
	    }
	    return numberedValue;
	    // class is VariableValue
//	    throw new RuntimeException("Expression cannot emit VariableValues: " + expression.toString());
	  }
	
	private void statement(long id, BasicBlock joinBlock) {
	  nodeCounter += 1;
		switch(token) {
		case Scanner.letToken:
			dotEmmit(statement+id, assignment+ nodeCounter);
			assignment(nodeCounter);
			break;
		case Scanner.callToken:
			dotEmmit(statement+id, funcCall+ nodeCounter);
			funcCall(nodeCounter);
			break;
		case Scanner.ifToken:
			dotEmmit(statement+id, ifStatement+ nodeCounter);
			ifStatement(nodeCounter);
			break;
		case Scanner.whileToken:
			dotEmmit(statement+id, whileStatement+ nodeCounter);
			whileStatement(nodeCounter);
			break;
		case Scanner.returnToken:
			dotEmmit(statement+id, returnStatement+ nodeCounter);
			returnStatement(nodeCounter);
			break;
		case Scanner.ident:
		  Ident identifier = ident(funcCall+id, "");
		  dotEmmit(funcCall+id, nodeCounter + ":" + identifier);
		  String errMsg = "statements cannot normally start with identifiers.";
		  Value x = predefinedCall(identifier.name);
		  if(x == null) {
		    throwSyntaxError(errMsg);
		  }
		  break;
		case Scanner.endToken:
		  break;
		default:
		  throwSyntaxError("invalid statement. token:" + token);
		}
	}
	
	  private Value predefinedCall(String identifier) {
	    switch(identifier) {
      case Parser.read:
        return inputNum(nodeCounter);
      case Parser.write:
        return outputNum(nodeCounter);
      case Parser.writeln:
        return outputNewLine(nodeCounter);
      default:
        return null;
      }
	  }
	
	private void statSequence(long id, BasicBlock joinBlock) {
		nodeCounter++;
		dotEmmit(statSequence+id, statement+nodeCounter);
		statement(nodeCounter, joinBlock);
		while(terminal(Scanner.semiToken, statSequence+id)) {
			dotEmmit(statSequence+id, statement + ++nodeCounter);
			statement(nodeCounter, joinBlock);
		}
	}
	
	private ArrayList<Integer> typeDecl(long id) {
	  ArrayList<Integer> typeNDims = new ArrayList<Integer>();
	  		
		if(terminal(Scanner.varToken, typeDecl+id)) {
		  typeNDims.add(Symbol.VARTYPE);
		  return typeNDims;
		}
		
		terminal(Scanner.arrayToken, typeDecl+id, "typeDecl neither var nor array token");
		
		typeNDims.add(Symbol.ARRTYPE);
		
		do {
			terminal(Scanner.openbracketToken, typeDecl+id, "array typeDecl missing open bracket.");
			
			int dims = number(typeDecl+id, "array typeDecl missing number b/w brackets.");
			
			typeNDims.add(dims); // TODO test if the latestVal is still available even after calling nextToken() in terminal(...)
			
			terminal(Scanner.closebracketToken, typeDecl+id, "array typeDecl missing close bracket.");
			
		} while(token == Scanner.openbracketToken);
		return typeNDims;
	}
	
	/**
	 * varDecl = typeDecl indent { “,” ident } “;” .
	 * 
	 * this method does not interact with the symbol table.
	 * @param id
	 * @return a list of symbols representing the declared variables.
	 */
	private ArrayList<Symbol> varDecl(long id) {
	  int type = -1;   
	  int[] dims = null;
	  
		this.dotEmmit(varDecl+id, typeDecl + ++nodeCounter);
		
		ArrayList<Integer> typeNDims = typeDecl(nodeCounter);
		type = typeNDims.get(0);
		dims = new int[typeNDims.size() - 1];
		for(int i = 1; i < typeNDims.size(); i += 1) {
		  dims[i - 1] = typeNDims.get(i);
		}
		
		ArrayList<Ident> varIdents = new ArrayList<>();
		
		Ident varIdent = ident(varDecl + id, "varDecl missing identifier.");
		varIdents.add(varIdent);
		
		while(terminal(Scanner.commaToken, varDecl + id)) {
			varIdent = ident(varDecl + id, "varDecl missing identifier.");
			varIdents.add(varIdent);
		}
		
		terminal(Scanner.semiToken, varDecl + id, "funcDecl missing semicolon.");
		
		ArrayList<Symbol> vars = new ArrayList<>();
		String scope = FlowGraph.current().name();
    switch(type) {
      case Symbol.VARTYPE:
        for(Ident ident : varIdents) {
          Symbol symbol = Symbol.variable(ident.id, ident.name, scope, false);
          vars.add(symbol);
        }
        break;
      case Symbol.ARRTYPE:
        for(Ident ident : varIdents) {
          Symbol symbol = Symbol.array(ident.id, ident.name, scope, dims);
          vars.add(symbol);
        }
        break;
      default:  
        throw new RuntimeException("varDecl. type not recognized.");
    }
		
		return vars;
	}
	
	private Symbol funcDecl(long id) {
	  int funcToken = -1;
  
		switch(token) {
		case Scanner.funcToken:
		case Scanner.procToken:
			funcToken = token;
		  this.dotEmmit(funcDecl + id, ++nodeCounter+":"+scanner.Id2String(token));
			nextToken();
			break;
		default:
			throwSyntaxError("funcDecl does not start with proc/func keywords.");
		}
		
		Ident funcIdent = ident(funcDecl + id, "");
		
		FlowGraph.currentIs(FlowGraph.create(funcIdent.name, 
		    funcToken == Scanner.funcToken ? SymbolType.FUNC : SymbolType.PROC));
		
		Symbol[] params = null;
		
		if(token == Scanner.openparenToken) {
			this.dotEmmit(funcDecl + id, formalParam + ++nodeCounter);
			params = formalParam(nodeCounter);
		} else {
		  params = new Symbol[0];
		}
		
		FlowGraph.current().setupActivationRecord(new ArrayList<>(Arrays.asList(params)));
		
		terminal(Scanner.semiToken, funcDecl + id, "funcDecl missing semicolon.");
		
		Symbol funDecl = null;
    switch(funcToken) {
    case Scanner.funcToken:
      funDecl = Symbol.func(funcIdent.id, funcIdent.name, null, params); break;
    case Scanner.procToken:
      funDecl = Symbol.proc(funcIdent.id, funcIdent.name, params); break;
    default:
      throwSyntaxError("funcDecl does not start with proc/func keywords.");
    }
    
    symbolTable().add(funDecl);
		
		this.dotEmmit(funcDecl + id, funcBody + ++nodeCounter);
		funcBody(nodeCounter);
		
		terminal(Scanner.semiToken, funcDecl + id, "funcDecl missing semicolon.");
		
		
		
		
		return funDecl;
	}

	private void funcBody(long id) {
	  BasicBlock.currentBlockIs(BasicBlock.createNew());
	  
	  HashMap<Integer, Integer> arrays = new HashMap<>();
		while(token == Scanner.varToken || token == Scanner.arrayToken) {
			this.dotEmmit("computation", "varDecl"+ ++nodeCounter);
			ArrayList<Symbol> vars = varDecl(nodeCounter);
			    
			for(int index = 0; index < vars.size(); index += 1) {
			  Symbol var = vars.get(index);
			  symbolTable().add(var.baseAddressIs(Registers.FP()));
			  if(var.type() == SymbolType.ARR) {
			    arrays.put(var.id(), var.length());
			  }
			}
		}
		
		FlowGraph currentFlow = FlowGraph.current();
		
		ActivationRecord stackFrame = currentFlow.activation();
    for(int arrayId : arrays.keySet()) {
      stackFrame.allocateArray(arrayId, arrays.get(arrayId));
    }
		
		terminal(Scanner.beginToken, funcBody+id, "funcBody missing beginToken.");
		
		Instruction start = new Instruction(Opcodes.MUSTART, null , null);
		AuxillaryProcs.addInsn(start);
    currentFlow.muStart(start);
		
		dotEmmit(funcBody+id, statSequence + ++nodeCounter);
		statSequence(nodeCounter, null);
		
		
		
		if(currentFlow.type() == SymbolType.PROC) {
		  Instruction end = new Instruction(Opcodes.MUEND, null, null);
	    muend(end, currentFlow);
		}
		
		terminal(Scanner.endToken, funcBody+id, "funcBody missing endToken.");
	}
	
	  private void muend(Instruction end, FlowGraph currentFlow) {
	    AuxillaryProcs.addInsn(end);
	    ArrayList<Symbol> defdGlobals = currentFlow.getDefinedGlobals(); 
	    ArrayList<Instruction> storeGlobals =
	        FlowGraph.storeGlobals(defdGlobals, end.ownerBlock());
	    for(Instruction store : storeGlobals) {
	      AuxillaryProcs.addInsnBefore(store, end);
	    }
      currentFlow.muEnd(end);
	  }

	private Symbol[] formalParam(long id) {
	  
	  terminal(Scanner.openparenToken, formalParam+id, "formalParam missing openparenToken.");
		
	  ArrayList<Ident> params = new ArrayList<>();
	  
	  
		if(token == Scanner.ident) {
		  int pId = scanner.latestId;
		  Ident param = new Ident(scanner.Id2String(pId), pId);
		  params.add(param);
		  FlowGraph.current().addFormalParam(param.name);
			dotEmmit(formalParam+id, ++nodeCounter+":" + param);
			nextToken();
			while(terminal(Scanner.commaToken, formalParam+id)) {
				param = ident(formalParam+id, "formalParam missing ident");
				params.add(param);
				FlowGraph.current().addFormalParam(param.name);
			}
		}
		
		terminal(Scanner.closeparenToken, formalParam+id, "formalParam missing closeparenToken.");
		 
		Symbol[] paramSymbols = new Symbol[params.size()];
		String scope = FlowGraph.current().name();
		for(int i = 0; i < params.size(); i += 1) {
		  Ident ident = params.get(i);
		  Symbol parameter = Symbol.variable(ident.id, ident.name, scope, true);
		  symbolTable().add(parameter.baseAddressIs(Registers.FP()));
		  paramSymbols[i] = parameter;
		}
		
		return paramSymbols;
	}
	
	public void computation() {
		dotEmmitStart();
		while(token != Scanner.mainToken && token != Scanner.eofToken) {
			nextToken();
		}

		terminal(Scanner.mainToken, computation, "computation not started with main keyword.");
		
		SymbolTable.init();
		
		FlowGraph main = FlowGraph.create(SymbolTable.MAIN, SymbolType.PROC);
		FlowGraph.currentIs(main);
    
    HashMap<Integer, Integer> arrays = new HashMap<>();
    ArrayList<Symbol> parameters = new ArrayList<>();
		while(token == Scanner.varToken || token == Scanner.arrayToken) {
			dotEmmit(computation, varDecl + ++nodeCounter);
			ArrayList<Symbol> vars = varDecl(nodeCounter);
      for(Symbol var : vars) {
        symbolTable().add(var.baseAddressIs(Registers.GP()));
        if(var.type() == SymbolType.ARR) {
          arrays.put(var.id(), var.length());
        } else {
          parameters.add(var);
        }
      }
		}
		
		
		FlowGraph.current().setupActivationRecord(parameters);
		
		ActivationRecord stackFrame = FlowGraph.current().activation();
    for(int arrayId : arrays.keySet()) {
      stackFrame.allocateArray(arrayId, arrays.get(arrayId));
    }
		
		while(token == Scanner.funcToken || token == Scanner.procToken) {
			this.dotEmmit(computation, funcDecl+ ++nodeCounter);
			Symbol funcOrProc = funcDecl(nodeCounter);
			
		}
		
		terminal(Scanner.beginToken, computation, "main missing beginToken.");
		
		FlowGraph.currentIs(main);
		BasicBlock.currentBlockIs(BasicBlock.createNew());
		AuxillaryProcs.addInsn(ZERO);
		this.dotEmmit(computation, statSequence + ++nodeCounter);
		statSequence(nodeCounter, null);
		
		terminal(Scanner.endToken, computation, "main missing endToken.");
		
		terminal(Scanner.periodToken, computation, "main missing periodToken.");
		
		AuxillaryProcs.addInsn(new Instruction(Opcodes.END, null, null));
 		
		dotEmmitStop();
	}
	
	private void throwSyntaxError(String errorMessage) {
		String finalMessage = "syntax error: " + errorMessage 
				+ "\ntoken:" + this.scanner.printToken();;
		throw new RuntimeException(finalMessage);
	}
	
	public static void throwCompilationError(String errorMessage) {
	  String finalMessage = "compile error: " + errorMessage;
	  String scope = "\ncurrent flow:" + FlowGraph.current().name();
    throw new RuntimeException(finalMessage + scope);
	}

	  private void terminal(int expectedToken, String parentName, String errMsg) {
	    if(token == expectedToken) {
	      dotEmmit(parentName, ++nodeCounter+ ":" + scanner.Id2String(token));
	      nextToken();
	    } else {
	      throwSyntaxError(errMsg);
	    } 
	  }
	  	  
	  private boolean terminal(int expectedToken, String parentName) {
	    if(token == expectedToken) {
        dotEmmit(parentName, ++nodeCounter+ ":" + scanner.Id2String(token));
        nextToken();
        return true;
      }
	    return false;
	  }
	  
	  private int number(String parentName, String errMsg) {
	    int number = 0;
	    if(token == Scanner.number) {
	      number = scanner.latestVal;
        dotEmmit(parentName, ++nodeCounter+ ":" + scanner.latestVal);
        nextToken();
      } else {
        throwSyntaxError(errMsg);
      }
	    return number;
	  }
	  
	  private Ident ident(String parentName, String errMsg) {
	    String name = null;
	    int id = -1;
	    if(token == Scanner.ident) {
	      id = scanner.latestId;
        name = scanner.Id2String(id);
        this.dotEmmit(parentName, ++nodeCounter + ":" + name);
        nextToken();
      } else {
        throwSyntaxError("varDecl missing ident.");
      }
	    
	    return new Ident(name, id);
	  }
	

	private void dotEmmitStart() {
//		System.out.println("digraph G {");
//		System.out.println("label=\"" + scanner.mainSourceFileName.replace(".", "") + "\"");
	}
	
	private void dotEmmitStop() {
//		System.out.println("}");
	}
	
	private void dotEmmit(String node1, String node2) {
//		System.out.println(node1 + " -> \"" + node2 + "\"");
	}
	
	private long nodeCounter = 0;
	private static final String designator = "designator";
	private static final String factor = "factor";
	private static final String term = "term";
	private static final String expression = "expression";
	private static final String relation = "relation";
	private static final String assignment = "assignment";
	private static final String funcCall = "funcCall";
	private static final String ifStatement = "ifStatement";
	private static final String whileStatement = "whileStatement";
	private static final String returnStatement = "returnStatement";
	private static final String statement = "statement";
	private static final String statSequence = "statSequence";
	private static final String typeDecl = "typeDecl";
	private static final String varDecl = "varDecl";
	private static final String funcDecl = "funcDecl";
	private static final String formalParam = "formalParam";
	private static final String funcBody = "funcBody";
	private static final String computation = "computation";
	private static final String read = "InputNum";
	private static final String write = "OutputNum";
	private static final String writeln = "OutputNewLine";
	
	private static class Ident {
	  String name;
	  int id;
	  
	  public Ident(String name, int id) {
	    this.name = name;
	    this.id = id;
	  }
	}
	
}