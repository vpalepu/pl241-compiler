package self.vpalepu.pl241;

import java.util.ArrayList;

import self.vpalepu.pl241.Registers.RegisterValue;

public class MuInstruction extends Instruction {

  private final String functionName;
  private final String callerFunctionName;
  
  
  public MuInstruction(String functionName, 
                       String callerFunctionName) {
    super(Opcodes.MU, null, null);
    this.functionName = functionName;
    this.callerFunctionName = callerFunctionName;
  }
  
  public MuInstruction(String functionName, 
                       String callerFunctionName,
                       BasicBlock bb) {
    super(Opcodes.MU, null, null, bb);
    this.functionName = functionName;
    this.callerFunctionName = callerFunctionName;
  }
  
  public String functionName() {
    return this.functionName;
  }
  
  public Instruction target() {
    FlowGraph targetFlow = FlowGraph.get(functionName);
    Instruction target = targetFlow.entryBlock().instructions().head();
    return target;
  }
  
  public void setupTarget() {
    Instruction target = target();
    this.operand1Is(target);
  }
  
  public String callerFunctionName() {
    return this.callerFunctionName;
  }
  
  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append(super.toString())
       .append(" ").append(functionName);
    return str.toString();
  }
  
  
}
