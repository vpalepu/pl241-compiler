package self.vpalepu.pl241;

import self.vpalepu.pl241.Instruction.Operand;

public class Result {
	private Kind kind;
	private int value;
	private int variable;
	private int register;
	private int conditionCode;
	
	private String valueId;
	
	public Result(Kind kind, int resultValue) {
		this.value = -1;
		this.variable = -1;
		this.register = -1;
		this.conditionCode = -1;
		this.kind = kind;
		switch(this.kind) {
		case Const:
			value = resultValue;
			break;
		case Variable:
			variable = resultValue;
			break;
		case Register:
			register = resultValue;
			break;
		case Condition:
			conditionCode = resultValue;
			break;
		default: throw new RuntimeException("WTH!");
		}
	}
	
	public int getValue() {
		switch(kind) {
		case Const:
			return value;
		case Variable:
			return variable;
		case Register:
			return register;
		case Condition:
		  throw new RuntimeException("Use getCc instead.");
		default: throw new RuntimeException("WTH!");
		}
	}
	
	public int getCc() {
	  if(kind != Kind.Condition) {
	    throw new RuntimeException("Kind is not Cond. Use getValue(Kind) instead.");
	  }
	  return conditionCode;
	}
	
	public Kind getKind() {
		return kind;
	}
	
	public void setKind(Kind _kind) {
		this.kind = _kind;
	}
	
	public void setCc(int _ccCode) {
	  if(kind != Kind.Condition) {
      throw new RuntimeException("Kind is not Cond. Use setValue(II) instead.");
	  }
		conditionCode = _ccCode;
	}
	
	public void setValue(Kind _kind, int _value) {
		switch(_kind) {
		case Const:
			value = _value;
			break;
		case Variable:
			variable = _value;
			break;
		case Register:
			register = _value;
			break;
		case Condition:
			throw new RuntimeException("Use setCc instead.");
		default: 
			throw new RuntimeException("WTH!");
		}
	}
	
	public void combine(InsnSet op, Result y) {
		if(this.getKind() == Kind.Const && y.getKind() == Kind.Const) {
			int value = 0;
			switch(op) {
			case ADD: 
				value = this.getValue() + y.getValue();
				break;
			case SUB:
				value = this.getValue() - y.getValue();
				break;
			case MUL:
			  value = this.getValue() * y.getValue();
				break;
			case DIV:
				value = this.getValue() / y.getValue();
				break;
			default:
			  throw new RuntimeException("Not yet implemented");
			}
			this.setValue(this.getKind(), value);
		} else {
		  Instruction newInsn = new Instruction(op, new Operand(this), new Operand(y));
		  AuxillaryProcs.addInsn(newInsn);
		}
	}
	
	public void combine(Result index) {
		index.combine(InsnSet.MUL, new Result(Kind.Const, 4));
		Instruction i1 = null;
		Instruction i2 = new Instruction(InsnSet.ADD, new Operand(Register.FP()), new Operand(this));
		AuxillaryProcs.addInsn(i2);
    Instruction i3 = new Instruction(InsnSet.ADDA, new Operand(i1), new Operand(i2));
    AuxillaryProcs.addInsn(i3);
    Instruction i4 = new Instruction(InsnSet.LOAD, new Operand(i3), null);
    AuxillaryProcs.addInsn(i4);
	}
	
	public String toString() {
	  StringBuffer str = new StringBuffer();
	  switch(kind) {
	  
    case Const:
      str.append("#").append(value);
      break;
    case Register:
      str.append("R").append(register);
      break;
    case Condition:
//      str.append("cc").append(conditionCode);
//      break;
    case Variable:
      str.append(Scanner.get(null).Id2String(variable));
      break;
    default:
	  }
	  return str.toString();
	}
	
	public static enum Kind {
		Const, Variable, Register, Condition
	};
	
	public static class Register {
	  private static Result[] registers = new Result[32];
	  
	  public static Result R(int x) {
	    if(registers[x] == null) {
	      registers[x] = new Result(Kind.Register, x);
	    }
	    return registers[x];
	  }
	  
	  public static Result R0() {
	    if(registers == null) {
	      registers[0] = new Result(Kind.Register, 0);
	    }
	    return registers[0];
	  }
	  
	  public static Result RET() {
	    if(registers == null) {
        registers[27] = new Result(Kind.Register, 27);
      }
      return registers[27];
	  }

	  public static Result FP() {
	    if(registers == null) {
        registers[28] = new Result(Kind.Register, 28);
      }
      return registers[28];
	  }

	  public static Result SP() {
	    if(registers == null) {
        registers[29] = new Result(Kind.Register, 29);
      }
      return registers[29];
	  }

	  public static Result R30() {
	    if(registers == null) {
        registers[30] = new Result(Kind.Register, 30);
      }
      return registers[30];
	  }

	  public static Result R31() {
	    if(registers == null) {
        registers[31] = new Result(Kind.Register, 31);
      }
      return registers[31];
	  }
	}
}