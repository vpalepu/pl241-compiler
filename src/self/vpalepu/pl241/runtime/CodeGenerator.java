package self.vpalepu.pl241.runtime;

import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import com.google.common.base.Joiner;
import com.google.common.collect.HashBasedTable;

import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.ConstantValue;
import self.vpalepu.pl241.Instruction;
import self.vpalepu.pl241.InstructionList;
import self.vpalepu.pl241.Opcodes;
import self.vpalepu.pl241.Registers;
import self.vpalepu.pl241.VariableZero;
import self.vpalepu.pl241.Registers.RegisterValue;
import self.vpalepu.pl241.SymbolTable;
import self.vpalepu.pl241.Value;
import self.vpalepu.pl241.FlowGraph;

public class CodeGenerator {
  
  private final ArrayList<FlowGraph> graphs;
  private final ArrayList<Integer> code;
  
  public static CodeGenerator create(ArrayList<FlowGraph> graphs) {
    return new CodeGenerator(graphs);
  }
  
  private CodeGenerator(ArrayList<FlowGraph> graphs) {
    this.graphs = graphs;
    this.code = new ArrayList<>();
  }
  
  public CodeGenerator gen() {
    FlowGraph main = FlowGraph.initGlobalPointers(0, graphs);
    ArrayList<Instruction> instructions = new ArrayList<>();
    accumulateInstructions(instructions, checkNotNull(main));
    for(FlowGraph g : graphs) {
      if(g.name().equals(SymbolTable.MAIN)) {
        continue;
      }
      accumulateInstructions(instructions, g);
    }
    
    gen(instructions);
    
//    int fixup = 0; 
//    int instructionWord = code.get(fixup);
//    checkPlenInsnWord(instructionWord);
//    int programLength = (this.code.size() * 4) - 1;
//    Instruction initPlen = new Instruction(Opcodes.MOVE,
//                                           new ConstantValue(programLength * 4),
//                                           Registers.PLen())
//                           .resultIs(Registers.PLen());
//    instructionWord = gen(initPlen);
//    code.set(fixup, instructionWord);
    
    return this;
  }
  
    private void checkPlenInsnWord(int instructionWord) {
      // System.err.println(DLX.disassemble(instructionWord));
      int op = instructionWord >>> 26;
      int a = (instructionWord >>> 21) & 0x1F;
      int b = (instructionWord >>> 16) & 0x1F;
      int c = (short) instructionWord; // another dirty trick
      checkState(op == DLX.ADDI && a == 27 && b == 0 && c == 0,
                 Joiner.on(" ").join(op, a, b, c));
    }

  public int[] toIntArray() {
    int[] c = new int[code.size()];
    for(int i = 0; i < c.length; i += 1) {
      c[i] = code.get(i);
    }
    return c;
  }
  
  public void print() {
    for(int iWord : code) {
      System.err.print(DLX.disassemble(iWord));
    }
  }
  
  private void accumulateInstructions(ArrayList<Instruction> instructions,
                                      FlowGraph ... graphs) {
    for(FlowGraph graph : graphs) {
      BasicBlock entry = graph.entryBlock();
      HashBasedTable<Integer, Integer, Boolean> edges = simulateAllEdges(graph);
      ArrayList<BasicBlock> blocks = topSort(entry, edges);
      
      for(BasicBlock block : blocks) {
        addInstructionsFromList(block.instructions(), instructions);
      }
    }
  }
  
  public void gen(ArrayList<Instruction> instructions) {
    for(Instruction insn : instructions) {
      checkState(insn != InstructionList.ZERO);
      
      if(insn.isBranch()) {
        Instruction branchTargetInsn;
        if(insn.operation() == Opcodes.BRA) {
          branchTargetInsn = (Instruction) insn.operand1();
        } else {
          branchTargetInsn = (Instruction) insn.operand2();
        }
        
        int branchTarget = instructions.indexOf(branchTargetInsn);
        while(branchTarget == -1) {
          branchTargetInsn = (Instruction) branchTargetInsn.replacement();
          branchTarget = instructions.indexOf(branchTargetInsn);
        }
        
//        Instruction afterPhi = phi.next();
//        while(afterPhi == null) {
//          ArrayList<BasicBlock> nextBlocks = block.next();
//          if(nextBlocks.size() != 1) {
//            throw new RuntimeException("Huston, we have a problem.");
//          }
//          afterPhi = nextBlocks.get(0).instructions().head();
//        }
        
        int branchTargetOffset = branchTarget - instructions.indexOf(insn);
        if(insn.operation() == Opcodes.BRA) {
          insn.operand1Is(new ConstantValue(branchTargetOffset));
        } else {
          insn.operand2Is(new ConstantValue(branchTargetOffset));
        }
      }
      
      if(insn.operation() == Opcodes.MU 
          && insn.operand1().getClass() == Instruction.class) {
        Instruction operand1 = (Instruction) insn.operand1();
        int operand = instructions.indexOf(operand1) * 4;
        insn.operand1Is(new ConstantValue(operand + 4));
      }
      
      int i = gen(insn);
      this.code.add(i);
//      System.err.print((this.code.size() - 1) + ") " 
//                       + insn.toString() + "\t\t\t\t\t" 
//                       + DLX.disassemble(i));
    }
  }
  
    private void addInstructionsFromList(InstructionList list,
                                         ArrayList<Instruction> instructions) {
      for(Instruction pointer = list.head();
          pointer != null;
          pointer = pointer.next()) {
        if(pointer == InstructionList.ZERO) {
          continue;
        }
        instructions.add(pointer);
      }
  
    }  
  
  public ArrayList<BasicBlock> topSort(BasicBlock entry, 
      HashBasedTable<Integer, Integer, Boolean> edges) {
    ArrayList<BasicBlock> blocks = new ArrayList<>();
    ArrayList<BasicBlock> roots = new ArrayList<>();

    roots.add(checkNotNull(entry));
    for(BasicBlock block = null; !roots.isEmpty();) {
      block = roots.remove(roots.size() - 1);
      int blockId = block.id();
      
      blocks.add(block);
      ArrayList<BasicBlock> nextBlocks = new ArrayList<>(block.next());
      Collections.reverse(nextBlocks);
      for(BasicBlock nextBlock : nextBlocks) {
        int nextBlockId = nextBlock.id();
        Boolean isBack = edges.remove(blockId, nextBlockId);
        if(isBack == null || isBack == true) {
          continue;
        }
        Map<Integer, Boolean> sources = edges.column(nextBlockId);
        HashSet<Integer> forwardEdgeSources = new HashSet<>();
        for(int source : sources.keySet()) {
          boolean isBackEdge = sources.get(source);
          if(!isBackEdge) forwardEdgeSources.add(source);
        }
        if(forwardEdgeSources.isEmpty())
          roots.add(nextBlock);
      }
    }
    
    return blocks;
  }
  
  public HashBasedTable<Integer, Integer, Boolean> simulateAllEdges(FlowGraph graph) {
    HashBasedTable<Integer, Integer, Boolean> edges = HashBasedTable.create();
    for(BasicBlock block : graph.universe()) {
      int blockId = block.id();
      for(BasicBlock nextBlock : block.next()) {
        int nextBlockId = nextBlock.id();
        boolean isBackEdge = false;
        if(nextBlock.dominates(block)) {
          isBackEdge = true;
        }
        edges.put(blockId, nextBlockId, isBackEdge);
      }
    }
    return edges;
  }


  private int gen(Instruction insn) {
    int mnem = -1;
    int arg1 = 0;
    int arg2 = 0;
    int arg3 = 0;
    
    Opcodes opcode = insn.operation();
    Value operand1 = insn.operand1();
    Value operand2 = insn.operand2();
    Value result = insn.result();
    
    if(operand1 != null && operand1.getClass() == VariableZero.class) {
      operand1 = Registers.zero();
    }
    
    if(operand2 != null && operand2.getClass() == VariableZero.class) {
      operand2 = Registers.zero();
    }
    
    arg1 = result == null? 0 : result.value();
    arg2 = operand1 == null ? 0 : operand1.value();
    arg3 = operand2 == null ? 0 : operand2.value();
    
    switch(opcode) {
    case MOVE:
      arg3 = arg2;
      arg2 = 0;
      mnem = DLX.ADD;
      if(operand1.getClass() == ConstantValue.class) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case ADD:
      mnem = DLX.ADD;
      if(isOperand2Constant(insn)) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case ADDA:
      mnem = DLX.ADD;
      if(isOperand2Constant(insn)) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case BRA:
      arg3 = arg2;
      arg2 = 0;
    case BEQ:
      mnem = DLX.BEQ;
      return DLX.assemble(mnem, arg2, arg3);
    case BGE:
      mnem = DLX.BGE;
      return DLX.assemble(mnem, arg2, arg3);
    case BGT:
      mnem = DLX.BGT;
      return DLX.assemble(mnem, arg2, arg3);
    case BLE:
      mnem = DLX.BLE;
      return DLX.assemble(mnem, arg2, arg3);
    case BLT:
      mnem = DLX.BLT;
      return DLX.assemble(mnem, arg2, arg3);
    case BNE:
      mnem = DLX.BNE;
      return DLX.assemble(mnem, arg2, arg3);
    case CMP:
      mnem = DLX.CMP;
      if(isOperand2Constant(insn)) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case DIV:
      mnem = DLX.DIV;
      if(isOperand2Constant(insn)) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case END:
      mnem = DLX.RET;
      return DLX.assemble(mnem, 0);
    case LOAD:
      mnem = DLX.LDX;
      if(insn.operand2().getClass() == ConstantValue.class) {
        mnem -= 1;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case MU:
      mnem = DLX.JSR;
      return DLX.assemble(DLX.JSR, arg2);
    case MUEND:
      mnem = DLX.RET;
      return DLX.assemble(mnem, Registers.RET_ADDR().value());
    case MUSTART:
      return 0;
//      throw new UnsupportedOperationException("MUEND has no machine translation.");
    case MUL:
      mnem = DLX.MUL;
      if(isOperand2Constant(insn)) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case NEG:
      mnem = DLX.SUB;
      return DLX.assemble(mnem, arg1, 0, arg2);
    case NOP:
      throw new UnsupportedOperationException("NOP has no machine translation.");
    case PHI:
      throw new UnsupportedOperationException("PHIs have no machine translation.");
    case READ:
      mnem = DLX.RDI;
      return DLX.assemble(mnem, arg1);
    case RET:
      mnem = DLX.RET;
      return DLX.assemble(mnem, arg3);
    case STORE:
      mnem = DLX.STX;
      if(isOperand2Constant(insn)) {
        mnem -= 1;
      }
      return DLX.assemble(mnem, arg2, arg1, arg3);
    case SUB:
      mnem = DLX.SUB;
      if(isOperand2Constant(insn)) {
        mnem += 16;
      }
      return DLX.assemble(mnem, arg1, arg2, arg3);
    case WLN:
      mnem = DLX.WRL;
      return DLX.assemble(mnem);
    case WRITE:
      mnem = DLX.WRD;
      checkState(operand1.getClass() == RegisterValue.class, operand1.toString());
      arg2 = ((RegisterValue) operand1).value();
      return DLX.assemble(mnem, arg2);
    default:
      throw new UnsupportedOperationException("UnId'd opcode.");
    }
  }
    
    private boolean isOperand2Constant(Instruction insn) {
      return insn.operand2().getClass() == ConstantValue.class;
    }
    
    private boolean isOperand1Constant(Instruction insn) {
      return insn.operand1().getClass() == ConstantValue.class;
    }
    
    private int[] disassembleInsnWord(int instructionWord) {
      int op = instructionWord >>> 26;
      int a = (instructionWord >>> 21) & 0x1F;
      int b = (instructionWord >>> 16) & 0x1F;
      int c = (short) instructionWord; // another dirty trick
      return new int[] {op, a, b, c};
    }
}
