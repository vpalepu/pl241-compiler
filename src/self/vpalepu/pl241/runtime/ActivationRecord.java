package self.vpalepu.pl241.runtime;

import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;

import self.vpalepu.pl241.AuxillaryProcs;
import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.ConstantValue;
import self.vpalepu.pl241.Instruction;
import self.vpalepu.pl241.InstructionList;
import self.vpalepu.pl241.Opcodes;
import self.vpalepu.pl241.Registers;
import self.vpalepu.pl241.SymbolTable.Symbol;
import self.vpalepu.pl241.Value;

/**
 * params ... , 
 * return address, 
 * callerSP, 
 * R1, R2, R3, R4, R5, R6, R7, R8, 
 * array allocations/register spills 
 * @author vijay
 *
 */

public class ActivationRecord {
  private ArrayList<MemoryBlock> allocations;
  private final ArrayList<Symbol> params;
  private final int paramCount;
  private final int returnAddrOffset;
  private final int callerSPoffset;
  private final int[] registerOffsets;
  
  public static ActivationRecord create(ArrayList<Symbol> params) {
    return new ActivationRecord(params);
  }
  
    private ActivationRecord(ArrayList<Symbol> params) {
      this.params = params;
      this.paramCount = this.getParams().size();
      returnAddrOffset = this.paramCount;
      callerSPoffset = this.paramCount + 1;
      registerOffsets = new int[8];
      for(int i = 0; i < 8; i += 1) {
        registerOffsets[i] = callerSPoffset + i + 1;
      }
      allocations = new ArrayList<MemoryBlock>();
    }
  
  /**
   * @return the word count in the activation record.
   */
  public int size() {
    int size = paramCount + 2; // 2 blocks occupied by the return address and callerStackPointer
    size += 8; // 8 blocks occupied by the registers
    for(MemoryBlock m : allocations) {
      if(m == null) continue;
      size += m.isArray() ? m.arraylength() : 1;
    }
    return size;
  }
  
  public boolean allocateValue(int valueId) {
    boolean isArray = false;
    int length = -1;
    
    if(this.contains(valueId, isArray, length)) {
      return false;
    }
    
    MemoryBlock var = MemoryBlock.create(valueId, isArray, length)
                                 .withOffset(this.size());;
    allocations.add(var);
    return true;
  }
  
  public boolean allocateArray(int valueId, int length) {
    
    boolean isArray = true;
    checkState(length > 0);
    if(this.contains(valueId, isArray, length)) {
      return false;
    }
    
    MemoryBlock var = MemoryBlock.create(valueId, isArray, length)
                                 .withOffset(this.size());
    allocations.add(var);
    return true;
  }
  
  public boolean contains(int valueId, boolean isArray, int length) {
    for(MemoryBlock var : allocations) {
      if(var.valueId() == valueId
          && var.isArray == isArray
          && var.arraylength == length) {
        return true;
      }
    }
    return false;
  }
  
  public int offset(int valueId) {
    for(MemoryBlock var : allocations) {
      if(var.valueId() == valueId) {
        return var.offset();
      }
    }
    throw new RuntimeException("offset not available in frame for valueId:" + valueId);
  }
  
  public int spillOffset(int valueId) {
    for(MemoryBlock var : allocations) {
      if(var.valueId() == valueId && !var.isArray()) {
        return var.offset();
      }
    }
    // dirty hack for : 
    // throw new RuntimeException("offset not available in frame for valueId:" + valueId);
    allocateValue(valueId);
    for(MemoryBlock var : allocations) {
      if(var.valueId() == valueId && !var.isArray()) {
        return var.offset();
      }
    }
    
    throw new RuntimeException("Unreachable code:-\n"
        + " offset not available in frame for valueId:" + valueId);
  }
  

  public int paramOffset(int valueId) {
    int offset = 0;
    for(Symbol param : this.getParams()) {
      if(param.id() == valueId) {
        return offset;
      }
      offset += 1;
    }
    
    throw new RuntimeException("param offset not available in frame for valueId:" + valueId);
  }
  
  public int registerOffsets(int regNum) {
    return registerOffsets[regNum - 1];
  }
  
  public int returnAddrOffset() {
    return this.returnAddrOffset;
  }
  
  public int callerFPOffset() {
    return this.callerSPoffset;
  }
  
  /**
   * @return the params
   */
  public ArrayList<Symbol> getParams() {
    return params;
  }

  public static class MemoryBlock {
    private final int valueId;
    private final boolean isArray;
    private final int arraylength;
    private int offset;
    
    public static MemoryBlock create(int valueId, 
                                       boolean isArray, 
                                       int arraylength) {
      return new MemoryBlock(valueId, isArray, arraylength);
    }
    
    private MemoryBlock(int valueId, boolean isArray, int len) {
      this.valueId = valueId;
      this.offset = -1;
      this.isArray = isArray;
      if(isArray) checkState(len > 0);
      this.arraylength = len;
    }
    
    public int arraylength() {
      return arraylength;
    }
    
    public MemoryBlock withOffset(int offset) {
      checkState(offset != -1, "LocalVariable offset within an Activation Record cannot be null.");
      this.offset = offset;
      return this;
    }
    
    public int offset() {
      checkState(offset != -1, "LocalVariable offset within the Activation Record is not setup yet.");
      return offset;
    }
    
    public boolean isArray() {
      return isArray;
    }
    
    public int valueId() {
      return valueId;
    }
  
    @Override
    public String toString() {
      StringBuffer buffer = new StringBuffer();
      buffer.append(valueId);
      if(this.isArray) {
        buffer.append("[").append(arraylength).append("]");
      }
      buffer.append("@").append(offset);
      return buffer.toString();
    }
  }

  
}
