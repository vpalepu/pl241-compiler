package self.vpalepu.pl241;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class Scanner {
	
  private static Scanner scanner = null;
  public static Scanner get(File file) {
    if(scanner == null) {
      scanner = new Scanner(file);
    }
    return scanner;
  }
  
	private FileReader fileReader;
	private StringBuffer accumulator;
	private HashMap<Integer, String> table;
	
	public int token;
	public int latestVal;
	public int latestId;
	public String mainSourceFileName;
	
	public Scanner(File sourceFile) {
		fileReader = new FileReader(sourceFile, StandardCharsets.US_ASCII);
		this.token = errorToken;
		this.accumulator = new StringBuffer();
		this.table = new HashMap<Integer, String>();
		this.prePopulateTable();
		this.mainSourceFileName = sourceFile.getName();
		this.latestId = -1;
		this.latestVal = -1;
	}
	
	public void next() {
		token = scan();
	}
	
	public int getSym() {
		return this.token;
	}
	
	public String getCurrentLine() {
	  return "Line:" + fileReader.currentLineNumber + ": " + fileReader.currentLine;
	}
	
	private void prePopulateTable() {
		this.table.put(timesToken, "*");
		this.table.put(divToken, "/");
		this.table.put(plusToken, "+");
		this.table.put(minusToken, "-");
		this.table.put(periodToken, ".");
		this.table.put(commaToken, ",");
		this.table.put(openbracketToken, "[");
		this.table.put(closebracketToken, "]");
		this.table.put(closeparenToken, ")");
		this.table.put(openparenToken, "(");
		this.table.put(semiToken, ";");
		this.table.put(endToken, "}");
		this.table.put(beginToken, "{");
		this.table.put(thenToken, "then");
		this.table.put(doToken, "do");
		this.table.put(odToken, "od");
		this.table.put(fiToken, "fi");
		this.table.put(elseToken, "else");
		this.table.put(letToken, "let");
		this.table.put(callToken, "call");
		this.table.put(ifToken, "if");
		this.table.put(whileToken, "while");
		this.table.put(returnToken, "return");
		this.table.put(varToken, "var");
		this.table.put(arrayToken, "array");
		this.table.put(funcToken, "function");
		this.table.put(procToken, "procedure");
		this.table.put(mainToken, "main");
		this.table.put(eofToken, "end of file");
		this.table.put(errorToken, "");
		this.table.put(neqlToken, "!=");
		this.table.put(eqlToken, "==");
		this.table.put(gtrToken, ">");
		this.table.put(lssToken, "<");
		this.table.put(geqToken, ">=");
		this.table.put(leqToken, "<=");
		this.table.put(becomeToken, "<-");		
	}
	
	private int scan() {
		char ch = fileReader.getSym();
		
		switch(ch) {
		case (char) 0: fileReader.getNextSym(); return errorToken;
		case (char) -1: fileReader.getNextSym(); return eofToken;
		case '*': fileReader.getNextSym(); return timesToken;
		case '/': fileReader.getNextSym(); return divToken;
		case '+': fileReader.getNextSym(); return plusToken; 
		case '-': fileReader.getNextSym(); return minusToken;
		case '.': fileReader.getNextSym(); return periodToken;
		case ',': fileReader.getNextSym(); return commaToken;
		case '[': fileReader.getNextSym(); return openbracketToken;
		case ']': fileReader.getNextSym(); return closebracketToken;
		case ')': fileReader.getNextSym(); return closeparenToken;
		case '(': fileReader.getNextSym(); return openparenToken;
		case ';': fileReader.getNextSym(); return semiToken;
		case '}': fileReader.getNextSym(); return endToken;
		case '{': fileReader.getNextSym(); return beginToken;
		default: break;
		}
		
		int tokenId;
		if(Character.isDigit(ch)) {
			number();
			tokenId = number;
			this.latestVal = Integer.parseInt(accumulator.toString());
			accumulator.delete(0, accumulator.length());
		} else if(Character.isLetter(ch)) {
			ident();
			tokenId = getReservedOrIdentId(accumulator.toString());
			latestId = tablePointer;
			accumulator.delete(0, accumulator.length());
		} else if(ch == '>' || ch == '<' || ch == '=' || ch == '!') {
			tokenId = relOpAndBecome();
		} else {
			tokenId = errorToken;
			fileReader.getNextSym();
		}
		return tokenId;
	}
	
	private int getReservedOrIdentId(String token) {
		switch(token) {
		case "then": return thenToken;
		case "do": return doToken;
		case "od": return odToken;
		case "fi": return fiToken;
		case "else": return elseToken;
		case "let": return letToken;
		case "call": return callToken;
		case "if": return ifToken;
		case "while": return whileToken;
		case "return": return returnToken;
		case "var": return varToken;
		case "array": return arrayToken;
		case "function": return funcToken;
		case "procedure": return procToken;
		case "main": return mainToken;
		default: this.insertIntoTable(token); return ident;
		}
	}
	
  	private int tablePointer = 0;
    private void insertIntoTable(String token) {
      while(this.table.containsKey(this.tablePointer)) {
        tablePointer += 1;
      }
      this.table.put(tablePointer, token);
    }
	
	private void ident() {
		char currentSymbol = fileReader.getSym();
		while(Character.isLetter(currentSymbol) 
				|| Character.isDigit(currentSymbol)) {
			accumulator.append(currentSymbol);
			currentSymbol = fileReader.getNextSym();
		}
	}
	
	private void number() {
		char currentSymbol = fileReader.getSym();
		while(Character.isDigit(currentSymbol)) {
			accumulator.append(currentSymbol);
			currentSymbol = fileReader.getNextSym();
		}
	}
	
	private int relOpAndBecome() {
		char currentSymbol = fileReader.getSym();
		switch(currentSymbol) {
		case '=':
			currentSymbol = fileReader.getNextSym();
			if(currentSymbol == '=') { // ==
				fileReader.getNextSym();
				return eqlToken;
			} else { // = ... syntax error
				throw new RuntimeException("syntax error: undefined token: '='");
			}
		case '!': 
			currentSymbol = fileReader.getNextSym();
			if(currentSymbol == '=') { // != 
				fileReader.getNextSym();
				return neqlToken;
			} else { // ! ... syntax error
				throw new RuntimeException("syntax error: undefined token: '!'");
			}
		case '>': 
			currentSymbol = fileReader.getNextSym();
			if(currentSymbol == '=') { // >=
				fileReader.getNextSym();
				return geqToken;
			} else { // >
				return gtrToken;
			}
		case '<':
			currentSymbol = fileReader.getNextSym();
			if(currentSymbol == '=') { // <=
				fileReader.getNextSym();
				return leqToken;
			} else if (currentSymbol == '-') { // <-
				fileReader.getNextSym();
				return becomeToken;
			} else { // <
				return lssToken;
			}
		default: throw new RuntimeException("WTH!");
		}
	}
	
	public String Id2String(int id) {
		return this.table.get(id);
	}
	
	public int String2Id(String id) {
		for(int key : table.keySet()) {
			if(table.get(key).equals(id)) {
				return key;
			}
		}
		throw new RuntimeException("no key in table found for value:" + id);
	}
	
	public String printToken() {
		switch(token) {
		case Scanner.number:
			return (token + " " + this.latestVal);
		case Scanner.ident:
			return (token + " " + this.Id2String(this.latestId) + " " + this.latestId);
		case Scanner.errorToken: 
			if(Parser.debug) return "0";
			return "";
		default:
			return token + " " + this.Id2String(token);
		}
	}
	
	public static final int errorToken = 0;
	public static final int timesToken = 1;
	public static final int divToken = 2;
	
	public static final int plusToken = 11;
	public static final int minusToken = 12;
	
	public static final int eqlToken = 20;
	public static final int neqlToken = 21;
	public static final int lssToken = 22;
	public static final int geqToken = 23;
	public static final int leqToken = 24;
	public static final int gtrToken = 25;
	
	public static final int periodToken = 30;
	public static final int commaToken = 31;
	public static final int openbracketToken = 32;
	public static final int closebracketToken = 34;
	public static final int closeparenToken = 35;
	
	public static final int becomeToken = 40;
	public static final int thenToken = 41;
	public static final int doToken = 42;
	
	public static final int openparenToken = 50;
	
	public static final int number = 60;
	public static final int ident = 61;
	
	public static final int semiToken = 70;
	
	public static final int endToken = 80;
	public static final int odToken = 81;
	public static final int fiToken = 82;
	
	public static final int elseToken = 90;
	
	public static final int letToken = 100;
	public static final int callToken = 101;
	public static final int ifToken = 102;
	public static final int whileToken = 103;
	public static final int returnToken = 104;
	
	public static final int varToken = 110;
	public static final int arrayToken = 111;
	public static final int funcToken = 112;
	public static final int procToken = 113;
	
	public static final int beginToken = 150;
	public static final int mainToken = 200;
	public static final int eofToken = 255;
	
	
	
	
	
	
}
