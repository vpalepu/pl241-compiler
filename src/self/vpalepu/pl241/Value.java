package self.vpalepu.pl241;

public abstract class Value {
  public abstract int value();
  public abstract void valueIs(int _value);
  public abstract Value combine(Opcodes op, Value a, BasicBlock ownerBlock);
}
