package self.vpalepu.pl241;

import static com.google.common.base.Preconditions.*;
import static self.vpalepu.pl241.InstructionList.ZERO;
import static self.vpalepu.pl241.SymbolTable.symbolTable;
import static self.vpalepu.pl241.ValueTable.valueTable;
import static self.vpalepu.pl241.BasicBlock.currentBlock;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import self.vpalepu.pl241.Registers.RegisterValue;
import self.vpalepu.pl241.SymbolTable.Symbol;
import self.vpalepu.pl241.SymbolTable.SymbolType;
import self.vpalepu.pl241.phase2.Anchor;
import self.vpalepu.pl241.runtime.ActivationRecord;
import self.vpalepu.pl241.tests.ParserDriver;
import self.vpalepu.pl241.viz.InferenceVisualizer;
import self.vpalepu.pl241.viz.InferenceVisualizer.ShellSettings;

import com.google.common.base.Function;

public class FlowGraph {
  public static ArrayList<FlowGraph> grandUniverse = new ArrayList<FlowGraph>();
  private static FlowGraph current;
  
  private ArrayList<BasicBlock> universe;
  private HashMap<Integer, ArrayList<Integer>> domTree;
  private String name;
  private SymbolType type;
  private ActivationRecord activationRecord;

  private ArrayList<String> formalParams;
  
  private Instruction muEnd;
  private Instruction muStart;
  
  private boolean[] usedRegisters;
  private ArrayList<Symbol> definedGlobals;
  
  public static FlowGraph current() {
    return current;
  }
  
  public static void currentIs(FlowGraph _current) {
    current = _current;
  }
  
  public static FlowGraph mainFlow(ArrayList<FlowGraph> graphs) {
    FlowGraph main = null;
    for(FlowGraph g : graphs) {
      String name = g.name();
      if(name.equals(SymbolTable.MAIN)) {
        main = g;
        break;
      }
    }
    return main;
  }
  
  public static FlowGraph get(String name) {
    for(FlowGraph graph : grandUniverse) {
      if(graph.name().equals(name)) {
        return graph;
      }
    }
    return null;
  }
  
  public static FlowGraph create(String name, SymbolType type) {
    checkState(type == SymbolType.FUNC || type == SymbolType.PROC);
    FlowGraph graph = new FlowGraph(name, type);
    grandUniverse.add(graph);
    return graph;
  }
  
  private FlowGraph(String _name, SymbolType _type) {
    name = _name;
    type = _type;
    universe = new ArrayList<BasicBlock>();
    formalParams = new ArrayList<String>();
    domTree = new HashMap<Integer, ArrayList<Integer>>();
    usedRegisters = new boolean[8];
    for(int i = 0; i < 8; i += 1) {
      usedRegisters[i] = false;
    }
    
    definedGlobals = new ArrayList<>();
  }
  
  public ArrayList<BasicBlock> universe() {
    return universe;
  }
  
  public String name() {
    return name;
  }

  public BasicBlock entryBlock() {
    return universe.get(0);
  }
  
  public BasicBlock blockById(int blockId) {
    BasicBlock block = null;
    for(BasicBlock b : universe) {
      if(b.id() == blockId) {
        block = b;
        break;
      }
    }
    
    if(block == null) 
      throw new RuntimeException("basicBlock " + blockId 
          + " not found in universe.");
    return block;
  }
  
  public void addDefinedGlobal(Symbol symbol) {
    this.getDefinedGlobals().add(symbol);
  }
  
  public void addFormalParam(String param) {
    formalParams.add(param);
  }
  
  public String[] formalParams() {
    return this.formalParams.toArray(new String[0]);
  }

  public void setupActivationRecord(ArrayList<Symbol> params) {
    this.activationRecord = ActivationRecord.create(params);
  }
  
  public ActivationRecord activation() {
    return this.activationRecord;
  }
  
  public void anchor() {
    Anchor.reset();
    ArrayList<Integer> domTreeDFT = 
        DomTreeOperations.travelDepthFirst(domTree, this.entryBlock().id());
//    System.err.println("domTree DFT: " + Arrays.toString(domTreeDFT.toArray()));
    int previousBlockId = -1;
    for(int blockId : domTreeDFT) {
      boolean isChild = true;
      if(previousBlockId != -1) {
        ArrayList<Integer> children = domTree.get(previousBlockId);
        isChild = children != null && children.contains(blockId);
      }
      
      if(!isChild) {
        Anchor.reset(computeEliminated(previousBlockId, blockId));
//        System.err.println(blockId + " resets to " + previousBlockId + "\n");
//        System.err.println(Anchor.stringify() + "\n*******break*******\n");
      }
      
      Anchor.anchor(blockById(blockId));
//      System.err.println(blockId + "done\n");
//      System.err.println(Anchor.stringify() + "\n*******break*******\n");
      previousBlockId = blockId;
    }
  }
  
      private HashSet<Integer> computeEliminated(int prevBlockId, 
          int currentBlockId) {
        int limit = blockById(currentBlockId).immDom().id();
        HashSet<Integer> eliminated = new HashSet<>();
        
        int pointer = prevBlockId;
        while(pointer != limit) {
          eliminated.add(pointer);
          pointer = blockById(pointer).immDom().id();
        }
        return eliminated;
      }
  
  public void buildDomTree() {
    domTree = new HashMap<Integer, ArrayList<Integer>>();
    for(BasicBlock block : universe) {
      int blockId = block.id();
      BasicBlock immDom = block.immDom();
      if(immDom == null) continue;
      int immDomId = immDom.id();
      ArrayList<Integer> children = domTree.get(immDomId);
      if(children == null) {
        children = new ArrayList<Integer>();
        domTree.put(immDomId, children);
      }
      children.add(blockId);
    }
//    System.err.println("basic block dom tree building complete");
  }
  
  public void eliminateRedundancy1() {
    ArrayList<Integer> domTreeDFT = 
        DomTreeOperations.travelDepthFirst(domTree, this.entryBlock().id());
    optimize(domTreeDFT, copyPropogate);
    removeOrReEvaluate(domTreeDFT);
    
    optimize(domTreeDFT, constantWrites);
    removeOrReEvaluate(domTreeDFT);
    
    optimize(domTreeDFT, constantBranches);
    removeOrReEvaluate(domTreeDFT);
    
    optimize(domTreeDFT, constantInstructions);
    removeOrReEvaluate(domTreeDFT);
  }
  
  public void eliminateRedundancy2() {
    ArrayList<Integer> domTreeDFT = 
        DomTreeOperations.travelDepthFirst(domTree, this.entryBlock().id());

    optimize(domTreeDFT, cse);
    removeOrReEvaluate(domTreeDFT);
    
    optimize(domTreeDFT, removeKills);
    removeOrReEvaluate(domTreeDFT);
  }
  
  public void eliminateNops() {
    ArrayList<Integer> domTreeDFT = 
        DomTreeOperations.travelDepthFirst(domTree, this.entryBlock().id());
    optimize(domTreeDFT, eliminateNops);
    removeOrReEvaluate(domTreeDFT);
  }
  
    private void removeOrReEvaluate(ArrayList<Integer> domTreeDFT) {
      for(int blockId : domTreeDFT) {
        InstructionList insnList = blockById(blockId).instructions();
        Instruction insn = insnList.head();
        while(insn != null) {
          if(insn.deleted()) {
            insnList.remove(insn);
          } else {
            reEvaluate(insn);
          }
          insn = insn.next();
        }
      }
    }
    
    private void optimize(ArrayList<Integer> domTreeDFT, 
                          Function<Instruction, Instruction> optimization) {
      for(int blockId : domTreeDFT) {
        BasicBlock block = blockById(blockId);
        InstructionList insnList = block.instructions();
        if(insnList.isEmpty()) continue;
        
        for(Instruction insn = insnList.head();
            !insnList.isEmpty() && insn != null;
            insn = insn.next()) {
          reEvaluate(insn);
          optimization.apply(insn);
        }
        
      }
    }
  
      private void reEvaluate(Instruction insn) {
        insn.operand1Is(eval(insn.operand1()));
        insn.operand2Is(eval(insn.operand2()));
      }
      
        private Value eval(Value value) {
          if(value == null || !(value instanceof Instruction)) 
            return value;
          
          Instruction insn = (Instruction) value;
          return insn.deleted() ? eval(insn.replacement()) : insn; 
        }
  
    private Function<Instruction, Instruction> cse = 
        new Function<Instruction, Instruction>() {
      @Override public Instruction apply(Instruction insn) {
        if(insn.operation() == Opcodes.STORE) return insn;
        return insn.operation() == Opcodes.LOAD ? cLe(insn) : cse(insn);
      }
      
          private Instruction cLe(Instruction insn) {
            Instruction opDom = insn.opDom();
      
            Value insnOp1 = insn.operand1();
            Value insnOp2 = insn.operand2();
            while(opDom != null) {
              Value opDomOp1 = opDom.operand1();
              Value opDomOp2 = opDom.operand2();
              switch(opDom.operation()) {
              case LOAD:
                boolean ops1Equal = opsEqual(opDomOp1, insnOp1);
                boolean ops2Equal = opsEqual(opDomOp2, insnOp2); 
                if(ops1Equal && ops2Equal) {
                  insn.replace(cLe(opDom));
                  return insn;
                }
                break;
              case STORE:
                if(insn.loadStoreSym() != -1 
                    && insn.loadStoreSym() == opDom.loadStoreSym())
                  return insn;
                break;
              default:
                throw new RuntimeException("");
              }
              opDom = opDom.opDom();
            }
            return insn;
          }
          
          private Instruction cse(Instruction insn) {
            switch(insn.operation()) {
            case NOP: case READ: case END: case WLN: case WRITE:
            case MUEND: case MUSTART: case MU:
              return insn;
            default:
              break;
            }
            
            Instruction opDom = insn.opDom();
            while(opDom != null) {
              Value opDomOp1 = opDom.operand1();
              Value opDomOp2 = opDom.operand2();
              Value insnOp1 = insn.operand1();
              Value insnOp2 = insn.operand2();
              boolean ops1Equal = opsEqual(opDomOp1, insnOp1); 
              boolean ops2Equal = opsEqual(opDomOp2, insnOp2);
              if(ops1Equal && ops2Equal) {
                insn.replace(cse(opDom));
                break;
              }
              opDom = opDom.opDom();
            }
            return insn;
          }
          
          private boolean opsEqual(Value op1, Value op2) {
            if(op1 == null && op2 == null) {
              return true;
            }
            
            if(op1.getClass() != op2.getClass()) {
              return false;
            }
            
            if(op1.value() == op2.value()) {
              return true;
            }
            
            return false;
          }
    };
    
    
    private Function<Instruction, Instruction> copyPropogate = 
        new Function<Instruction, Instruction>() {
      @Override public Instruction apply(Instruction insn) {
        if(insn.operation() == Opcodes.MOVE) {
          insn.replace(insn.operand1()); 
        } else {
          fold(insn);
        }
        
        return null;
      }
      
        private void fold(Instruction insn) {
          Value operand1 = insn.operand1();
          Value operand2 = insn.operand2();
          
          Opcodes opcode = insn.operation();
          
          if(operand1 != null 
              && operand1.getClass() == ConstantValue.class 
              && operand2 != null 
              && operand2.getClass() == ConstantValue.class) {
            switch(opcode) {
            case ADD: case MUL: case SUB: case DIV: case CMP:
              ConstantValue constant = 
                (ConstantValue) operand1.combine(opcode, operand2, insn.ownerBlock());
              insn.replace(constant);
              break;
            default:
            }
          }
        }
    };
    
    private Function<Instruction, Instruction> eliminateNops = 
        new Function<Instruction, Instruction>() {
      @Override public Instruction apply(Instruction insn) {
        if(insn == null || insn.operation() != Opcodes.NOP) return insn;
        Instruction next = insn.next();
        if(next == null) {
          ArrayList<BasicBlock> nextBlocks = insn.ownerBlock().next();
          switch(nextBlocks.size()) {
          case 0: break;
          case 1: next = nextBlocks.get(0).instructions().head(); break;
          default:
            throw new RuntimeException("issue in finding the next block. number of nextBlocks: " + nextBlocks.size());
          }
        }
        insn.replace(next);
        return insn;
      }
    };
    
    
    
    private Function<Instruction, Instruction> removeKills = 
        new Function<Instruction, Instruction>() {
      @Override public Instruction apply(Instruction insn) {
        if(insn == null 
            || !(insn.operation() == Opcodes.STORE 
                && insn.operand1() == null 
                && insn.operand2() == null)) return insn;
        Instruction next = insn.next();
        if(next == null) {
          ArrayList<BasicBlock> nextBlocks = insn.ownerBlock().next();
          switch(nextBlocks.size()) {
          case 0: break;
          case 1: next = nextBlocks.get(0).instructions().head(); break;
          default:
            throw new RuntimeException("issue in finding the next block. number of nextBlocks: " + nextBlocks.size());
          }
        }
        insn.replace(next);
        return insn;
      }
    };
    
    private Function<Instruction, Instruction> constantWrites = 
        new Function<Instruction, Instruction>() {

      @Override
      public Instruction apply(Instruction arg0) {
        if(arg0.operation() == Opcodes.WRITE 
            && arg0.operand1().getClass() == ConstantValue.class) {
          if(arg0.operand1().value() == 0) {
            arg0.operand1Is(ZERO);
          } else {
            BasicBlock block = arg0.ownerBlock();
            Instruction add = new Instruction(Opcodes.ADD, 
                                              InstructionList.ZERO, 
                                              arg0.operand1(), 
                                              arg0.ownerBlock());
            arg0.operand1Is(add);
            block.instructions().addInsnBefore(add, arg0);
          }
        }
        return null;
      }
    };
    
    
    private Function<Instruction, Instruction> constantBranches = 
        new Function<Instruction, Instruction>() {

      @Override
      public Instruction apply(Instruction arg0) {
        if((arg0.isBranch())
            && arg0.operand1().getClass() == ConstantValue.class) {
          if(arg0.operand1().value() == 0) {
            arg0.operand1Is(ZERO);
          } else {
            BasicBlock block = arg0.ownerBlock();
            Instruction add = new Instruction(Opcodes.ADD, 
                InstructionList.ZERO, 
                arg0.operand1(), 
                arg0.ownerBlock());
            arg0.operand1Is(add);
            block.instructions().addInsnBefore(add, arg0);
          }
        }
        return null;
      }
    };
    
    private Function<Instruction, Instruction> constantInstructions = 
        new Function<Instruction, Instruction>() {

      @Override
      public Instruction apply(Instruction arg0) {
        
        if(!checkNotNull(arg0).isBranch() 
            && arg0.operation() != Opcodes.WRITE 
            && arg0.operation() != Opcodes.PHI
            && arg0.operand1() != null
            && arg0.operand1().getClass() == ConstantValue.class) {
          if(arg0.operand1().value() == 0) {
            arg0.operand1Is(ZERO);
          } else {
            BasicBlock block = arg0.ownerBlock();
            Instruction add = new Instruction(Opcodes.ADD, 
                                              InstructionList.ZERO, 
                                              arg0.operand1(), 
                                              arg0.ownerBlock());
            arg0.operand1Is(add);
            block.instructions().addInsnBefore(add, arg0);
          }
        }
        return null;
      }
    };
    
    
  public void eliminatePhis2() {
    for(BasicBlock block : this.universe()) {
      for(PhiInstruction phi : block.phis()) {
        Value result = phi.result();
        Value op1 = phi.operand1();
        Value op2 = phi.operand2();
        if(result == op1 && result == op2) { 
          // do nothing.
        } else if(op1 == op2 && result != op1) {
          Instruction mov = new Instruction(Opcodes.MOVE, op1, result);
          mov.resultIs(result);
          mov.pointTo(block);
          block.instructions().addInsnAfter(mov, phi);
        } else if(result != op1 && result == op2) {
          ArrayList<BasicBlock> blockPrevs = block.previous();
          checkState(blockPrevs.size() == 2);
          BasicBlock blockA = blockPrevs.get(0);
          handlePhiOperand(block, result, op1, blockA);
          
        } else if(result != op2 && result == op1) {
          ArrayList<BasicBlock> blockPrevs = block.previous();
          checkState(blockPrevs.size() == 2);
          BasicBlock blockB = blockPrevs.get(1);
          handlePhiOperand(block, result, op2, blockB);
          
        } else if(result != op2 && result != op1) {
          ArrayList<BasicBlock> blockPrevs = block.previous();
          checkState(blockPrevs.size() == 2);
          
          if(result != op1) {
            BasicBlock blockA = blockPrevs.get(0);
            handlePhiOperand(block, result, op1, blockA);
          }
          
          if(result != op2) {
            BasicBlock blockB = blockPrevs.get(1);
            handlePhiOperand(block, result, op2, blockB);
          }
        }
        
        Instruction afterPhi = phi.next();
        while(afterPhi == null) {
          ArrayList<BasicBlock> nextBlocks = block.next();
          if(nextBlocks.size() != 1) {
            throw new RuntimeException("Huston, we have a problem.");
          }
          afterPhi = nextBlocks.get(0).instructions().head();
        }
        phi.replace(afterPhi);
        block.instructions().remove(phi);
      }
    }
  }

    private void handlePhiOperand(BasicBlock block, 
                                  Value result, 
                                  Value op,
                                  BasicBlock opBlock) {
      Instruction mov = new Instruction(Opcodes.MOVE, op, result, opBlock)
                        .resultIs(result);
      InstructionList opBlockInsns = opBlock.instructions();
      Instruction opBlockLastInsn = opBlockInsns.last(); 
      if(opBlockLastInsn.isBranch()) {
        opBlockInsns.addInsnBefore(mov, opBlockLastInsn);
      } else {
        opBlockInsns.addToLast(mov);
      }
    }
  
  private static Value[] colorValues = new Value[] {
    Registers.zero(),
    Registers.getRegister(1),
    Registers.getRegister(2),
    Registers.getRegister(3),
    Registers.getRegister(4),
    Registers.getRegister(5),
    Registers.getRegister(6),
//    Registers.getRegister(7),
//    Registers.getRegister(8),
    InstructionList.ZERO
  };
  
  public void insertSpillCode() {
    for(BasicBlock block : this.universe()) {
      ArrayList<Instruction> insns = new ArrayList<>();
      InstructionList instructions = block.instructions();
      for(Instruction insn = instructions.head(); 
          insn != null; 
          insn = insn.next()) {
        insns.add(insn);
      }
      
      for(Instruction insn : insns) {
        insn.allocateOrSpill(activationRecord);
      }
    }
  }
 
  public void allocateVirtualRegisters() {
    InterferenceGraph interference = InterferenceGraph.create().build(this);
    interference = interference.coalesce(this);
    
    ArrayList<Integer> zeroColored = new ArrayList<>();
    for(BasicBlock block : this.universe()) {
      InstructionList insns = block.instructions();
      for(Instruction i = insns.head(); i != null; i = i.next()) {
        if(i.isBranch()
            || i.operation() == Opcodes.END
            || i.operation() == Opcodes.WLN
            || i.operation() == Opcodes.WRITE
            || i.operation() == Opcodes.NOP
            || i.operation() == Opcodes.MU
            || i.operation() == Opcodes.MUEND
            || i.operation() == Opcodes.MUSTART
            || i.result() == Registers.zero()) {
          zeroColored.add(i.id());
        }
      }
    }
    
    // TODO remove zero-colored nodes.
    
    HashMap<Integer, Integer> colorings = 
        interference.color(zeroColored, Registers.REGISTER_ALLOC_COUNT);
    
    for(int insnId : zeroColored) {
      if(!colorings.containsKey(insnId)) {
        colorings.put(insnId, 0);
      }
    }
    
//    System.err.println("Number of Colors used: " 
//                       + new HashSet<>(colorings.values()).size());
    
//    if(ParserDriver.DEBUG) {
//      interference.visualize(this.universe, colorings);
//    }
    
    ArrayList<BasicBlock> blocks = this.universe();
    
    for(BasicBlock block : blocks) {
      InstructionList insns = block.instructions();
      for(Instruction insn = insns.head(); insn != null; insn = insn.next()) {
        insn.color(colorings);
      }
    }
    
    // find the general purpose registers being utilized by this control flow.
    for(int color : new HashSet<>(colorings.values())) {
      if(color > 0 && color <=6) {
        usedRegisters[color - 1] = true;
      }
    }
  }

  
  /**
   * 
   * @param programLength will be given as a 32-bit integer. 
   * the method will compute the appropriate byte address from it.
   * @param mainEntry
   */
  public static FlowGraph initGlobalPointers(int programLength, 
                                             ArrayList<FlowGraph> graphs) {
    FlowGraph main = FlowGraph.mainFlow(checkNotNull(graphs));
    BasicBlock mainEntry = checkNotNull(main).entryBlock();
    
//    Instruction initGp = new Instruction(Opcodes.MOVE, 
//                                         new ConstantValue(9996), 
//                                         Registers.GP());
    Instruction initGp = new Instruction(Opcodes.SUB, Registers.GP(), new ConstantValue(3));
    initGp.resultIs(Registers.GP());
    initGp.pointTo(mainEntry);
    
//    Instruction initFp = new Instruction(Opcodes.MOVE, 
//                                         new ConstantValue(9996),
//                                         Registers.FP());
    Instruction initFp = new Instruction(Opcodes.MOVE, Registers.GP(), Registers.FP());
    initFp.resultIs(Registers.FP());
    initFp.pointTo(mainEntry);
    
    ActivationRecord mainFrame = main.activation();
    int mainSize = mainFrame.size();
//    Instruction initSp = new Instruction(Opcodes.MOVE, 
//                                         new ConstantValue(9996 + (mainSize * -4)),
//                                         Registers.SP());
    Instruction initSp = new Instruction(Opcodes.SUB, Registers.GP(), new ConstantValue(mainSize * 4));
    initSp.resultIs(Registers.SP());
    initSp.pointTo(mainEntry);
    
//    Instruction initPlen = new Instruction(Opcodes.MOVE,
//                                           new ConstantValue(programLength * 4),
//                                           Registers.PLen());
//    initPlen.resultIs(Registers.PLen());
//    initPlen.pointTo(mainEntry);
    
    InstructionList instructions = mainEntry.instructions();
    instructions.addToFront(initSp);
    instructions.addToFront(initFp);
    instructions.addToFront(initGp);
//    instructions.addToFront(initPlen);
    return main;
  }
  
  public static class DomTreeOperations {
    
    public static ArrayList<Integer> 
    travelDepthFirst(HashMap<Integer, ArrayList<Integer>> domTree, int root) {
      Stack<Integer> stack = new Stack<Integer>();
      ArrayList<Integer> traversal = new ArrayList<Integer>();
      stack.push(root);
      while(!stack.isEmpty()) {
        int top = stack.pop();
        traversal.add(top);
        ArrayList<Integer> children = domTree.get(top);
        if(children == null || children.isEmpty()) continue;
        for(int child : children) {
          stack.push(child);
        }
      }
      return traversal;
    }
    
    public static ArrayList<Integer> 
    travelBreadthFirst(HashMap<Integer, ArrayList<Integer>> domTree) {
      ArrayList<Integer> queue = new ArrayList<>(); 
      ArrayList<Integer> traversal = new ArrayList<Integer>();
      int root = 0;
      queue.add(queue.size(), root);
      while(!queue.isEmpty()) {
        int top = queue.remove(0);
        traversal.add(top);
        ArrayList<Integer> children = domTree.get(top);
        if(children == null || children.isEmpty()) continue;
        for(int child : children) {
          queue.add(queue.size(), child);
        }
      }
      return traversal;
    }
  }

  /* MUs */
  
  public void expandMus() {
    for(BasicBlock block : this.universe()) {
      InstructionList instructions = block.instructions();
      for(Instruction insn = instructions.head(); 
          insn != null; 
          insn = insn.next()) {
        if(insn.getClass() != MuInstruction.class) {
          continue;
        }
        
        MuInstruction mu = (MuInstruction) insn;
        mu.setupTarget();
      }
    }
    
    if(this.muStart != null) {
      replaceMuStart();
    }
    
    if(this.muEnd != null) {
      replaceMuEnd();
    }
  }
  
  public static ArrayList<Instruction> storeGlobals(ArrayList<Symbol> definedGlobals,
                                                    BasicBlock block) {
    ArrayList<Instruction> insns = new ArrayList<>();
    for(Symbol definedGlobal : definedGlobals) {
      RegisterValue base = definedGlobal.baseAddress();
      checkState(base == Registers.GP(), 
                 "Global Symbol's Base Register:" + base.toString());
      FlowGraph mainFlow = FlowGraph.get(SymbolTable.MAIN);
      ActivationRecord mainStackFrame = mainFlow.activation();
      int usedGlobalId = definedGlobal.id();
      int offset = mainStackFrame.paramOffset(usedGlobalId);
      ConstantValue offsetValue = new ConstantValue(offset * -4);
      Value value = valueTable().get(usedGlobalId, block);

      Instruction store = new Instruction(Opcodes.STORE, value, offsetValue, block);
      store.loadStoreSymIs(definedGlobal.id());
      store.resultIs(Registers.GP());
      insns.add(store);
    }
    return insns;
  }
  
  public static 
  HashMap<Symbol, Instruction[]> loadGlobals(ArrayList<Symbol> usedGlobals,
                                             BasicBlock block) {
    HashMap<Symbol, Instruction[]> loadInstructions = new HashMap<>();
    
    for(Symbol usedGlobal : usedGlobals) {
      RegisterValue base = usedGlobal.baseAddress();
      checkState(base == Registers.GP(), 
                 "Global Symbol's Base Register:" + base.toString());
      ActivationRecord mainStackFrame = FlowGraph.get(SymbolTable.MAIN).activation();
      int usedGlobalId = usedGlobal.id();
      int offest = mainStackFrame.paramOffset(usedGlobalId);
      Instruction[] loadInsns = Instruction.load(base, offest * -1, block);
      loadInstructions.put(usedGlobal, loadInsns);
    }
    
    return loadInstructions;
  }
  
  public Instruction muStart() {
    return this.muStart;
  }
  
  public void muStart(Instruction muStart) {
    this.muStart = muStart;
  }
  
  public void muEnd(Instruction muEnd) {
    this.muEnd = muEnd;
  }
  
  public Instruction muEnd() {
    return muEnd;
  }
  
  
  public void replaceMuStart() {
    BasicBlock block = muStart.ownerBlock();
    ActivationRecord stackFrame = this.activation();
    ArrayList<Instruction> insns = new ArrayList<>();
    RegisterValue scratch = Registers.SCRATCH3();
    
    // 1 is added to the callerFPOffset (in the target-stack-frame) since the
    // the offset is being computed wrt the caller-SP, which is one behind the 
    // targetFP; and the callerFPOffset is computed wrt to the targetFP
    int callerFPOffset = stackFrame.callerFPOffset() + 1;
    RegisterValue value = Registers.FP();
    List<Instruction> callerFPSeq = 
        storeValue(block, scratch, callerFPOffset, value, Registers.SP());
    insns.addAll(callerFPSeq);
    
    ArrayList<Instruction> fpSp = initFPandSP();
    insns.addAll(fpSp);
    
    int returnAddrOffset = stackFrame.returnAddrOffset();
    value = Registers.RET_ADDR();
    List<Instruction> retAddrSeq = 
        storeValue(block, scratch, returnAddrOffset, value, Registers.FP());
    insns.addAll(retAddrSeq);
    
    for(int i = 1; i <= 8; i += 1) {
      if(usedRegisters[i - 1] == false) {
        continue;
      }
      int offset = stackFrame.registerOffsets(i);
      value = Registers.getRegister(i);
      insns.addAll(storeValue(block, scratch, offset, value, Registers.FP()));
    }
   
    InstructionList list = block.instructions();
    Instruction after = muStart;
    for(Instruction insn : insns) {
      list.addInsnAfter(insn, after);
      after = insn;
    }
  }
  
    private ArrayList<Instruction> initFPandSP() {
      BasicBlock entry = this.entryBlock();
      Instruction moveFp = new Instruction(Opcodes.SUB, 
          Registers.SP(), 
          new ConstantValue(4));
      moveFp.resultIs(Registers.FP());
      moveFp.pointTo(entry);
      
      int stackFrameSize = this.activation().size();
      Instruction subSp = new Instruction(Opcodes.SUB, 
          Registers.SP(), 
          new ConstantValue(stackFrameSize * 4));
      subSp.resultIs(Registers.SP());
      subSp.pointTo(entry);
      
      ArrayList<Instruction> ret = new ArrayList<>();
      ret.add(moveFp);
      ret.add(subSp);
      return ret;
    }

    private List<Instruction> storeValue(BasicBlock block, 
                                         RegisterValue scratch, 
                                         int callerSPOffset, 
                                         RegisterValue value, 
                                         RegisterValue base) {
      ConstantValue offset = new ConstantValue(callerSPOffset * -4);
      Instruction[] storeSeq = 
          Instruction.store(base, offset, value, block, scratch);
      return Arrays.asList(storeSeq);
    }
  
  public void replaceMuEnd() {
    BasicBlock block = muEnd.ownerBlock();
    ActivationRecord record = this.activationRecord;
    ArrayList<Instruction> insns = new ArrayList<>();
    
    if(muEnd.operand1() != null) {
      Instruction moveReturn = new Instruction(Opcodes.MOVE, 
                                               muEnd.operand1(),
                                               Registers.RET(),
                                               block).resultIs(Registers.RET());
      insns.add(moveReturn);
    }
    
    ConstantValue offset = new ConstantValue(record.returnAddrOffset() * -4);
    Instruction[] loadRetAddrSeq = 
        Instruction.load(Registers.FP(), offset, block, Registers.SCRATCH3(), Registers.RET_ADDR());
    insns.addAll(Arrays.asList(loadRetAddrSeq));
    
    for(int i = 1; i <= 8; i += 1) {
      if(usedRegisters[i - 1] == false) {
        continue;
      }
      offset = new ConstantValue(record.registerOffsets(i) * -4);
      Instruction[] loadRegsiterSeq = 
          Instruction.load(Registers.FP(), offset, block, Registers.SCRATCH3(), Registers.getRegister(i));
      insns.addAll(Arrays.asList(loadRegsiterSeq));
    }
    
    Instruction moveSp = 
        new Instruction(Opcodes.ADD, Registers.FP(), new ConstantValue(4), block)
        .resultIs(Registers.SP());
    insns.add(moveSp);
    
    offset = new ConstantValue(record.callerFPOffset() * -4);
    Instruction[] loadCallerFPSeq = 
        Instruction.load(Registers.FP(), offset, block, Registers.SCRATCH3(), Registers.FP());
    insns.addAll(Arrays.asList(loadCallerFPSeq));
    
    InstructionList instructions = block.instructions();
    for(Instruction ins : insns) {
      instructions.addInsnBefore(ins, muEnd);
    }
  }

  
  
  public SymbolType type() {
    return this.type;
  }
  
  @Override
  public String toString() {
    return this.type + " " + this.name;
  }

  /**
   * @return the definedGlobals
   */
  public ArrayList<Symbol> getDefinedGlobals() {
    return definedGlobals;
  }
  
}
