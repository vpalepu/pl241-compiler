package self.vpalepu.pl241;

import static com.google.common.base.Preconditions.*;
import static self.vpalepu.pl241.InstructionList.ZERO;

import java.util.ArrayList;
import java.util.HashMap;

public class Registers {
  
  public static final int REGISTER_ALLOC_COUNT = 6;
  public static final int MAX_REGISTERS = 32;
  
  private static final RegisterValue[] registers = new RegisterValue[MAX_REGISTERS];
  public static RegisterValue getRegister(int i) {
    if(registers[i] == null) {
      registers[i] = new RegisterValue(i);
    }
    return registers[i];
  }
  
  
  public static RegisterValue zero() {
    if(registers[0] == null) {
      registers[0] = new RegisterValue(0);
    }
    return registers[0];
  }
  
  /**
   * 
   * @param index is Zero-index, has max value of 7
   * @return
   */
  public static RegisterValue arg(int index) {
    checkState(index >= 0 && index <= 7);
    return getRegister(9 + index);
  }
  
  /**
   * 
   * @return R9
   */
  public static RegisterValue arg0() {
    return getRegister(9);
  }
  
  /**
   * 
   * @return R10
   */
  public static RegisterValue arg1() {
    return getRegister(10);
  }
  
  /**
   * 
   * @return R11
   */
  public static RegisterValue arg2() {
    return getRegister(11);
  }
  
  /**
   * 
   * @return R12
   */
  public static RegisterValue arg3() {
    return getRegister(12);
  }
  
  /**
   * 
   * @return R13
   */
  public static RegisterValue arg4() {
    return getRegister(13);
  }
  
  /**
   * 
   * @return R14
   */
  public static RegisterValue arg5() {
    return getRegister(14);
  }
  
  /**
   * 
   * @return R15
   */
  public static RegisterValue arg6() {
    return getRegister(15);
  }
  
  /**
   * @return R16
   */
  public static RegisterValue arg7() {
    return getRegister(16);
  }
  
  public static RegisterValue argRetAdr() {
    return getRegister(17);
  }
  
  public static RegisterValue argCallerSP() {
    return getRegister(18);
  }
  
  /**
   * RET value
   * @return R22
   */
  public static RegisterValue RET() {
    return getRegister(22);
  }
  
  /**
   * @return R25
   */
  public static RegisterValue SCRATCH1() {
    return getRegister(25);
  }
  
  /**
   * @return R24
   */
  public static RegisterValue SCRATCH2() {
    return getRegister(24);
  }
  
  /**
   * @return R23
   */
  public static RegisterValue SCRATCH3() {
    return getRegister(23);
  }
  
  /**
   * @return R27
   */
  public static RegisterValue PLen() {
    return getRegister(27);
  }
  
  /**
   * 
   * @return R28
   */
  public static RegisterValue FP() {
    return getRegister(28);
  }
  
  /**
   * 
   * @return R29
   */
  public static RegisterValue SP() {
    return getRegister(29);
  }
  
  /**
   * 
   * @return R30
   */
  public static RegisterValue GP() {
    return getRegister(30);
  }
  
  /**
   * RET address
   * @return
   */
  public static RegisterValue R31() {
    return getRegister(31);
  }
  
  /**
   * 
   * @return R31
   */
  public static RegisterValue RET_ADDR() {
    return R31();
  }
  
  public static class RegisterValue extends Value {
    private int value;

    public RegisterValue(int _value) {
      value = _value;
    }
    
    @Override
    public int value() {
      return value;
    }

    @Override
    public void valueIs(int _value) {
      value = _value;
    }

    @Override
    public Value combine(Opcodes op, Value a, BasicBlock ownerBlock) {
      
      Instruction insn = new Instruction(op, this, a);
      
      if(op == Opcodes.MOVE) {
        ((VariableValue) a).setNumberedValue(insn, ownerBlock);
      } else {
        if(a != null && a.getClass() == VariableValue.class) {
          Value numValue = ((VariableValue) a).getNumberedValue(ownerBlock);
          if(numValue.getClass() == VariableZero.class) {
            insn.zeroSymId(2, VariableZero.id(numValue));
            numValue = ZERO;
          }
          insn.operand2Is(numValue);
        }
      }
          
      AuxillaryProcs.addInsn(insn);
      return insn;
    }
    
    @Override
    public String toString() {
      
      switch(value) {
      case 31:
        return "RET_Addr";
      case 30:
        return "GP";
      case 29:
        return "SP";
      case 28:
        return "FP";
      case 27: 
        return "PLen";
      case 25:
        return "SCRATCH1";
      case 24:
        return "SCRATCH2";
      case 23:
        return "SCRATCH3";
      case 22:
        return "RET_Value";
      default:
        return "R" + value;
      }
      
      
    }
    
    @Override
    public boolean equals(Object value) {
      if(value == null) {
        return false;
      }
      if(!(value instanceof RegisterValue)) {
        return false;
      }
      
      if(((RegisterValue)value).value == this.value) {
        return true;
      }
      
      return false;
    }
    
    @Override
    public int hashCode() {
      return this.value;
    }
  }
  
  public static class VirtualRegister extends Value {

    private final int value;
    
    private static HashMap<Integer, VirtualRegister> registers = new HashMap<>();
    public static VirtualRegister R(int i) {
      VirtualRegister reg = registers.get(i);
      if(reg == null) {
        reg = new VirtualRegister(i);
        registers.put(i, reg);
      }

      return reg;
    }
    
    private VirtualRegister(int value) {
      this.value = value;
    }
    
    @Override
    public int value() {
      return value;
    }

    @Override
    public void valueIs(int _value) {
      throw new UnsupportedOperationException();
    }

    @Override
    public Value combine(Opcodes op, Value a, BasicBlock ownerBlock) {
      throw new UnsupportedOperationException();
    }
    
    @Override
    public int hashCode() {
      return this.value;
    }
    
    @Override
    public String toString() {
      return "VR" + value;
    }
    
    @Override
    public boolean equals(Object value) {
      if(value == null) {
        return false;
      }
      if(!(value instanceof VirtualRegister)) {
        return false;
      }
      
      if(((VirtualRegister)value).value == this.value) {
        return true;
      }
      
      return false;
    }
    
  }
  
}
