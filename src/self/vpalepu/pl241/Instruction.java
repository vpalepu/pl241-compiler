package self.vpalepu.pl241;

import static self.vpalepu.pl241.InstructionList.ZERO;
import static self.vpalepu.pl241.ValueTable.valueTable;
import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.text.html.HTMLDocument.HTMLReader.SpecialAction;

import self.vpalepu.pl241.Registers.RegisterValue;
import self.vpalepu.pl241.Registers.VirtualRegister;
import self.vpalepu.pl241.SymbolTable.Symbol;
import self.vpalepu.pl241.runtime.ActivationRecord;

public class Instruction extends Value {
  
  private int valueNumber;
  private Opcodes operation;
  private Value operand1 = null;
  private Value operand2 = null;
  private Instruction next = null;
  private Instruction previous = null;
  private BasicBlock basicBlock = null;
  private Instruction opDom = null;
  private Value replacement = null;
  private Value result = null;
  
  private int loadStoreSym = -1;
  private int[] zeroSymIds = new int[2];
  
  public Instruction(Opcodes op, Value a, Value b) {
    valueNumber = incId();
    operation = op;
    operand1 = a;
    operand2 = b;
  }
  
  public Instruction(Opcodes op, Value a, Value b, BasicBlock bb) {
    valueNumber = incId();
    operation = op;
    operand1 = a;
    operand2 = b;
    basicBlock = bb;
  }

  public void delete() {
    Instruction next = next();
    Instruction previous = previous();
    
    if(previous != null) {
      previous.next = next;
    }
    
    if(next != null) {
      next.previous = previous;
    }
    
//    this.next = null;
//    this.previous = null;
  }
  
  public boolean deleted() {
    return replacement != null;
  }
  
  @Override
  public Value combine(Opcodes op, Value a, BasicBlock ownerBlock) {
    Instruction insn = new Instruction(op, this, a);
    
    if(op == Opcodes.MOVE) {
      ((VariableValue) a).setNumberedValue(insn, ownerBlock);
    } else {
      if(a != null && a.getClass() == VariableValue.class) {
        Value numValue = ((VariableValue) a).getNumberedValue(ownerBlock);
        if(numValue.getClass() == VariableZero.class) {
          insn.zeroSymId(2, VariableZero.id(numValue));
          numValue = ZERO;
        }
        insn.operand2Is(numValue);
      }
    }
        
    AuxillaryProcs.addInsn(insn);
    return insn;
  }
  
  private static int ID = -1;
  private static int incId() {
    ID += 1;
    return ID;
  }
  
  public int id() {
    return valueNumber;
  }
  
  public boolean isBranch() {
    Opcodes opcode = operation();
    if(opcode == null) return false;
    switch(opcode) {
    case BEQ:
    case BGE:
    case BGT:
    case BLE:
    case BLT:
    case BNE:
    case BRA:
      return true;
    default:
      return false;
    }
  }
  
  public Instruction resultIs(Value result) {
    this.result = result; return this;
  }
  
  public Value result() {
    return this.result;
  }
  
  public Instruction next() {
    return next;
  }
  
  public void nextIs(Instruction _next) {
    if(_next == null) return;
    this.next = _next;
    _next.previous = this;
  }
  
  public Instruction opDom() {
    return opDom;
  }
  
  public int loadStoreSym() {
    return this.loadStoreSym;
  }
  
  public void loadStoreSymIs(int sym) {
    this.loadStoreSym = sym;
  }
  
  /**
   * 
   * @param i == 1 or 2
   * @return
   */
  public int zeroSymId(int i) {
    return zeroSymIds[i-1];
  }
  
  /**
   * 
   * @param i == 1 or 2
   * @param symId
   */
  public void zeroSymId(int i, int symId) {
    zeroSymIds[i-1] = symId;
  }
  
  public void opDom(Instruction _opDom) {
    Opcodes op1 = this.operation();
    Opcodes op2 = _opDom.operation();
    if(op1 != Opcodes.LOAD && op1 != Opcodes.STORE && op1 != op2) {
      throw new RuntimeException("OpDom Insn assignment: opcode mismatch.");
    }
    
    if((op1 == Opcodes.LOAD || op1 == Opcodes.STORE) 
        && op2 != Opcodes.LOAD && op2 != Opcodes.STORE)
      throw new RuntimeException("OpDom Insn assignment: opcode mismatch.");
    opDom = _opDom;
  }
  
  public Value operand1() {
    return operand1;
  }
  
  public void operand1Is(Value _operand) {
    operand1 = _operand;
  }
  
  public Value operand2() {
    return operand2;
  }
  
  public void operand2Is(Value _operand) {
    operand2 = _operand;
  }
 
  public Opcodes operation() {
    return operation;
  }
  
  public void operationIs(Opcodes op) {
    this.operation = op;
  }
  
  public BasicBlock ownerBlock() {
    return basicBlock;
  }
  
  public void pointTo(BasicBlock block) {
    basicBlock = block;
  }

  public Instruction previous() {
    return previous;
  }
  
  public void previousIs(Instruction _previous) {
    if(_previous == null) return;
    this.previous = _previous;
    _previous.next = this;
  }

  public void replace(Value value) {
    replacement = value;
  }
  
  public Value replacement() {
    return replacement;
  }
  
  public String toString() {
    String operand1String;
    String operand2String;
    
    if(operand1 == null) {
      operand1String = "";
    } else if(operand1.getClass() == VariableZero.class) {
      operand1String = String.valueOf(ZERO.value());
    } else if(operand1 instanceof Instruction) {
      operand1String = String.valueOf(operand1.value());
    } else {
      operand1String = operand1.toString();
    }
    
    
    if(operand2 == null) {
      operand2String = "";
    } else if(operand2.getClass() == VariableZero.class) {
      operand2String = String.valueOf(ZERO.value());
    } else if(operand2 instanceof Instruction) {
        operand2String = String.valueOf(operand2.value());
    } else {
      operand2String = operand2.toString();
    }
    
    if(operation != null)
      switch(operation) {
      case LOAD:
        operand1String = (operand2String == null || operand2String.isEmpty() || operand2String.equals("0")) 
        ? operand1String 
            : (operand1String + "." + operand2String);
        operand2String = "";
        break;
      case STORE:
        String baseString = "";
        if(result  == null || result == ZERO || result == Registers.zero()) {
          baseString = "";
        } else {
          baseString = result.toString() + ".";
        }
        operand2String = (operand2String == null || operand2String.isEmpty()) ?
                         "" : (baseString + operand2String);
        break;
      default: break;
      }
    
    
    
    String sym = "";
    if(operation == Opcodes.LOAD || operation == Opcodes.STORE) {
      sym = this.loadStoreSym == -1 ? "" 
            : ("(" + Scanner.get(null).Id2String(this.loadStoreSym) + ")");
    }
    
    String opDom = this.opDom == null ? "" : "anchor:" + this.opDom.id();
    
    StringBuffer str = new StringBuffer();
    str.append("{").append(basicBlock == null ? "" : basicBlock.id()).append("} ")
        .append(valueNumber).append(" ").append(operation).append(" ")
        .append(operand1String).append(" ").append(operand2String).append(" ");
    if(result != null && operation != Opcodes.STORE && result != Registers.zero()) {
      str.append(" -» ").append(result.toString()).append(" ");
    }
    str.append(sym);
    str.append(opDom);
    return str.toString();
  }
  
  @Override
  public int value() {
    return valueNumber;
  }

  @Override
  public void valueIs(int _value) {
    valueNumber = _value;
  }
  
  public void allocateOrSpill(ActivationRecord frame) {
    //System.err.println(this.toString());
    
    if(this == InstructionList.ZERO) return;
        
    Value operand = this.operand1;
    if(this.operation() != Opcodes.BRA && operand != null) {
      operand = this.allocateOrSpillOperand(operand, frame, Registers.SCRATCH1());
      this.operand1Is(operand);
    }
    
    operand = this.operand2();
    if(!this.isBranch() && operand != null) {
      operand = this.allocateOrSpillOperand(operand, frame, Registers.SCRATCH2());
      this.operand2Is(operand);
    }
    
    if(this.result() != null 
        && this.result().getClass() != VirtualRegister.class) {
      return;
    }
    
    VirtualRegister reg = (VirtualRegister) this.result();
    int regId = reg.value();
    if(regId <= 8 && regId >= 0) {
      this.resultIs(Registers.getRegister(regId));
    } else {
      this.resultIs(Registers.SCRATCH1());
      frame.allocateValue(regId);
      ConstantValue offset = new ConstantValue(-4 * frame.spillOffset(regId));
      Instruction[] storeSeq = 
          Instruction.store(Registers.FP(), 
                            offset, 
                            Registers.SCRATCH1(), 
                            basicBlock, 
                            Registers.SCRATCH3());
      Instruction tail = this;
      for(Instruction insn : storeSeq) {
        basicBlock.instructions().addInsnAfter(insn, tail);
        tail = insn;
      }
    }
  }
  
    private Value allocateOrSpillOperand(Value operand,
                                         ActivationRecord frame,
                                         RegisterValue result) {
      Class<? extends Value> operandClass = operand.getClass();
      if(operandClass == VirtualRegister.class) {
        int regId = operand.value();
        if(regId <= 8 && regId >= 0) {
          return Registers.getRegister(regId);
        } else {
          int offsetValue = frame.spillOffset(regId);
          ConstantValue offset = new ConstantValue(-4 * offsetValue);
          Instruction[] loadSeq =
              Instruction.load(Registers.FP(), 
                               offset, 
                               basicBlock, 
                               Registers.SCRATCH3(), 
                               result);
          for(Instruction insn : loadSeq) {
            basicBlock.instructions().addInsnBefore(insn, this);
          }
          return result;
        }
      }
      return operand;
    }
  
  public void color(HashMap<Integer, Integer> colorings) {
    if(this == InstructionList.ZERO) return;
    
    Value operand = this.operand1();
    
    if(this.operation() != Opcodes.BRA && operand != null) {
      operand = this.colorOperand(operand, colorings);
      this.operand1Is(operand);
    }
    
    operand = this.operand2();
    if(!this.isBranch() && operand != null) {
      operand = this.colorOperand(operand, colorings);
      this.operand2Is(operand);
    }
    
    
    int insnId = this.id();
    int color = colorings.get(insnId);
    VirtualRegister vr = VirtualRegister.R(color);
    if(this.result == null)
      this.resultIs(vr);
  }
  
    private Value colorOperand(Value operand, 
                               HashMap<Integer, Integer> colorings) {
      Class<? extends Value> operandClass = operand.getClass();
      if(operandClass == Instruction.class 
          || operandClass == PhiInstruction.class) {
        int insnId = ((Instruction) operand).id();
        int color = colorings.get(insnId);
        VirtualRegister vr = VirtualRegister.R(color);
        return vr;
      } else if(operandClass == VariableValue.class) {
        throw new RuntimeException("Warning! Allocating operands to Registers:\n"
            + "there should be no variable values at this stage of\n"
            + "the compilation. VariableValue: " + operand.toString() + "\n"
            + "in instruction: " + this.toString());
      }
      return operand;
    }

  /**
   * 
   * @param base
   * @param offset is word offset. this method will compute the byte offset.
   * @param block
   * @return
   */
  public static Instruction[] load(Value base, int offset, BasicBlock block) {
    return load(base, new ConstantValue(offset*4), block);
  }
  
  public static Instruction[] load(Value base, Value offset, BasicBlock block) {
//    Instruction adda = new Instruction(Opcodes.ADDA, base, offset, block); 
    Instruction load = new Instruction(Opcodes.LOAD, base, offset, block);
    return new Instruction[] {load};
  }
  
  public static Instruction[] load(Value base, 
                                   Value offset, 
                                   BasicBlock block, 
                                   RegisterValue scratch, 
                                   RegisterValue result) {
//    Instruction adda = new Instruction(Opcodes.ADDA, , , block).resultIs(scratch); 
    Instruction load = new Instruction(Opcodes.LOAD, base, offset, block).resultIs(result);
    return new Instruction[] {load};
  }
  
  public static ArrayList<Instruction> loadArray(Value base, 
                                                 int aBaseOffset, 
                                                 BasicBlock block, 
                                                 ArrayList<Value> indicies,
                                                 Symbol array) {
    ConstantValue aBaseOffsetValue = new ConstantValue(aBaseOffset*4);
    return loadArray(base,  aBaseOffsetValue, block, indicies, array);
  }
  
  public static ArrayList<Instruction> loadArray(Value base, 
                                                 Value aBaseOffset, 
                                                 BasicBlock block, 
                                                 ArrayList<Value> indicies,
                                                 Symbol array) {
    ArrayList<Instruction> insns = new ArrayList<>();
    
    checkState(array.dims().size() == indicies.size(), 
               array.dims().size() + " != " + indicies.size());
    int indexCount = indicies.size();
    ArrayList<Integer> dimensions = array.dims();
    
    Instruction abase = new Instruction(Opcodes.SUB, base, aBaseOffset, block);
    Instruction adda = abase;
    
    insns.add(abase);
    
    int offset = 1;
    for(int i = 0; i < indexCount; i += 1) {
      Value index = indicies.get(i);
      adda = loadIndexSequence(block, insns, adda, index, -4 * offset);
      int dim = dimensions.get(i);
      offset = offset * dim;
    }
    
//    Value index = indicies.get(indexCount - 1);
//    loadIndexSequence(block, insns, adda, index, -4);
    return insns;
  }

      private static Instruction loadIndexSequence(BasicBlock block,
                                            ArrayList<Instruction> insns, 
                                            Instruction adda, 
                                            Value index,
                                            int offset) {
        Value mulIndex = null;
        if(index.getClass() == VariableZero.class) {
//          varZeroIndexVarId = VariableZero.id(index);
          mulIndex = ZERO; //TODO ensure that you do not have to anything else here.
        } else {
          if(index.getClass() == ConstantValue.class) {
            if(index.value() == 0) {
              mulIndex = ZERO;
            } else {
              mulIndex = new ConstantValue(offset * index.value());
            }
          } else {
            mulIndex = 
                new Instruction(Opcodes.MUL, index, new ConstantValue(offset), block);
          }
        }
        
        adda = new Instruction(Opcodes.ADDA, mulIndex, adda, block);
        
        if(mulIndex.getClass() == Instruction.class && mulIndex != ZERO) {
          insns.add((Instruction) mulIndex);
        }
        insns.add(adda);
        return adda;
      }
  
  /**
   * 
   * @param base
   * @param offset is a word offset. the method itself will compute the byte offset.
   * @param value
   * @param block
   * @return
   */
  public static Instruction[] store(Value base, 
                                    int offset, 
                                    Value value, 
                                    BasicBlock block) {
    return store(base, new ConstantValue(offset*4), value, block);
  }
  
  public static Instruction[] store(Value base, 
                                    Value offset, 
                                    Value value, 
                                    BasicBlock block) {
    Instruction add = new Instruction(Opcodes.ADDA, base, offset, block);
    Instruction store = new Instruction(Opcodes.STORE, value, add, block);
    return new Instruction[] {add, store};
  }
  
  public static Instruction[] store(Value base, 
                                    Value offset, 
                                    Value value, 
                                    BasicBlock block,
                                    RegisterValue scratch) {
//    Instruction add = 
//        new Instruction(Opcodes.ADDA, base, offset, block).resultIs(scratch);
    Instruction store = null;
    if(value.getClass() == VariableZero.class) {
      store = new Instruction(Opcodes.STORE, ZERO, offset, block).resultIs(base);
      store.zeroSymId(1, VariableZero.id(value));
    } else {
      store = new Instruction(Opcodes.STORE, value, offset, block).resultIs(base);
    }
    return new Instruction[] {store};
  }
  
  
}