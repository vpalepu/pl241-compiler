package self.vpalepu.pl241;

import java.util.HashMap;
import java.util.Random;

//import static self.vpalepu.pl241.InstructionList.singleton;

public class AuxillaryProcs {

  public static void addInsn(Instruction newInsn) {
    newInsn.pointTo(BasicBlock.currentBlock());
    BasicBlock.currentBlock().instructions().addToLast(newInsn);
  }
  
  public static void addInsn(Instruction ... newInsns) {
    for(Instruction insn : newInsns) {
      addInsn(insn);
    }
  }
  
  public static void addInsnBefore(Instruction newInsn, Instruction before) {
    BasicBlock curBlock = BasicBlock.currentBlock(); 
    newInsn.pointTo(curBlock);
    InstructionList curList = curBlock.instructions();
    curList.addInsnBefore(newInsn, before);
  }
  
  public static void addInsnAfter(Instruction newInsn, Instruction after) {
    BasicBlock curBlock = BasicBlock.currentBlock(); 
    newInsn.pointTo(curBlock);
    InstructionList curList = curBlock.instructions();
    curList.addInsnAfter(newInsn, after);
  }
  
  private static HashMap<Integer, Integer> memory = new HashMap<>();
  public static Integer baseAddress(int varId) {
    Integer baseAddr = memory.get(varId);
    if(baseAddr == null) {
      baseAddr = new Random().nextInt();
      memory.put(varId, baseAddr);
    }
    return baseAddr;
  }
  
  
  
}
