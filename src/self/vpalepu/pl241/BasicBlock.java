package self.vpalepu.pl241;

import static self.vpalepu.pl241.InstructionList.ZERO;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;

import com.google.common.base.Function;

public class BasicBlock {
  
  public static BasicBlock createNew() {
    BasicBlock b = new BasicBlock();
    FlowGraph.current().universe().add(b);
    return b;
  }
  
  private static BasicBlock currentBlock;
  public static BasicBlock currentBlock() {
    return currentBlock;
  }
  
  public static void currentBlockIs(BasicBlock _currentBlock) {
    currentBlock = _currentBlock;
  }
  
  private final ArrayList<BasicBlock> next;
  private final int bid;
  private final ArrayList<BasicBlock> previous;
  private BasicBlock joinBlock;
  private BasicBlock immDom;
  private InstructionList instructions;
  private boolean edge;
  
  public HashSet<Integer> liveIn;
  public int visitcount = 0;
  
  public BasicBlock() {
    next = new ArrayList<BasicBlock>();
    previous = new ArrayList<BasicBlock>();
    joinBlock = null;
    immDom = null;
    instructions = InstructionList.newlist();
    bid = generateId();
    edge = false;
    liveIn = new HashSet<>();
  }
  
  private static int id = 0;
  private static int generateId() {
    return id++;
  }
  
  @Override public String toString() {
    StringBuffer str = new StringBuffer();
    str.append(this.id()).append("\n");
    
    str.append(instructions.traverseAnd(new Function<Instruction, String>() {

      @Override public String apply(Instruction arg0) {
        return arg0.toString();
      }
      
    }));
    
    if(joinBlock != null)
      str.append("ipd:").append(joinBlock.id()).append("\n");
    if(instructions.head() != ZERO)
      str.append("head:").append(instructions.head().toString()).append("\n");
    
    if(next.size() > 0) {
      str.append("next:\n");
      for(BasicBlock bb : next) {
        str.append(bb.id()).append("\n");
      }
    }
    
    if(previous.size() > 0) {
      str.append("previous:\n");
      for(BasicBlock bb : previous) {
        str.append(bb.id()).append("\n");
      }
    }
    
    return str.toString();
  }
  
  public boolean edge() {
    return edge;
  }
  
  public void edgeIs(boolean _edge) {
    edge = _edge;
  }
  
  public PhiInstruction getPhi(PhiInstruction phiInsn, Value oldValue) {
    int varId = phiInsn.varId();
    return getPhi(varId, phiInsn, oldValue);
  }
  
  public PhiInstruction getPhi(Instruction moveInsn, Value oldValue) {
    VariableValue variable = (VariableValue) moveInsn.operand2();
    int varId = variable.value();
    return getPhi(varId, moveInsn, oldValue);
  }
  
    private PhiInstruction getPhi(int varId, Instruction insn, Value oldValue) {
      for(Instruction pointer = instructions.head();
          pointer != null;
          pointer = pointer.next()) {
        if(pointer instanceof PhiInstruction 
            && ((PhiInstruction) pointer).varId() == varId) {
          PhiInstruction phi = (PhiInstruction) pointer;
          return phi;
        }
      }
      
      PhiInstruction phi = 
          new PhiInstruction(Opcodes.PHI, oldValue, oldValue, varId);
      
      this.instructions().addToFront(phi);
      phi.pointTo(this);
      return phi;
    }
  
  
  public int id() {
    return bid;
  }
  
  public BasicBlock joinBlock() {
    return joinBlock;
  }

  public ArrayList<BasicBlock> enclosingLoopHeaders(ArrayList<BasicBlock> headers) {
    ArrayList<BasicBlock> enclosing = new ArrayList<>();
    for(BasicBlock loopHeader : headers) {
      if(loopHeader.dominates(this)) {
        enclosing.add(loopHeader);
      }
    }
    return enclosing;
  }
  
  public ArrayList<BasicBlock> next() {
    return next;
  }
  
  public ArrayList<BasicBlock> previous() {
    return previous;
  }
  
  public InstructionList instructions() {
    return instructions;
  }
  
  public void addNext(BasicBlock block) {
    next.add(checkNotNull(block));
    ArrayList<BasicBlock> blockPrevious = block.previous();
    if(blockPrevious.contains(this)) return;
    blockPrevious.add(this);
  }
  
  @Deprecated public void addPrevious(BasicBlock block) {
    previous.add(block);
  }
  
  @Deprecated public void joinBlockIs(BasicBlock _joinBlock) {
    joinBlock = _joinBlock;
  }
  
  public BasicBlock immDom() {
    if(immDom == null && this.previous.size() == 1) 
      return this.previous.get(0); // TODO this is an issue if there are no other incomming edges into a loop header. 
    return immDom;
  }
  
  @Deprecated public void immDomIs(BasicBlock _immDom) {
    immDom = _immDom;
  }
  
  public void joinAndImmDomAre(BasicBlock join, BasicBlock imm) {
    joinBlock = join;
    immDom = imm;
  }
  
  public ArrayList<PhiInstruction> phis() {
    ArrayList<PhiInstruction> allPhis = new ArrayList<PhiInstruction>();
    Instruction front = this.instructions.head();
    while(front != null) {
      if(front instanceof PhiInstruction) {
        allPhis.add((PhiInstruction)front);
      }
      front = front.next();
    }
    return allPhis;
  }
  
  public void traverse(PrintStream out, ArrayList<Integer> visited) {
    if(this.next.isEmpty()) return;
    if(visited.contains(this.id())) return;
    visited.add(this.id());
    for(BasicBlock block : this.next) {
      if(block == null) continue;
      StringBuffer edgedef = new StringBuffer();
      edgedef.append("block").append(this.id())
      .append(" -> ")
      .append("block").append(block.id());
      out.println(edgedef);
      block.traverse(out, visited);
    }
  }

  public boolean isEmpty() {
    if(instructions().head() == ZERO) return true;
    return false;
  }
  
  public boolean isLoopHeader() {
    ArrayList<BasicBlock> prevs = this.previous();
    if(prevs == null || prevs.isEmpty() || prevs.size() < 2) {
      return false;
    }
    
    if(this.dominates(prevs.get(0)) || this.dominates(prevs.get(1))) {
      return true;
    }
    return false;
  }
  
  public boolean dominates(BasicBlock block) {
    for(BasicBlock iDom = block.immDom();
        iDom != null;
        iDom = iDom.immDom()) {
      if(this.equals(iDom)) {
        return true;
      }
    }
    return false;
  }
  
  public void addLive(int element) {
    liveIn.add(element);
  }
  
  public void liveIn(HashSet<Integer> _liveIn) {
    liveIn = _liveIn;
  }
  
  public HashSet<Integer> liveIn() {
    return liveIn;
  }
  
  public void removeLive(int element) {
    liveIn.remove(element);
  }

  
  public BasicBlock branch() {
    ArrayList<BasicBlock> nexts = this.next();
    if(nexts == null || nexts.isEmpty()) return null;
    return nexts.get(0);
  }
  
  public BasicBlock fail() {
    ArrayList<BasicBlock> nexts = this.next();
    if(nexts == null || nexts.isEmpty() || next.size() < 2) return null;
    return nexts.get(1);
  }
}
