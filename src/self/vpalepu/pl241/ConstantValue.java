package self.vpalepu.pl241;

import static self.vpalepu.pl241.InstructionList.ZERO;

public class ConstantValue extends Value {
  
  private int value;
  
  public ConstantValue(Integer _value) {
    value = _value;
  }

  @Override
  public int value() {
    return value;
  }

  @Override
  public void valueIs(int id) {
    value = id;
  }

  @Override 
  public Value combine(Opcodes op, Value a, BasicBlock ownerBlock) {
    if(a != null && a.getClass() == ConstantValue.class) {
      return this.combine((ConstantValue)a, op);
    }
    
    Instruction insn = new Instruction(op, this, a);
    
    if(op == Opcodes.MOVE) {
      ((VariableValue) a).setNumberedValue(insn, ownerBlock);
    } else {
      if(a != null && a.getClass() == VariableValue.class) {
        Value numValue = ((VariableValue) a).getNumberedValue(ownerBlock);
        if(numValue.getClass() == VariableZero.class) {
          insn.zeroSymId(2, VariableZero.id(numValue));
          numValue = ZERO;
        }
        insn.operand2Is(numValue);
      }
    }
    
    AuxillaryProcs.addInsn(insn);
    insn.pointTo(ownerBlock);
    //System.err.println(insn.toString());
    
    return insn;
  }
  
    private ConstantValue combine(ConstantValue constant, Opcodes op) {
      int value = 0;
      switch(op) {
      case ADD: 
        value = this.value() + constant.value();
        break;
      case SUB:
        value = this.value() - constant.value();
        break;
      case MUL:
        value = this.value() * constant.value();
        break;
      case DIV:
        value = this.value() / constant.value();
        break;
      case CMP:
        value = sign(this.value() - constant.value());
        break;
      default:
        throw new RuntimeException("Not yet implemented");
      }
      return new ConstantValue(value);
    }
  
  @Override public String toString() {
    return "#" + value;
  }
  
  private int sign(int number) {
    return number == 0 ? 0 : (number > 0 ? 1 : -1);
  }

  /*@Override
  public boolean equals(Object value) {
    if(!(value instanceof ConstantValue)) return false;
    if(this.value == this.val) {
      
    }
  }*/
  

}
