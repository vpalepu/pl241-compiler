package self.vpalepu.pl241;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class FileReader {
	private char sym;
	private BufferedReader buffer;
	
	public String currentLine;
	public int currentLineNumber = 0;

	public char getSym() {
		return this.sym;
	}
	
	public char getNextSym() {
		this.sym = getNextChar();
		return this.sym;
	}
	
	private int pointer = -1;
	private char[] line = null;
	private char getNextChar() {
	  if(pointer == -1) {
	     line = getNextLine();
	     pointer = 0;
	  }
	  
	  if(line == null)
	    return (char) -1;
	  
	  char ch = line[pointer]; 
	  pointer++;
	  if(pointer == line.length)
	    pointer = -1;
	  return ch;
	}
	
	private char[] getNextLine() {
	  String line = null;
    try {
      line = this.buffer.readLine();
      currentLineNumber += 1;
      while(line != null && (line.isEmpty() || line.matches(".*(#|//).*"))) {
        line = this.buffer.readLine().trim();
        currentLineNumber += 1;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    currentLine = line;
    if(line == null) return null;
    return (line + "\n").toCharArray();
	}
	
	public void error(String errorMsg) {
		throw new RuntimeException(errorMsg);
	}
	
	public FileReader(File file, Charset encoding) {
		
		this.sym = '\u0000';
		try {
			InputStream in = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(in, encoding);
			this.buffer = new BufferedReader(reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	// only for sanity checking the filereader.
	public static void main(String[] args) {
		FileReader fileReader = 
				new FileReader(new File("tests/baby.txt"), Charset.defaultCharset());
		char ch = fileReader.getNextSym();
		while(ch != (char) -1) {
			System.out.println(ch);
			ch = fileReader.getNextSym();
		}
	}
}
