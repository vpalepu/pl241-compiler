package self.vpalepu.pl241;

public class Relation {

  private Value value;
  private int ccCode;
  
  public Relation(Value _value, int _ccCode) {
    value = _value;
    ccCode = _ccCode;
  }
  
  public int ccCode() {
    return ccCode;
  }
  
  public void ccCodeIs(int _ccCode) {
    ccCode = _ccCode;
  }
  
  public Value value() {
    return value;
  }
}
