package self.vpalepu.pl241;

import java.util.HashMap;
import java.util.HashSet;

public class VariableZero extends Instruction {

  private int variableId;
  
  
  private static final HashMap<Integer, VariableZero> ZEROS = new HashMap<>();
  public static VariableZero get(int variableId) {
    if(!ZEROS.containsKey(variableId)) {
      VariableZero zero = new VariableZero(variableId);
      ZEROS.put(variableId, zero);
    }
    
    VariableZero zero = ZEROS.get(variableId);
    return zero;
  }
  
  private VariableZero(int variableId) {
    super(null, null, null);
    this.variableId = variableId;
  }
  
  public int variableId() {
    return this.variableId;
  }
  
  public void variableIdIs(int varId) {
    variableId = varId;
  }
  
  public static int id(Value variableZero) {
    if(variableZero == null || variableZero.getClass() != VariableZero.class) {
      throw new RuntimeException();
    }
    return ((VariableZero) variableZero).variableId;
  }
  
//  @Override
//  public String toString() {
//    return InstructionList.ZERO.toString();
//  }

}
