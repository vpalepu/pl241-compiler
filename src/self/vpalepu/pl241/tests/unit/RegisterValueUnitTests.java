package self.vpalepu.pl241.tests.unit;

import static org.junit.Assert.*;

import self.vpalepu.pl241.Registers.RegisterValue;

import org.junit.AfterClass;
import org.junit.Test;

public class RegisterValueUnitTests {

  @Test
  public void hashCodesShouldBeEqualForRegisterValueObjectsWithEqualIds() {
    //given
    RegisterValue r1 = new RegisterValue(1);
    RegisterValue r2 = new RegisterValue(1);
    
    //when
    int hash1 = r1.hashCode();
    int hash2 = r2.hashCode();
    
    //then
    assertEquals(hash1, hash2);
    
  }

  @Test
  public void registerValuesWithSameIdsShouldBeStoredInEqualObjects() {
    //given
    RegisterValue r1 = new RegisterValue(1);
    RegisterValue r2 = new RegisterValue(1);
    
    //when
    boolean r1IsEqualToR2 = r1.equals(r2);
    boolean r2IsEqualToR1 = r2.equals(r1);
    
    //then
    assertTrue(r1IsEqualToR2);
    assertTrue(r2IsEqualToR1);
  }
  
  @Test
  public void hashCodesShouldBeInEqualForRegisterValueObjectsWithInequalIds() {
    //given
    RegisterValue r1 = new RegisterValue(1);
    RegisterValue r2 = new RegisterValue(2);
    
    //when
    int hash1 = r1.hashCode();
    int hash2 = r2.hashCode();
    
    //then
    assertNotEquals(hash1, hash2);
    
  }

  @Test
  public void registerValuesWithDifferetIdsShouldBeStoredInInequalObjects() {
    //given
    RegisterValue r1 = new RegisterValue(1);
    RegisterValue r2 = new RegisterValue(2);
    
    //when
    boolean r1IsEqualToR2 = r1.equals(r2);
    boolean r2IsEqualToR1 = r2.equals(r1);
    
    //then
    assertFalse(r1IsEqualToR2);
    assertFalse(r2IsEqualToR1);
  }
  
  @Test
  public void registerValueHashCodeShouldEqualId() {
    //given
    int id = 1;
    RegisterValue r = new RegisterValue(id);
    
    //when
    int hashcode = r.hashCode();
    
    //then
    assertEquals(hashcode, id);
  }

}
