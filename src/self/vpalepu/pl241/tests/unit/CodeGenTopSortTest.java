package self.vpalepu.pl241.tests.unit;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;

import org.junit.Test;

import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.FlowGraph;
import self.vpalepu.pl241.SymbolTable.SymbolType;
import self.vpalepu.pl241.runtime.CodeGenerator;

import com.google.common.collect.HashBasedTable;

public class CodeGenTopSortTest {

  @Test(expected= NullPointerException.class)
  public void shouldThrowNPEwhenEntryIsNull() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    kodgen.topSort(null, table);
    
    //then should throw null pointer exception.
  }
  
  @Test
  public void blockListContains1ElementWhenThereIsonlyOneBlock() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    BasicBlock block = new BasicBlock();
    graph.universe().add(block);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block, table);
    
    //then
    assertEquals(1, blocks.size());
  }

  
  @Test
  public void blockListConatainsOnlyEntryBlockForTwoDisjointBlocks() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block1, table);
    
    //then
    assertEquals(1, blocks.size());
    assertThat(blocks, hasItem(block1));
  }
  
  @Test
  public void blockListContainsTwoSortedBlocksForTwoLinearBlocksInthatOrder() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    block1.addNext(block2);
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block1, table);
    
    //then
    assertEquals("Block# is not 2", 2, blocks.size());
    assertTrue("Block1 came after Block2", blocks.indexOf(block1) < blocks.indexOf(block2));
  }
  
  @Test
  public void blockListContainsThreeSortedBlocksForThreeLinearBlocksInthatOrder() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    block1.addNext(block2);
    block2.addNext(block3);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block1, table);
    
    //then
    assertEquals("Edge# is not 3", 3, blocks.size());
    assertEquals("Block1 index should be 0", 0, blocks.indexOf(block1));
    assertEquals("Block2 index should be 1", 1, blocks.indexOf(block2));
    assertEquals("Block3 index should be 2", 2, blocks.indexOf(block3));
    
  }
  
  @Test
  public void blockListContainsThreeSortedBlocksForThreeForkConnectedBlocksWithOrder_1_2_3() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    block1.addNext(block2);
    block1.addNext(block3);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block1, table);
    
    //then
    assertEquals("Edge# is not 3", 3, blocks.size());
    assertEquals("Block1 index should be 0", 0, blocks.indexOf(block1));
    assertEquals("Block2 index should be 1", 1, blocks.indexOf(block2));
    assertEquals("Block3 index should be 2", 2, blocks.indexOf(block3));
  }
  
  @Test
  public void blockListContainsFourSortedBlocksForFourDiamondConnectedBlocksWithOrder_1_2_3_4() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    BasicBlock block4 = new BasicBlock();
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    graph.universe().add(block4);
    
    block1.addNext(block2);
    block1.addNext(block3);
    block2.addNext(block4);
    block3.addNext(block4);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block1, table);
    
    //then
    assertEquals("Edge# is not 4", 4, blocks.size());
    assertEquals("Block1 index should be 0", 0, blocks.indexOf(block1));
    assertEquals("Block2 index should be 1", 1, blocks.indexOf(block2));
    assertEquals("Block3 index should be 2", 2, blocks.indexOf(block3));
    assertEquals("Block3 index should be 3", 3, blocks.indexOf(block4));
  }
  
  @Test
  
  
  public void blockListContainsFourSortedBlocksForFourLoopConnectedBlocksWithOrder_0_1_2_3() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    BasicBlock block0 = new BasicBlock();
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    graph.universe().add(block0);
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    
    block0.addNext(block1);
    block1.joinAndImmDomAre(block0.joinBlock(), block0);
    block1.addNext(block2);
    block2.joinAndImmDomAre(block1, block1);
    block2.addNext(block1);
    block1.addNext(block3);
    block3.joinAndImmDomAre(block1.joinBlock(), block1);
    
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //when
    ArrayList<BasicBlock> blocks = kodgen.topSort(block0, table);
    
    //then
    assertEquals("Edge# is not 4", 4, blocks.size());
    assertEquals("Block1 index should be 0", 0, blocks.indexOf(block0));
    assertEquals("Block2 index should be 1", 1, blocks.indexOf(block1));
    assertEquals("Block3 index should be 2", 2, blocks.indexOf(block2));
    assertEquals("Block3 index should be 3", 3, blocks.indexOf(block3));
  }
}
