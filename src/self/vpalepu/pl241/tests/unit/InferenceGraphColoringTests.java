package self.vpalepu.pl241.tests.unit;


import static org.junit.Assert.*;
import static self.vpalepu.pl241.InterferenceGraph.FIRST_COLOR;
import static self.vpalepu.pl241.InterferenceGraph.NULL_NODE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Test;

import self.vpalepu.pl241.InterferenceGraph;

public class InferenceGraphColoringTests {


  @Test
  public void singleNodeShouldBeColoredWith1() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node = 1;
    graph.addEdge(node, NULL_NODE);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertEquals(FIRST_COLOR, (int) colorings.get(node));
  }

  @Test
  public void twoDisjointNodesShouldBeColoredWith1() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    graph.addNode(node1);
    graph.addNode(node2);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertEquals(FIRST_COLOR, (int) colorings.get(node1));
    assertEquals(FIRST_COLOR, (int) colorings.get(node2));
  }

  @Test
  public void twoConnectedNodesShouldBeColoredDifferently() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    graph.addEdge(node1, node2);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertNotEquals((int) colorings.get(node1), (int) colorings.get(node2));
  }
  
  @Test
  public void twoConnectedNodesShouldProduceTwoColors() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    graph.addEdge(node1, node2);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertEquals(2, new HashSet<>(colorings.values()).size());
  }
  
  @Test
  public void threeNodesAndTwoEdgesShouldProduceTwoColors() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    int node3 = 3;
    graph.addEdge(node1, node2);
    graph.addEdge(node3, node2);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertEquals(2, new HashSet<>(colorings.values()).size());
  }
  
  @Test
  public void threeDisjoinNodesShouldProduceColor1() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    int node3 = 3;
    graph.addNode(node1);
    graph.addNode(node2);
    graph.addNode(node3);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertEquals(1, new HashSet<>(colorings.values()).size());
    assertEquals(FIRST_COLOR, (int) colorings.get(node1));
  }
  
  @Test
  public void threeConnectedNodesWithTwoEdgesAndFourthDisjointNodeShouldProduceTwoColors() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    int node3 = 3;
    int node4 = 4;
    graph.addEdge(node1, node2);
    graph.addEdge(node3, node2);
    graph.addNode(node4);

    // when
    HashMap<Integer, Integer> colorings = graph.color();

    // then
    assertEquals(2, new HashSet<>(colorings.values()).size());
  }

  @Test
  public void singleNodeShouldBe1Colorable() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    graph.addEdge(node1, NULL_NODE);
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    
    //then
    assertEquals(1, colorings.keySet().size());
    assertEquals(1, new HashSet<>(colorings.values()).size());
    assertEquals(1, (int) colorings.get(node1));
  }
  
  @Test
  public void singleNodeColoringWith1ColorShouldHaveNoSpills() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    graph.addEdge(node1, NULL_NODE);
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    ArrayList<Integer> spills = InterferenceGraph.getSpills(colorings);
    
    //then
    assertEquals(0, spills.size());
  }
  
  @Test
  public void twoDisjointNodesShouldBe1Colorable() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    graph.addNode(node1);
    graph.addNode(node2);
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    
    //then
    assertEquals(2, colorings.keySet().size());
    assertEquals(1, new HashSet<>(colorings.values()).size());
    assertEquals(1, (int) colorings.get(node1));
    assertEquals(1, (int) colorings.get(node2));
  }
  
  @Test
  public void twoDisjointNodesColoringWith1ColorShouldHaveNoSpills() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    graph.addNode(node1);
    graph.addNode(node2);
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    ArrayList<Integer> spills = InterferenceGraph.getSpills(colorings);
    
    //then
    assertEquals(0, spills.size());
  }
  
  @Test
  public void tenDisjointNodesShouldBe1Colorable() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int nodecount = 10;
    for(int node = 1; node <= nodecount; node += 1) {
      graph.addNode(node);
    }
    
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    
    //then
    assertEquals(nodecount, colorings.keySet().size());
    assertEquals(1, new HashSet<>(colorings.values()).size());
    for(int node = 1; node <= nodecount; node += 1) {
      assertEquals(1, (int) colorings.get(node));
    }
  }
  
  @Test
  public void tenDisjointNodesColoringWith1ColorShouldHaveNoSpills() {
    // given
    InterferenceGraph graph = InterferenceGraph.create();
    int nodecount = 10;
    for(int node = 1; node <= nodecount; node += 1) {
      graph.addNode(node);
    }
    
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    ArrayList<Integer> spills = InterferenceGraph.getSpills(colorings);
    
    //then
    assertEquals(0, spills.size());
  }
  
  @Test
  public void twoConnectedNodesColoringWith1ColorShouldHave1Spill() {
    //given
    int node1 = 1;
    int node2 = 2;
    InterferenceGraph graph = InterferenceGraph.create();
    graph.addEdge(node1, node2);
    
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    ArrayList<Integer> spills = InterferenceGraph.getSpills(colorings);
    
    //then
    assertEquals(1, spills.size());
  }
  
  @Test
  public void colorsInTwoConnectedNodesColoredWith1RegisterShouldHaveColorsGreaterThanRegisterCount() {
    //given
    int node1 = 1;
    int node2 = 2;
    InterferenceGraph graph = InterferenceGraph.create();
    graph.addEdge(node1, node2);
    
    int registerCount = 1;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    ArrayList<Integer> spills = InterferenceGraph.getSpills(colorings);
    
    //then
    assertFalse(spills.isEmpty());
    assertTrue("colors# is less than or equal to register# despite spills", 
               new HashSet<>(colorings.values()).size() > registerCount);
  }
  
  @Test
  public void twoConnectedNodesShouldBe2Colorable() {
    //given
    int node1 = 1;
    int node2 = 2;
    InterferenceGraph graph = InterferenceGraph.create();
    graph.addEdge(node1, node2);
    
    int registerCount = 2;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    
    //then
    assertEquals(2, new HashSet<>(colorings.values()).size());
    assertEquals(graph.nodesCopy().size(), colorings.keySet().size());
    assertNotEquals((int) colorings.get(node1), (int) colorings.get(node2));
  }
  
  @Test
  public void twoConnectedNodesColoringWith2ColorsShouldHaveNoSpills() {
    //given
    int node1 = 1;
    int node2 = 2;
    InterferenceGraph graph = InterferenceGraph.create();
    graph.addEdge(node1, node2);
    
    int registerCount = 2;
    
    //when
    HashMap<Integer, Integer> colorings = graph.color(new ArrayList<Integer>(), registerCount);
    ArrayList<Integer> spills = InterferenceGraph.getSpills(colorings);
    
    //then
    assertEquals(0, spills.size());
  }
  
}
