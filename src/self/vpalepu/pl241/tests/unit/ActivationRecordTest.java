package self.vpalepu.pl241.tests.unit;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import self.vpalepu.pl241.SymbolTable.Symbol;
import self.vpalepu.pl241.runtime.ActivationRecord;

public class ActivationRecordTest {

  @Test
  public void returnAddrOffsetShouldBe0WhenThereAreNoParamters() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    
    //when
    int returnAddrLocation = record.returnAddrOffset();
    
    //then
    assertEquals(0, returnAddrLocation);
    
  }
  
  @Test
  public void callerSPOffestShouldBe1WhenThereAreNoParameters() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    
    //when
    int callerSPLocation = record.callerFPOffset();
    
    //then
    assertEquals(1, callerSPLocation);
  }
  
  @Test
  public void returnAddressOffsetShouldbe10WhenThereAre10Paramters() {
  //given
    int paramCount = 10;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    
    //when
    int returnAddrOffset = record.returnAddrOffset();
    
    //then
    assertEquals(10, returnAddrOffset);
  }
  
  @Test
  public void callerSPOffsetShouldbe11WhenThereAre9Paramters() {
  //given
    int paramCount = 10;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    
    //when
    int callerSPoffset = record.callerFPOffset();
    
    //then
    assertEquals(11, callerSPoffset);
  }

  @Test
  public void sizeShouldBe2With_0Params_And_0Vars() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    
    //when
    int size = record.size();
    
    //then
    assertEquals(2, size);
  }
  
  @Test
  public void sizeShouldBe12With_0Params_And_1ArrayOfSize10() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    record.allocateArray(1, 10);
    
    //when
    int size = record.size();
    
    //then
    assertEquals(12, size);
  }
  
  @Test
  public void sizeShouldBe13With_0Params_1ArrayOfSize10_1Var() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    record.allocateArray(1, 10);
    record.allocateValue(2);
    
    //when
    int size = record.size();
    
    //then
    assertEquals(13, size);
  }
  
  @Test
  public void allocatingTwoVarWithSameValueIDShouldNotChangeSize() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    
    //when
    record.allocateValue(1);
    int size1 = record.size(); 
    record.allocateValue(1);
    int size2 = record.size();
    
    //then
    assertEquals(3, size1);
    assertEquals(size1, size2);
  }
  
  @Test
  public void firstVarShouldBeAllocatedAtOffset2WhenParamCountIs0() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    int valueId = 1;
    record.allocateValue(valueId);
    
    //when
    int offset1 = record.offset(valueId);
    
    //then
    assertEquals(2, offset1);
  }
  
  @Test 
  public void secondAllocatedVarAfter10elementArrayAllocationShouldBeAvailableAtOffset12() {
    //given
    int paramCount = 0;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    int arrayId1 = 1;
    int arraylen1 = 10;
    record.allocateArray(arrayId1, arraylen1);
    int valueId2 = 2;
    record.allocateValue(valueId2);
    
    //when
    int offset2 = record.offset(valueId2);
    
    //then
    assertEquals(12, offset2);
  }
  
  @Test 
  public void secondAllocatedVarAfter10elementArrayAllocationWith10ParamsShouldBeAvailableAtOffset22() {
    //given
    int paramCount = 10;
    ActivationRecord record = ActivationRecord.create(new ArrayList<Symbol>(paramCount));
    int arrayId1 = 1;
    int arraylen1 = 10;
    record.allocateArray(arrayId1, arraylen1);
    int valueId2 = 2;
    record.allocateValue(valueId2);
    
    //when
    int offset2 = record.offset(valueId2);
    
    //then
    assertEquals(22, offset2);
  }
}
