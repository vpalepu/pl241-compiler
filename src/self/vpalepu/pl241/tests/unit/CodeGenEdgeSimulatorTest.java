package self.vpalepu.pl241.tests.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.common.collect.HashBasedTable;

import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.FlowGraph;
import self.vpalepu.pl241.SymbolTable.SymbolType;
import self.vpalepu.pl241.runtime.CodeGenerator;

public class CodeGenEdgeSimulatorTest {

  @Test
  public void simulateAllEdgesForEmptyFlowGraphShouldProduceEmptyTable() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertTrue(table.isEmpty());
  }
  
  @Test
  public void simulateAllEdgesForSingleBlockShouldProduceEmptyTable() {
    //given
    BasicBlock block = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block);
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertTrue(table.isEmpty());
  }
  
  @Test
  public void simulateAllEdgesForTwoDisjointBlocksShouldProduceEmptyTable() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertTrue(table.isEmpty());
  }
  
  @Test
  public void simulateAllEdgesForTwoConnectedBlocksShouldProduceOneForwardEdge() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    block1.addNext(block2);
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertEquals("Edge# is not 1", 1, table.size());
    assertFalse("Edge is not a forward edge", table.get(block1.id(), block2.id()));
  }
  
  @Test
  public void simulateAllEdgesForThreeLinearlyConnectedBlocksShouldProduceTwoForwardEdges() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    block1.addNext(block2);
    block2.addNext(block3);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertEquals("Edge# is not 2", 2, table.size());
    assertFalse("Edge is not a forward edge", table.get(block1.id(), block2.id()));
    assertFalse("Edge is not a forward edge", table.get(block2.id(), block3.id()));
  }
  
  @Test
  public void simulateAllEdgesForThreeForkConnectedBlocksShouldProduceTwoForwardEdges() {
    //given
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    block1.addNext(block2);
    block1.addNext(block3);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertEquals("Edge# is not 2", 2, table.size());
    assertFalse("Edge is not a forward edge", table.get(block1.id(), block2.id()));
    assertFalse("Edge is not a forward edge", table.get(block1.id(), block3.id()));
  }
  
  @Test
  public void simulateAllEdgesForFourDiamondConnectedBlocksShouldProduceFourForwardEdges() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    BasicBlock block4 = new BasicBlock();
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    graph.universe().add(block4);
    
    block1.addNext(block2);
    block1.addNext(block3);
    block2.addNext(block4);
    block3.addNext(block4);
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertEquals("Edge# is not 2", 4, table.size());
    assertFalse("Edge is not a forward edge", table.get(block1.id(), block2.id()));
    assertFalse("Edge is not a forward edge", table.get(block1.id(), block3.id()));
    assertFalse("Edge is not a forward edge", table.get(block2.id(), block4.id()));
    assertFalse("Edge is not a forward edge", table.get(block3.id(), block4.id()));
  }
  
  @Test
  public void simulateAllEdgesForFourLoopConnectedBlocksShouldProduceThreeForwardEdgesAndOneBackEdge() {
    //given
    FlowGraph graph = FlowGraph.create("dummy", SymbolType.PROC);
    BasicBlock block0 = new BasicBlock();
    BasicBlock block1 = new BasicBlock();
    BasicBlock block2 = new BasicBlock();
    BasicBlock block3 = new BasicBlock();
    graph.universe().add(block0);
    graph.universe().add(block1);
    graph.universe().add(block2);
    graph.universe().add(block3);
    
    block0.addNext(block1);
    block1.joinAndImmDomAre(block0.joinBlock(), block0);
    block1.addNext(block2);
    block2.joinAndImmDomAre(block1, block1);
    block2.addNext(block1);
    block1.addNext(block3);
    block3.joinAndImmDomAre(block1.joinBlock(), block1);
    
    
    CodeGenerator kodgen = CodeGenerator.create(null);
    
    //when
    HashBasedTable<Integer, Integer, Boolean> table = kodgen.simulateAllEdges(graph);
    
    //then
    assertEquals("Edge# is not 4", 4, table.size());
    assertFalse("Edge is expected as forward", table.get(block0.id(), block1.id()));
    assertFalse("Edge is expected as forward", table.get(block1.id(), block2.id()));
    assertFalse("Edge is expected as forward", table.get(block1.id(), block3.id()));
    assertTrue("Edge is expected as backward", table.get(block2.id(), block1.id()));
  }
  
}
