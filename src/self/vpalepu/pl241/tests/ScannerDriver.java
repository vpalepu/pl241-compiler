package self.vpalepu.pl241.tests;

import java.io.File;

import self.vpalepu.pl241.Scanner;

public class ScannerDriver {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(new File("tests/baby.txt"));
		scanner.next();
		int token = scanner.getSym();
		while(token != Scanner.eofToken && token != Scanner.periodToken) {
				switch(token) {
				case Scanner.number:
					System.out.println(token + " " + scanner.latestVal);
					break;
				case Scanner.ident:
					System.out.println(token + " " + scanner.Id2String(scanner.latestId) + " " + scanner.latestId);
					break;
				case Scanner.errorToken: break;
				default:
					System.out.println(token + " " + scanner.Id2String(token));
				}
				scanner.next();
				token = scanner.getSym();
		}
		System.out.println("this killed it:" + token);
	}
}
