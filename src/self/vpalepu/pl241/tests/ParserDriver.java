package self.vpalepu.pl241.tests;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.FlowGraph;
import self.vpalepu.pl241.Parser;
import self.vpalepu.pl241.Scanner;
import self.vpalepu.pl241.io.DotGraphs;
import self.vpalepu.pl241.io.DotPrinter;
import self.vpalepu.pl241.runtime.CodeGenerator;
import self.vpalepu.pl241.runtime.DLX;

public class ParserDriver {
  public static final boolean DEBUG = false;
  
	public static void main(String[] args) throws IOException {

		Parser parser = new Parser(Scanner.get(new File(args[1])));

		Parser.debug = false;
		parser.computation();
//		System.err.println("parser computation complete");
		
		DotGraphs graphs = new DotGraphs(args[0]);
		
		DotPrinter p1 = new DotPrinter(graphs.pass1());
		DotPrinter p2 = new DotPrinter(graphs.cpCse());
		DotPrinter p3 = new DotPrinter(graphs.color());
		DotPrinter p4 = new DotPrinter(graphs.spill());
		DotPrinter p5 = new DotPrinter(graphs.linkd());
		
		p1.emitStart();
		for(FlowGraph g : FlowGraph.grandUniverse) {
		  p1.emitDotFile(g);
    }
		p1.emitStop();
		
		p2.emitStart();
    for(FlowGraph g : FlowGraph.grandUniverse) {
      g.buildDomTree();
      g.anchor();
      g.eliminateRedundancy1();
      g.anchor();
      g.eliminateRedundancy2();
      p2.emitDotFile(g);
    }
    p2.emitStop();
		
    p3.emitStart();
    for(FlowGraph g : FlowGraph.grandUniverse) {
      g.allocateVirtualRegisters();
      p3.emitDotFile(g);
    }
    p3.emitStop();
    
    DotPrinter.emitStart(p4, p5);
		for(FlowGraph g : FlowGraph.grandUniverse) {
	    g.eliminatePhis2();
	    g.insertSpillCode();
	    p4.emitDotFile(g);
	    
	    g.expandMus();
	    g.eliminateNops();
	    p5.emitDotFile(g);
		}
		DotPrinter.emitStop(p4, p5);
		
		CodeGenerator kodgen = CodeGenerator.create(FlowGraph.grandUniverse);
		int[] code = kodgen.gen().toIntArray();
		kodgen.print();
		//DLX.load(code);
		//DLX.execute();
	}
} 
