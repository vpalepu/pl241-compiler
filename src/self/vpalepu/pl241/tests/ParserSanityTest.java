package self.vpalepu.pl241.tests;

import java.io.File;

import org.junit.Test;

import self.vpalepu.pl241.Parser;
import self.vpalepu.pl241.Scanner;

public class ParserSanityTest {
	
	@Test
	public void test001() {
		Parser parser = new Parser(new Scanner(new File("tests/test001.txt")));
		run(parser);
	}

	@Test
	public void test002() {
		Parser parser = new Parser(new Scanner(new File("tests/test002.txt")));
		run(parser);
	}

	@Test
	public void test003() {
		Parser parser = new Parser(new Scanner(new File("tests/test003.txt")));
		run(parser);
	}

	@Test
	public void test004() {
		Parser parser = new Parser(new Scanner(new File("tests/test004.txt")));
		run(parser);
	}

	@Test
	public void test005() {
		Parser parser = new Parser(new Scanner(new File("tests/test005.txt")));
		run(parser);
	}

	@Test
	public void test006() {
		Parser parser = new Parser(new Scanner(new File("tests/test006.txt")));
		run(parser);
	}

	@Test
	public void test007() {
		Parser parser = new Parser(new Scanner(new File("tests/test007.txt")));
		run(parser);
	}

	@Test
	public void test008() {
		Parser parser = new Parser(new Scanner(new File("tests/test008.txt")));
		run(parser);
	}

	@Test
	public void test009() {
		Parser parser = new Parser(new Scanner(new File("tests/test009.txt")));
		run(parser);
	}

	@Test
	public void test010() {
		Parser parser = new Parser(new Scanner(new File("tests/test010.txt")));
		run(parser);
	}

	@Test
	public void test011() {
		Parser parser = new Parser(new Scanner(new File("tests/test011.txt")));
		run(parser);
	}

	@Test
	public void test012() {
		Parser parser = new Parser(new Scanner(new File("tests/test012.txt")));
		run(parser);
	}

	@Test
	public void test013() {
		Parser parser = new Parser(new Scanner(new File("tests/test013.txt")));
		run(parser);
	}

	@Test
	public void test014() {
		Parser parser = new Parser(new Scanner(new File("tests/test014.txt")));
		run(parser);
	}

	@Test
	public void test015() {
		Parser parser = new Parser(new Scanner(new File("tests/test015.txt")));
		run(parser);
	}

	@Test
	public void test016() {
		Parser parser = new Parser(new Scanner(new File("tests/test016.txt")));
		run(parser);
	}

	@Test
	public void test017() {
		Parser parser = new Parser(new Scanner(new File("tests/test017.txt")));
		run(parser);
	}

	@Test
	public void test018() {
		Parser parser = new Parser(new Scanner(new File("tests/test018.txt")));
		run(parser);
	}

	@Test
	public void test019() {
		Parser parser = new Parser(new Scanner(new File("tests/test019.txt")));
		run(parser);
	}

	@Test
	public void test020() {
		Parser parser = new Parser(new Scanner(new File("tests/test020.txt")));
		run(parser);
	}

	@Test
	public void test021() {
		Parser parser = new Parser(new Scanner(new File("tests/test021.txt")));
		run(parser);
	}

	@Test
	public void test022() {
		Parser parser = new Parser(new Scanner(new File("tests/test022.txt")));
		run(parser);
	}

	@Test
	public void test023() {
		Parser parser = new Parser(new Scanner(new File("tests/test023.txt")));
		run(parser);
	}

	@Test
	public void test024() {
		Parser parser = new Parser(new Scanner(new File("tests/test024.txt")));
		run(parser);
	}

	@Test
	public void test025() {
		Parser parser = new Parser(new Scanner(new File("tests/test025.txt")));
		run(parser);
	}

	@Test
	public void test026() {
		Parser parser = new Parser(new Scanner(new File("tests/test026.txt")));
		run(parser);
	}

	@Test
	public void test027() {
		Parser parser = new Parser(new Scanner(new File("tests/test027.txt")));
		run(parser);
	}

	@Test
	public void test028() {
		Parser parser = new Parser(new Scanner(new File("tests/test028.txt")));
		run(parser);
	}

	@Test
	public void test029() {
		Parser parser = new Parser(new Scanner(new File("tests/test029.txt")));
		run(parser);
	}

	@Test
	public void test030() {
		Parser parser = new Parser(new Scanner(new File("tests/test030.txt")));
		run(parser);
	}

	@Test
	public void test031() {
		Parser parser = new Parser(new Scanner(new File("tests/test031.txt")));
		run(parser);
	}
	
	private static void run(Parser parser) {
		Parser.debug = false;
		parser.computation();
	}
}
