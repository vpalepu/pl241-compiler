package self.vpalepu.pl241.tests;

import java.util.HashMap;

import self.vpalepu.pl241.InterferenceGraph;
import self.vpalepu.pl241.viz.InferenceVisualizer;
import self.vpalepu.pl241.viz.InferenceVisualizer.ShellSettings;

public class InferenceVisualizerDriver {

  public static void main(String[] args) {
    InterferenceGraph graph = InterferenceGraph.create();
    int node1 = 1;
    int node2 = 2;
    int node3 = 3;
    int node4 = 4;
    graph.addEdge(node1, node2);
    graph.addEdge(node3, node2);
    graph.addEdge(node4, node1);
    
    HashMap<Integer, Integer> colorings = graph.color();
    
    InferenceVisualizer.create(graph, new ShellSettings("graph", 400, 400))
                       .buildViz(colorings, null).visualize();

  }

}
