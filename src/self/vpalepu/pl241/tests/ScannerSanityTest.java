package self.vpalepu.pl241.tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import self.vpalepu.pl241.Scanner;

public class ScannerSanityTest {

	@Test
	public void test001() {
		Scanner scanner = new Scanner(new File("tests/test001.txt"));
		run(scanner);
	}

	@Test
	public void test002() {
		Scanner scanner = new Scanner(new File("tests/test002.txt"));
		run(scanner);
	}

	@Test
	public void test003() {
		Scanner scanner = new Scanner(new File("tests/test003.txt"));
		run(scanner);
	}

	@Test
	public void test004() {
		Scanner scanner = new Scanner(new File("tests/test004.txt"));
		run(scanner);
	}

	@Test
	public void test005() {
		Scanner scanner = new Scanner(new File("tests/test005.txt"));
		run(scanner);
	}

	@Test
	public void test006() {
		Scanner scanner = new Scanner(new File("tests/test006.txt"));
		run(scanner);
	}

	@Test
	public void test007() {
		Scanner scanner = new Scanner(new File("tests/test007.txt"));
		run(scanner);
	}

	@Test
	public void test008() {
		Scanner scanner = new Scanner(new File("tests/test008.txt"));
		run(scanner);
	}

	@Test
	public void test009() {
		Scanner scanner = new Scanner(new File("tests/test009.txt"));
		run(scanner);
	}

	@Test
	public void test010() {
		Scanner scanner = new Scanner(new File("tests/test010.txt"));
		run(scanner);
	}

	@Test
	public void test011() {
		Scanner scanner = new Scanner(new File("tests/test011.txt"));
		run(scanner);
	}

	@Test
	public void test012() {
		Scanner scanner = new Scanner(new File("tests/test012.txt"));
		run(scanner);
	}

	@Test
	public void test013() {
		Scanner scanner = new Scanner(new File("tests/test013.txt"));
		run(scanner);
	}

	@Test
	public void test014() {
		Scanner scanner = new Scanner(new File("tests/test014.txt"));
		run(scanner);
	}

	@Test
	public void test015() {
		Scanner scanner = new Scanner(new File("tests/test015.txt"));
		run(scanner);
	}

	@Test
	public void test016() {
		Scanner scanner = new Scanner(new File("tests/test016.txt"));
		run(scanner);
	}

	@Test
	public void test017() {
		Scanner scanner = new Scanner(new File("tests/test017.txt"));
		run(scanner);
	}

	@Test
	public void test018() {
		Scanner scanner = new Scanner(new File("tests/test018.txt"));
		run(scanner);
	}

	@Test
	public void test019() {
		Scanner scanner = new Scanner(new File("tests/test019.txt"));
		run(scanner);
	}

	@Test
	public void test020() {
		Scanner scanner = new Scanner(new File("tests/test020.txt"));
		run(scanner);
	}

	@Test
	public void test021() {
		Scanner scanner = new Scanner(new File("tests/test021.txt"));
		run(scanner);
	}

	@Test
	public void test022() {
		Scanner scanner = new Scanner(new File("tests/test022.txt"));
		run(scanner);
	}

	@Test
	public void test023() {
		Scanner scanner = new Scanner(new File("tests/test023.txt"));
		run(scanner);
	}

	@Test
	public void test024() {
		Scanner scanner = new Scanner(new File("tests/test024.txt"));
		run(scanner);
	}

	@Test
	public void test025() {
		Scanner scanner = new Scanner(new File("tests/test025.txt"));
		run(scanner);
	}

	@Test
	public void test026() {
		Scanner scanner = new Scanner(new File("tests/test026.txt"));
		run(scanner);
	}

	@Test
	public void test027() {
		Scanner scanner = new Scanner(new File("tests/test027.txt"));
		run(scanner);
	}

	@Test
	public void test028() {
		Scanner scanner = new Scanner(new File("tests/test028.txt"));
		run(scanner);
	}

	@Test
	public void test029() {
		Scanner scanner = new Scanner(new File("tests/test029.txt"));
		run(scanner);
	}

	@Test
	public void test030() {
		Scanner scanner = new Scanner(new File("tests/test030.txt"));
		run(scanner);
	}

	@Test
	public void test031() {
		Scanner scanner = new Scanner(new File("tests/test031.txt"));
		run(scanner);
	}
	
	private static void run(Scanner scanner) {
		scanner.next();
		int token = scanner.getSym(); 
		while(token != Scanner.eofToken
				&& token != Scanner.periodToken) {
				System.out.println(scanner.printToken());
				scanner.next();
				token = scanner.getSym();
		}
		System.out.println("this killed it:" + token);
		assertEquals(Scanner.periodToken, token);
	}

}
