package self.vpalepu.pl241.viz;

import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Visualizer {

  public static void main(String[] args) {
    
    Display d = new Display();
    Shell shell = new Shell(d);
    shell.setText("Hello, World");
    shell.setLayout(new FillLayout());
    shell.setSize(400, 400);

    Graph g = new Graph(shell, SWT.NONE);
    GraphNode hello = new GraphNode(g, SWT.NONE, "Hello");
    GraphNode world = new GraphNode(g, SWT.NONE, "World");
    
    new GraphConnection(g, SWT.COLOR_BLUE + SWT.COLOR_CYAN, hello, world);
    
    g.setLayoutAlgorithm(new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
    
    shell.open();
    while (!shell.isDisposed()) {
      while (!d.readAndDispatch()) {
        d.sleep();
      }
    }
  }
  
}
