package self.vpalepu.pl241.viz;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;

import self.vpalepu.pl241.InterferenceGraph;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

public final class InferenceVisualizer {
  private final InterferenceGraph graph;
  private final Graph view; 
  
  public static InferenceVisualizer create(InterferenceGraph graph,
                                           ShellSettings settings) {
    Display d = new Display();
    Shell shell = new Shell(d);
    shell.setText(settings.title());
    shell.setLayout(new FillLayout());
    shell.setSize(settings.length(), settings.width());
    
    return new InferenceVisualizer(graph, new Graph(shell, SWT.NONE)); 
  }
  
  public InferenceVisualizer(InterferenceGraph graph, Graph view) {
    this.graph = graph;
    this.view = view;
    this.view.setConnectionStyle(ZestStyles.CONNECTIONS_SOLID);
  }
  
  public InferenceVisualizer buildViz() {
    HashMap<Integer, GraphNode> nodes = new HashMap<>(); 
    for(int node : this.graph.nodesCopy()) {
      GraphNode n = new GraphNode(view, SWT.NONE, String.valueOf(node));
      System.out.println(n.getBackgroundColor().toString());
      n.setBackgroundColor(new Color(this.view.getParent().getDisplay(), 0, 0, 0));
      n.setForegroundColor(new Color(this.view.getParent().getDisplay(), 132, 13, 13));
      nodes.put(node, n);
    }
    
    for(Entry<Integer, Integer> entry : this.graph.edges()) {
      GraphNode x = nodes.get(entry.getKey());
      GraphNode y = nodes.get(entry.getValue());
      new GraphConnection(this.view, SWT.NONE, x, y);
    }
    
    return this;
  }
  
  private final static RGB[] colorMap = { new RGB(0, 0, 0),
      new RGB(200, 0, 0),
      new RGB(0, 200, 0),
      new RGB(0, 0, 200),
      new RGB(200, 200, 0),
      new RGB(0, 200, 200),
      new RGB(200, 0, 200),
      new RGB(250, 250, 0),
      new RGB(0, 250, 250),
      new RGB(250, 0, 250)};
  
  public InferenceVisualizer buildViz(HashMap<Integer, Integer> colorings,
                                      HashMap<Integer, String> insnNames) {
    HashMap<Integer, GraphNode> nodes = new HashMap<>();
    final HashMap<String, Color> insnColors = new HashMap<>(); 
    
    for(int node : this.graph.nodesCopy()) {
      int color = colorings.get(node);
      String insnName = insnNames.get(node);
      GraphNode n = new GraphNode(view, SWT.NONE, insnNames.get(node) 
                                                  + " color: " 
                                                  + color);
      Color foreColor = new Color(this.view.getParent().getDisplay(), colorMap[color]);
      insnColors.put(insnName, foreColor);
      n.setBackgroundColor(foreColor);
      n.setForegroundColor(new Color(this.view.getParent().getDisplay(), 250, 250, 250));
      nodes.put(node, n);
    }
    
    for(Entry<Integer, Integer> entry : this.graph.edges()) {
      GraphNode x = nodes.get(entry.getKey());
      GraphNode y = nodes.get(entry.getValue());
      new GraphConnection(this.view, SWT.NONE, x, y);
    }
    
    this.view.addSelectionListener(new SelectionAdapter() {

      public void widgetSelected(SelectionEvent e) {
        Graph view = (Graph) e.widget;
        Color select = new Color(view.getParent().getDisplay(), new RGB(50, 50, 50));
        Color neigh = new Color(view.getParent().getDisplay(), new RGB(100, 100, 100));
        Color fade = new Color(view.getParent().getDisplay(), new RGB(250, 250, 250));
        @SuppressWarnings("rawtypes") List selections = view.getSelection();
        
        if(selections == null || selections.isEmpty()) {
          restoreColors(view);
        }
        
        for(Object selection : selections) {
          if(selection.getClass() == GraphNode.class) {
            GraphNode node = (GraphNode) selection;
            System.out.println(node.toString());
            HashSet<GraphNode> neighbours = new HashSet<>();
            
            for(Object target : node.getTargetConnections()) {
              GraphConnection conn = (GraphConnection) target;
              neighbours.add(conn.getDestination());
              neighbours.add(conn.getSource());
            }
            
            for(Object target : node.getSourceConnections()) {
              GraphConnection conn = (GraphConnection) target;
              neighbours.add(conn.getDestination());
              neighbours.add(conn.getSource());
            }
            neighbours.remove(node);
            for(GraphNode ne : neighbours) {
              System.out.println("\t" + ne.toString());
            }
            
            for(Object anyNode : view.getNodes()) {
              GraphNode n = (GraphNode) anyNode;
              if(n == node) {
                n.setBackgroundColor(select);
                n.setHighlightColor(select);
              } else if(neighbours.contains(n)) {
                n.setBackgroundColor(neigh);
              } else {
                n.setBackgroundColor(fade);
              }
            }
            
            for(Object ed : view.getConnections()) {
              GraphConnection edge = (GraphConnection) ed;
              edge.setLineColor(fade);
            }
            
          }
        }
      }
      
        private void restoreColors(Graph graph) {
          for(Object node : graph.getNodes()) {
            GraphNode n = (GraphNode) node;
            String text = n.getText();
            int colorCode = Integer.parseInt(text.split("color: ")[1]);    
            Color color = insnColors.get(text);
            if(color == null) color = new Color(graph.getParent().getDisplay(), 
                                                colorMap[colorCode]);
            
            n.setBackgroundColor(color);
          }
          
          Color fade = new Color(graph.getParent().getDisplay(), new RGB(200, 200, 200));
          for(Object edge : graph.getConnections()) {
            GraphConnection ed = (GraphConnection) edge;
            
            ed.setLineColor(fade);
          }
        }

    });
    
    return this;
  }
  
  public void visualize() {
    RadialLayoutAlgorithm l = 
        new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
    view.setLayoutAlgorithm(l, true);
    Shell shell = (Shell) this.view.getParent();
    shell.open();
    Display d = shell.getDisplay();
    while (!shell.isDisposed()) {
      while (!d.readAndDispatch()) {
        d.sleep();
      }
    }
  }
  

  
  public static class ShellSettings {
    private final String title;
    private final int length;
    private final int width;
    
    public ShellSettings(String title, int len, int wid) {
      this.title = title;
      this.length = len;
      this.width = wid;
    }

    /**
     * @return the title
     */
    public String title() {
      return title;
    }

    /**
     * @return the length
     */
    public int length() {
      return length;
    }

    /**
     * @return the width
     */
    public int width() {
      return width;
    }
    
    
  }
}
