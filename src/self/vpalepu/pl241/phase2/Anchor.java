package self.vpalepu.pl241.phase2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.Instruction;
import self.vpalepu.pl241.Opcodes;

public class Anchor {
  
  private static HashMap<Opcodes, Instruction> ANCHOR;
  
  static {
    reset();
  }
  
  public static void reset() {
    ANCHOR = new HashMap<>();
    ANCHOR.put(Opcodes.NEG, null);
    ANCHOR.put(Opcodes.ADD, null);
    ANCHOR.put(Opcodes.SUB, null);
    ANCHOR.put(Opcodes.MUL, null);
    ANCHOR.put(Opcodes.DIV, null);
    ANCHOR.put(Opcodes.CMP, null);
    ANCHOR.put(Opcodes.ADDA, null);
    ANCHOR.put(Opcodes.LOAD, null);
    ANCHOR.put(Opcodes.STORE, null);
    ANCHOR.put(Opcodes.MOVE, null);
    ANCHOR.put(Opcodes.PHI, null);
    ANCHOR.put(Opcodes.END, null);
    ANCHOR.put(Opcodes.BRA, null);
    ANCHOR.put(Opcodes.BNE, null);
    ANCHOR.put(Opcodes.BEQ, null);
    ANCHOR.put(Opcodes.BLE, null);
    ANCHOR.put(Opcodes.BLT, null);
    ANCHOR.put(Opcodes.BGE, null);
    ANCHOR.put(Opcodes.BGT, null);
    ANCHOR.put(Opcodes.READ, null);
    ANCHOR.put(Opcodes.WRITE, null);
    ANCHOR.put(Opcodes.WLN, null);
    ANCHOR.put(Opcodes.MU, null);
    ANCHOR.put(Opcodes.NOP, null);
  }
  
  public static String stringify() {
    StringBuffer buffer = new StringBuffer();
    for(Entry<Opcodes, Instruction> entry : ANCHOR.entrySet()) {
      Instruction headInsn = entry.getValue();
      if(headInsn == null) continue;
      buffer.append(entry.getKey())
          .append("::");
      Instruction pointer = headInsn;
      while(pointer != null) {
        buffer.append("\n")
            .append(pointer.toString());
        pointer = pointer.opDom();
      }
      buffer.append("\n\n");
    }
    return buffer.toString();
  }
  
  public static void anchor(BasicBlock block) {
    for(Instruction pointer = block.instructions().head();
        pointer != null;
        pointer = pointer.next()) {
      setHeadInsn(pointer.operation(), pointer);
    }
  }
  
  public static Instruction getHeadInsn(Opcodes opcode) {
    Instruction headInsn = ANCHOR.get(opcode);
    if(headInsn == null) {
      throw new RuntimeException("Anchor for " + opcode + " not setup yet.");
    }
    return headInsn;
  }
  
  public static void setHeadInsn(Opcodes _opcode, Instruction _insn) {
    if(_opcode == Opcodes.STORE)
        _opcode = Opcodes.LOAD;
    Instruction headInsn = ANCHOR.get(_opcode);
    if(headInsn != null) _insn.opDom(headInsn);
    ANCHOR.put(_opcode, _insn);
    if(_opcode == Opcodes.LOAD)
      ANCHOR.put(Opcodes.STORE, _insn);
  }
  
  public static boolean find(Instruction _insn) {
    Instruction headInsn = ANCHOR.get(_insn.operation());
    while(headInsn != null) {
      if(headInsn == _insn) return true;
      headInsn = headInsn.opDom();
    }
    return false;
  }
  
  public static void reset(HashSet<Integer> elimination) {
    for(Opcodes opcode : ANCHOR.keySet()) {
      reset(opcode, elimination);
    }
  }
  
  private static void reset(Opcodes _opcode, HashSet<Integer> elimination) {
    Instruction headInsn = ANCHOR.get(_opcode);
    if(headInsn == null) return;
    if(_opcode == null) return;
    while(headInsn != null) {
      if(!elimination.contains(headInsn.ownerBlock().id()))
        break;
      headInsn = headInsn.opDom();
    }
    ANCHOR.put(_opcode, headInsn);
  }
}
