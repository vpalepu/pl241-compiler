package self.vpalepu.pl241.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import self.vpalepu.pl241.BasicBlock;
import self.vpalepu.pl241.FlowGraph;
import self.vpalepu.pl241.Instruction;

import com.google.common.base.Function;

public class DotPrinter {
  private final PrintStream out;
  
  public DotPrinter(PrintStream out) {
    this.out = out;
  }
  
  public DotPrinter(File file) {
    try {
      this.out = new PrintStream(file);
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
  
  public static void emitStart(DotPrinter ... printers) {
    for(DotPrinter printer : printers) {
      printer.emitStart();
    }
  }
  
  public static void emitStop(DotPrinter ... printers) {
    for(DotPrinter printer : printers) {
      printer.emitStop();
    }
  }
  
  public void emitStart() {
    out.println("digraph struct {\n");
  }
  
  public void emitStop() {
    out.println("\n}");
  }
  
  public void emitDotFile(FlowGraph flow) {
    ArrayList<BasicBlock> universe = flow.universe();
    out.println("subgraph cluster_" + flow.name() 
        + " { label = \"" + flow.name()
        + "\";\n node [margin=0.75 shape=record]");
    for(BasicBlock block : universe) {
      StringBuffer blockdef = new StringBuffer();
      blockdef.append("block").append(block.id());
      blockdef.append("[label =\"{");
      
      String nodeDefs = block.instructions().
          traverseAnd(new Function<Instruction, String>() {
        @Override
        public String apply(Instruction arg0) {
          StringBuffer nodedef = new StringBuffer();
          nodedef.append("<i").append(arg0.id()).append(">");
          nodedef.append(arg0.toString().replaceAll("\\{", "\\\\{").replaceAll("\\}", "\\\\}"));
          nodedef.append("\\l");
          if(arg0.next() != null)
            nodedef.append("|");
          return nodedef.toString();
        }
      });
      blockdef.append(nodeDefs);
      
      blockdef.append("}\"]\n");
      
      out.println(blockdef);
    }
    
    if(universe.size() == 1) {
      out.println("\n}");
      return;
    }
    
    out.println("\nedge [color=green];\n");
    for(BasicBlock block : universe) {
      BasicBlock immDom = block.immDom();
      if(immDom == null) continue;
      StringBuffer domTreeEdge = new StringBuffer();
      domTreeEdge.append("block").append(block.id())
          .append(" -> ")
          .append("block").append(immDom.id());
      out.println(domTreeEdge.toString());
    }

    out.println("\nedge [color=blue];\n");

    for(BasicBlock block : universe) {
      BasicBlock joinBlock = block.joinBlock();
      if(joinBlock == null) continue;
      StringBuffer domTreeEdge = new StringBuffer();
      domTreeEdge.append("block").append(block.id())
          .append(" -> ")
          .append("block").append(joinBlock.id());
      out.println(domTreeEdge.toString());
    }
  
    out.println("\nedge [color=black];\n");
    ArrayList<Integer> visited = new ArrayList<Integer>();
    for(BasicBlock block : universe) {
      if(visited.contains(block.id())) continue;
      block.traverse(out, visited);      
    }
    out.println("\n}");
  }
  
}
