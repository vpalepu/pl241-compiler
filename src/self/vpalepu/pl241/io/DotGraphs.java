package self.vpalepu.pl241.io;

import java.io.File;

public class DotGraphs {
  
  public DotGraphs(String root) {
    this.pass1 = new File(root + "pass1.dot");
    this.cpCse = new File(root + "cpCse.dot");
    this.color = new File(root + "color.dot");
    this.spill = new File(root + "spill.dot");
    this.linkd = new File(root + "linkd.dot");
  }
  
  
  private final File pass1;
  private final File cpCse;
  private final File color;
  private final File spill;
  private final File linkd;
  
  
  public File pass1() {
    return pass1;
  }
  
  public File cpCse() {
    return cpCse;
  }
  
  public File color() {
    return color;
  }
  
  public File spill() {
    return spill;
  }
  
  public File linkd() {
    return linkd;
  }
  
  
}
