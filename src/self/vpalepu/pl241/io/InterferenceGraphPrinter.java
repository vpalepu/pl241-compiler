package self.vpalepu.pl241.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Map.Entry;

import self.vpalepu.pl241.InterferenceGraph;

public class InterferenceGraphPrinter {
  private final PrintStream out;
  
  public InterferenceGraphPrinter(PrintStream _out) {
    out = _out;
  }
  
  public InterferenceGraphPrinter(File file) {
    try {
      this.out = new PrintStream(file);
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
  
  public void emitStart() {
    out.println("graph struct {\n");
  }
  
  public void emitStop() {
    out.println("\n}");
  }
  
  public void emitDotFile(InterferenceGraph iGraph, String flowName) {
    out.println("subgraph cluster_" + flowName 
        + " { label = \"" + flowName
        + "\";\n node [margin=0.75 shape=record]");
    
    ArrayList<Integer> nodes = iGraph.nodesCopy();
    
    for(int node : nodes) {
      out.println(node);
    }
    
    
    
    for(Entry<Integer, Integer> edge : iGraph.edges()) {
      StringBuffer edgeLine = new StringBuffer();
      edgeLine.append(edge.getKey())
              .append(" -- ")
              .append(edge.getValue());
      
      out.println(edgeLine);
    }
    
    out.println("\n}");
  }
  
}
