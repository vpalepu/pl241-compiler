package self.vpalepu.pl241;

import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;

import self.vpalepu.pl241.Registers.RegisterValue;

public class SymbolTable {
  
  public static final String MAIN = "main";
  
  private static SymbolTable table;
  
  public static SymbolTable init() {
    if(table == null) table = new SymbolTable();
    return table;
  }
  
  public static SymbolTable symbolTable() {
    return checkNotNull(table);
  }
  
  private ArrayList<Symbol> symbols;
  
  private SymbolTable() {
    symbols = new ArrayList<>();
  }
  
  public void add(Symbol symbol) {
    if(this.conflicts(checkNotNull(symbol).ident(), 
                      symbol.type(), 
                      symbol.scope())) {
      Parser.throwCompilationError("multiple declarations of:" 
                                    + symbol.toString());
    }
    symbols.add(checkNotNull(symbol));
  }
  
  public Symbol get(String ident, SymbolType type, String scope) {
    for(Symbol s : symbols) {
      if(s.type() == type 
          && s.ident().equals(ident)
          && s.scope().equals(scope)) {
        return s;
      }
    }
    return null;
  }
  
  public Symbol get(int id, String ident, SymbolType type, String scope) {
    for(Symbol s : symbols) {
      if(s.id() == id) {
        checkState(s.type() == type 
            && s.ident().equals(ident)
            && s.scope().equals(scope),
            "symbol data mismatch.");
        return s;
      }
    }
    return null;
  }
  
  public Symbol get(int id) {
    for(Symbol s : symbols) {
      if(s.id() == id) {
        return s;
      }
    }
    return null;
  }
  
  public Symbol fetch(String possibleScope, String desigName, SymbolType type) { 
    Symbol symbol;
    
    if(contains(desigName, type, possibleScope)) {
      symbol = get(desigName, type, possibleScope);
      symbol.isUsed(true);
    } else if(contains(desigName, type, SymbolTable.MAIN)) { 
      symbol = table.get(desigName, type, SymbolTable.MAIN);
      symbol.isUsed(true);
    } else {
      symbol = null;
    }
    
    return symbol;
  }
  
  public boolean contains(String ident, SymbolType type, String scope) {
    return get(ident, type, scope) != null;
  }
  
  public boolean conflicts(String ident, SymbolType type, String scope) {
    SymbolType altType = null;
    switch(type) {
    case ARR:
      altType = SymbolType.VAR;
      break;
    case FUNC:
      altType = SymbolType.PROC;
      break;
    case PROC:
      altType = SymbolType.FUNC;
      break;
    case VAR:
      altType = SymbolType.ARR;
      break;
    default:
      throw new RuntimeException("method: SymbolTable/conflicts. error: unknown symbol type.");
    }
    
    boolean conflicts = contains(ident, type, scope) 
                        || contains(ident, altType, scope);
    return conflicts;
  }
  
  
  
  public static class Symbol {
    public static final int VARTYPE = 0;
    public static final int ARRTYPE = 1;
    
    private final int id;
    private final String ident;
    private final SymbolType type;
    private final ArrayList<Integer> dimensions;
    private final ArrayList<Symbol> params;
    private final Symbol ret;
    private final String scope;
    private boolean used;
    private final boolean isParam;
    private int runtimeOffset;
    private RegisterValue baseAddress;
    
    public static Symbol variable(int id, String ident, String scope, boolean isParam) {
      return new Symbol(id, checkNotNull(ident), 
                        SymbolType.VAR, 
                        null, 
                        null, 
                        null, 
                        checkNotNull(scope),
                        isParam);
    }
    
    public static Symbol array(int id, String ident, String scope, int ... dimensions) {
      ArrayList<Integer> dims = new ArrayList<>();
      for(int d : dimensions) {
        dims.add(d);
      }
      return new Symbol(id, checkNotNull(ident), 
                        SymbolType.ARR, 
                        dims, 
                        null, 
                        null, 
                        checkNotNull(scope),
                        false);
    }
    
    public static Symbol func(int id, String ident, Symbol ret, Symbol ... params) {
      ArrayList<Symbol> parameters = new ArrayList<>();
      for(Symbol p : params) {
        if(p == null) 
          throw new RuntimeException("func Symbol. non-null param required.");
        parameters.add(p);
      }
      return new Symbol(id, checkNotNull(ident), 
                        SymbolType.FUNC, 
                        null, 
                        parameters, 
                        ret, 
                        MAIN,
                        false);
    }
    
    public static Symbol proc(int id, String ident, Symbol ... params) {
      ArrayList<Symbol> parameters = new ArrayList<>();
      for(Symbol p : params) {
        if(p == null) 
          throw new RuntimeException("proc Symbol. non-null param required.");
        parameters.add(p);
      }
      return new Symbol(id, checkNotNull(ident), 
                        SymbolType.FUNC, 
                        null, 
                        parameters, 
                        null, 
                        MAIN,
                        false);
    }
    
    private Symbol(int id,
                   String ident, 
                   SymbolType type, 
                   ArrayList<Integer> dimensions, 
                   ArrayList<Symbol> params,
                   Symbol ret,
                   String scope,
                   boolean isParam) {
      this.id = id;
      this.ident  = ident;
      this.type = type;
      this.dimensions = dimensions;
      this.params = params;
      this.ret = ret; 
      this.scope = scope;
      this.used = false;
      this.baseAddress = Registers.zero();
      this.runtimeOffset = 0;
      this.isParam = isParam;
    }
    
    public int id() {
      return id;
    }
    
    public String ident() {
      return ident;
    }
    
    public SymbolType type() {
      return type;
    }
    
    public boolean isParam() {
      return isParam;
    }
    
    public ArrayList<Integer> dims() {
      if(type != SymbolType.ARR) {
        throw new RuntimeException("Why do you need this?");
      }
      return dimensions;
    }
    
    public int length() {
      checkState(type == SymbolType.ARR);
      int length = 1;
      for(int dim : dimensions) {
        length *= dim;
      }
      return length;
    }
    
    public ArrayList<Symbol> params() {
      if(type != SymbolType.FUNC || type != SymbolType.PROC) {
        throw new RuntimeException("Why do you need this?");
      }
      return params;
    }
    
    public Symbol ret() {
      if(type != SymbolType.FUNC) {
        throw new RuntimeException("why do you need this?");
      }
      return ret;
    }
    
    public String scope() {
      return scope;
    }
    
    public boolean isUsed() {
      return used;
    }
    
    public Symbol isUsed(boolean used) {
      this.used = used; return this;
    }
    
    public boolean isGlobal() {
      if(SymbolTable.MAIN.equals(this.scope)) {
        return true;
      }
      return false;
    }
    
    public int runtimeOffset() {
      return this.runtimeOffset;
    }
    
    public Symbol runtimeOffsetIs(int offset) {
      this.runtimeOffset = offset; return this;
    }
    
    public RegisterValue baseAddress() {
      return this.baseAddress;
    }
    
    public Symbol baseAddressIs(RegisterValue base) {
      this.baseAddress = base; return this;
    }
    
    @Override
    public String toString() {
      StringBuffer buffer = new StringBuffer();
      buffer.append(this.type).append(" ")
            .append(this.ident).append("@")
            .append(this.scope);
      return buffer.toString();
    }
  }
  
  public static enum SymbolType {
    FUNC, PROC, VAR, ARR
  }
}
