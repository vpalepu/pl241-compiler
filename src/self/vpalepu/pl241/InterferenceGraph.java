package self.vpalepu.pl241;

import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;


import com.google.common.collect.HashMultimap;

public class InterferenceGraph {
  private final HashMultimap<Integer, Integer> graph;
  private final HashSet<Integer> nodes;
  private final HashMultimap<Integer, Integer> clusters;
  private final HashMap<Integer, Integer> phiClusterMap;
  
  public static final boolean NODE_IS_NULL(int node) {
     if(node < 0) {
       return true;
     }
     return false;
  }
  
  public static final int NULL_NODE = -1;
  public static final int FIRST_COLOR = 1;  
  
  public static InterferenceGraph create() {
    return new InterferenceGraph();
  }
  
  private InterferenceGraph() {
    graph = HashMultimap.create();
    nodes = new HashSet<>();
    clusters = HashMultimap.create();
    phiClusterMap = new HashMap<>();
  }
  
  public void addEdge(int node1, int node2) {
    if(NODE_IS_NULL(node1))
      throw new RuntimeException(node1 + "!?! Really? "
          + "You've the gall to send a null node?");
    
    nodes.add(node1);
    if(NODE_IS_NULL(node2)) return;
    
    if(node1 == node2) return; 
    nodes.add(node2);
    graph.put(node1, node2);
    graph.put(node2, node1);
  }
  
  public void addNode(int node) {
    nodes.add(node);
  }

  public boolean containsEdge(int node1, int node2) {
    if(!graph.containsKey(node1) 
        || !getAdjNodes(node1).contains(node2)) {
      return false;
    }
    return true;
  }
  
  public boolean containsNode(int node) {
    if(!nodes.contains(node)) 
      return false;
    return true;
  }
  
  public Set<Integer> getAdjNodes(int node) {
    return graph.get(node);
  }
  
  public boolean isEdgeUndirected(int node1, int node2) {
    boolean to = getAdjNodes(node1).contains(node2);
    if(!to) return false;
    boolean fro = getAdjNodes(node2).contains(node1);
    if(!fro) return false;
    return true;
  }

  public ArrayList<Integer> nodesCopy() {
    return new ArrayList<>(this.nodes);
  }
  
  public Iterable<Entry<Integer,Integer>> edges() {
    return this.graph.entries();
  }
  
  public void removeNode(int node) {
    nodes.remove(node);
    ArrayList<Integer> adjNodes = new ArrayList<>(graph.get(node));
    for(int adj : adjNodes) {
      graph.remove(node, adj);
      graph.remove(adj, node);
    }
  }
  
  public InterferenceGraph build(FlowGraph flowgraph) {
    BasicBlock entry = flowgraph.entryBlock();
    
    ArrayList<BasicBlock> loopHeaders = new ArrayList<>();
    for(BasicBlock b: flowgraph.universe()) {
      if(b.isLoopHeader())
        loopHeaders.add(b);
    }
    
    calcLiveRange(entry, null, 1, loopHeaders);
    calcLiveRange(entry, null, 2, loopHeaders);
    return this;
  }
  
    private HashSet<Integer> calcLiveRange(BasicBlock b, 
                                           BasicBlock from, 
                                           int passCount,
                                           ArrayList<BasicBlock> loopHeaders) {
      if(b == null) return new HashSet<>();
      
//      System.err.println("basic block:" + b.id());
      
      HashSet<Integer> live;
      if(b.visitcount >= passCount) {
        live = b.liveIn();
      } else {
        b.visitcount += 1;
        if(b.visitcount == 2) {
          ArrayList<BasicBlock> headers = b.enclosingLoopHeaders(loopHeaders);
          for(BasicBlock h : headers) {
            b.liveIn().addAll(h.liveIn()); // TODO use the API in BasicBlock to add elements.
          }
        }
        
        live = new HashSet<>();
        HashSet<Integer> s1, s2;
        BasicBlock branchBlock = b.branch();
        if(branchBlock != null) {
          s1 = calcLiveRange(branchBlock, b, passCount, loopHeaders);
          live.addAll(s1);
        }
        
        BasicBlock failBlock = b.fail();
        if(failBlock != null) {
          s2 = calcLiveRange(failBlock, b, passCount, loopHeaders);
          live.addAll(s2);
        }
        
        Instruction last = b.instructions().last();
        while(last != null) {
          if(last.getClass() == PhiInstruction.class) {
            last = last.previous();
            continue;
          }
          
          
          int lastId = last.id();
//          System.err.println(live);
//          System.err.println(last.toString());
          live.remove(lastId);
          
          
          if(live.isEmpty()) {
            this.addNode(lastId);
          }
          
          for(int living : live) {
            this.addEdge(lastId, living);
//            System.err.println(lastId + " --- " + living + "@site1");
          }
          
          Value[] operands = new Value[] {last.operand1(), last.operand2()};
          for(Value opd : operands) {
            if(opd == null 
                || (opd.getClass() != Instruction.class 
                && opd.getClass() != PhiInstruction.class)) 
              continue;
            live.add(opd.value());
          }
          
//          System.err.println(live);
          
          last = last.previous();
        }
        
        b.liveIn(live);
      }
      
      Instruction last = b.instructions().last();
      while(last != null) {
        if(last.getClass() != PhiInstruction.class) {
          last = last.previous();
          continue;
        }
        
        int lastId = last.id();
//        System.err.println(live);
//        System.err.println(last.toString());
        live.remove(lastId);
        
        
        if(live.isEmpty()) {
          this.addNode(lastId);
        }
        
        for(int living : live) {
          this.addEdge(lastId, living);
//          System.err.println(lastId + " --- " + living + "@site2");
        }
        
        if(last.operand1() == null || last.operand2() == null) {
          throw new RuntimeException("PhiInsn as a null operand: " + last.toString());
        }
        
        if(from == ((PhiInstruction)last).blockA() 
            && (last.operand1().getClass() == Instruction.class 
                || last.operand1().getClass() == PhiInstruction.class)) {
          live.add(last.operand1().value());
        }
        
        if(from == ((PhiInstruction)last).blockB() 
            && (last.operand2().getClass() == Instruction.class
                || last.operand2().getClass() == PhiInstruction.class)) {
          live.add(last.operand2().value());
        }
        
//        System.err.println(live);
        
//        Value[] operands = new Value[] {last.operand1(), last.operand2()};
//        for(Value opd : operands) {
//          if(opd.getClass() != Instruction.class) 
//            continue;
//          live.add(opd.value());
//        }
        
        last = last.previous();
      }
      
      return live;
    }
  
  public InterferenceGraph coalesce(FlowGraph flowgraph) {
//    System.err.println(this);
    int maxNodeId = Collections.max(nodes) + 1;
    for(BasicBlock block : flowgraph.universe()) {
      for(PhiInstruction phi : block.phis()) {
        int phiId = phi.id();
        int clusterId = - 1;
        for(int clusterKey : clusters.keySet()) {
          Set<Integer> cluster = clusters.get(clusterKey);
          if(cluster.contains(phiId)) {
            clusterId = clusterKey;
            break;
          }
        }
        
        if(clusterId == -1) {
          clusterId = maxNodeId;
          addNode(clusterId);
          phiClusterMap.put(phiId, clusterId);
          addToCluster(clusterId, phiId);
        }
        
        Value[] operands = new Value[] { phi.operand1(), phi.operand2() };
        for(Value operand : operands) {
          Class<?> operandClass = operand.getClass();
          if(operandClass != Instruction.class 
              && operandClass != PhiInstruction.class) {
            continue;
          }
          
          int operandId = operand.value();
          if(!interferesWithCluster(clusterId, operandId)) {
            addToCluster(clusterId, operandId);
            removeNode(operandId);
          }
        }
        
        removeNode(phiId);
        maxNodeId += 1;
      }
    }
//    System.err.println(this);
    return this;
  }
  
    private boolean interferesWithCluster(int clusterId, int nodeId) {
      if(!clusters.containsKey(clusterId)) {
        return false;
      }
      
      Set<Integer> interferingNodeIds = getAdjNodes(clusterId);
      if(interferingNodeIds.contains(nodeId)) {
        return true;
      }
//      for(int interferingNodeId : interferingNodeIds) {
//        if(containsEdge(nodeId, interferingNodeId)) 
//          return true;
//      }
      return false;
    }
    
    private void addToCluster(int clusterId, int nodeId) {
      
      clusters.put(clusterId, nodeId);
      Set<Integer> adjnodeIds = getAdjNodes(nodeId);
      if(phiClusterMap.containsKey(nodeId)) {
        int nodeIdcluster = phiClusterMap.get(nodeId);
        adjnodeIds.addAll(getAdjNodes(nodeIdcluster));
      }
      for(int adjnodeId : adjnodeIds) {
        addEdge(clusterId, adjnodeId);
      }
      
    }
    
    
  public HashMap<Integer, Integer> color(ArrayList<Integer> zeroColored, 
                                         int regcount) {
    int maxColor = regcount;
    HashMap<Integer, Integer> neighborCounts = this.neighborCount();
    
    Stack<Integer> nodeStack = new Stack<>();
    
    for(int i = 0; i < this.nodes.size(); i += 1) {
      int node = getNextNode2(regcount, neighborCounts);
      nodeStack.push(node);
      
      Set<Integer> neighbors = this.graph.get(node);
      for(int neighbor : neighbors) {
        if(nodeStack.contains(neighbor)) {
          continue;
        }
        int neighbour2count = neighborCounts.get(neighbor);
        checkState(neighbour2count != 0);
        neighborCounts.put(neighbor, neighbour2count - 1);
      }
      neighborCounts.remove(node);
    }
    
    HashMap<Integer, Integer> coloring = color(maxColor, nodeStack, zeroColored);
    
    HashMap<Integer, Integer> clusterColoring = new HashMap<>();
    Set<Integer> nodes = coloring.keySet();
    for(int nodeId : nodes) {
      if(clusters.containsKey(nodeId)) {
        Set<Integer> members = clusters.get(nodeId);
        for(int member : members) {
          clusterColoring.put(member, coloring.get(nodeId));
        }
      }
    }
    
    for(int clusterMemberId : clusterColoring.keySet()) {
      coloring.put(clusterMemberId, clusterColoring.get(clusterMemberId));
    }
    
    coloring.put(0, 0);
    
    return coloring;
  }
  
    private int getNextNode(int regcount,
                            HashMap<Integer, Integer> neighborCounts) {
      
//      int node = getNodeWithLessThanXNeighbours(regcount, neighborCounts);
      
      
      for(int n : neighborCounts.keySet()) {
        int count = neighborCounts.get(n);
        if(count < regcount) {
          return n;
        }
      }
      
      int node = -1;
      if(node == -1) {
        if(!neighborCounts.isEmpty()) {
//          int min = Collections.max(neighborCounts.values());
          int max = -1;
          for(int n : neighborCounts.keySet()) {
            int neighborCount = neighborCounts.get(n);
            if(max <= neighborCount) {
              max = neighborCount;
              node = n;
            }
//            if(min >= neighborCount) {
//              min = neighborCount;
//              node = n;
//            }
          }
//          node = neighborCounts.keySet().iterator().next();
        } else {
          throw new RuntimeException("This is why you became a programmer "
              + "did you not? To write code that is not supposed to "
              + "be reached at all, but somehow still does. When the fuck will "
              + "you learn that programming is not about wishes? It is about "
              + "cold logic.");
        }
      }
      return node;
    }
    
        private int getNodeWithLessThanXNeighbours(int x, 
            HashMap<Integer, Integer> neighborCount) {
          for(int node : neighborCount.keySet()) {
            int count = neighborCount.get(node);
            if(count < x) {
              return node;
            }
          }
          return -1;
        }
        
    private int getNextNode2(int regcount,
        HashMap<Integer, Integer> neighborCounts) {

      for(int n : neighborCounts.keySet()) {
        int count = neighborCounts.get(n);
        if(count < regcount) {
          return n;
        }
      }

      int node = -1;
      checkState(!neighborCounts.isEmpty(), "how neighbourCounts table is empty.");

      int maxNeighbors = -1;
      for(int n : neighborCounts.keySet()) {
        int neighborCount = neighborCounts.get(n);
        if(maxNeighbors <= neighborCount) {
          maxNeighbors = neighborCount;
          node = n;
        }
      }

      return node;
    }
  
    private HashMap<Integer, Integer> color(int maxColor, 
                                            Stack<Integer> nodeStack,
                                            ArrayList<Integer> zeroColored) {
      HashMap<Integer, Integer> coloring = new HashMap<>();
      InterferenceGraph rebuild = InterferenceGraph.create();
      while(!nodeStack.isEmpty()) {
        int node = nodeStack.pop();
        rebuild.addNode(node);
        HashSet<Integer> usedColors = new HashSet<>();
        
        for(int originalAdj : this.graph.get(node)) {
          if(!rebuild.containsNode(originalAdj)) continue;
          checkState(coloring.containsKey(originalAdj), originalAdj);
          int takenColor = coloring.get(originalAdj);
          usedColors.add(takenColor);
          rebuild.addEdge(node, originalAdj);
        }
        
        int color;
        if(zeroColored.contains(node)) {
          color = 0;
        } else if(node == 0) {
          color = 0;
        } else {
          color = getLeastPossibleColor(usedColors, maxColor);
          color = getLeastUnusedColor(usedColors);
        }

        coloring.put(node, color);
      }
      return coloring;
    }
  
        private int getLeastPossibleColor(HashSet<Integer> takenColors, 
                                          int maxColor) {
          for(int color = 1; color <= maxColor; color += 1) {
            if(takenColors.contains(color)) {
              continue;
            }
            return color;
          }
          return maxColor + 1;
        }
        
        private int getLeastUnusedColor(HashSet<Integer> usedColors) {
          int color = 1;
          
          while(usedColors.contains(color)) {
            color += 1;
          }
          
          return color;
        }
  
  public static ArrayList<Integer> getSpills(HashMap<Integer, Integer> colorings) {
    ArrayList<Integer> spills = new ArrayList<>();
    for(int insnId : colorings.keySet()) {
      int color = colorings.get(insnId);
      if(color == -1 && !spills.contains(insnId)) {
        spills.add(color);
      }
    }
    return spills;
  }      
        
  public HashMap<Integer, Integer> neighborCount() {
    HashMap<Integer, Integer> neighborCount = new HashMap<>();
    for(int node : this.nodes) {
      neighborCount.put(node, graph.get(node).size());
    }
    return neighborCount;
  }
    
  public HashMap<Integer, Integer> color() {
    int maxColor = FIRST_COLOR - 1;
    
    ArrayList<Integer> all = new ArrayList<>(graph.keySet());
    HashMap<Integer, Integer> colorings = new HashMap<>();
    HashSet<Integer> colors = new HashSet<>();
    
    for(int i = 0; i < all.size(); i += 1) {
      int node = all.get(i);
      
      Set<Integer> adjNodes = graph.get(node);
      
      ArrayList<Integer> adjColors = new ArrayList<>(); 
      for(int adjNode : adjNodes) {
        Integer color = colorings.get(adjNode);
        if(color == null) continue;
        adjColors.add(color);
      }
      ArrayList<Integer> candidateColors = new ArrayList<>();
      
      for(int color : colors) {
        if(adjColors.contains(color)) {
          continue;
        }
        candidateColors.add(color);
      }
      
      int color = candidateColors.isEmpty() ? ++maxColor 
                                            : Collections.min(candidateColors);
      colorings.put(node, color);
      colors.add(maxColor);
    }
    
    for(int node : this.nodes) {
      if(all.contains(node)) {
        checkState(colorings.keySet().contains(node));
        continue;
      }
      checkState(!colorings.keySet().contains(node));
      colorings.put(node, FIRST_COLOR);
    }
    
    return colorings;
  }
  
  @Override
  public String toString() {
    StringBuffer buff = new StringBuffer();
    for(int node : nodes) {
      buff.append(node).append("=").append(this.graph.get(node)).append("\n");
    }
    
    buff.append("clusters:\n");
    
    for(int node : clusters.keySet()) {
      buff.append(node).append("=").append(this.clusters.get(node)).append("\n");
    }
    
    return buff.toString();
  }

//  public void visualize(ArrayList<BasicBlock> blocks, 
//                        HashMap<Integer, Integer> colorings) {
//    HashMap<Integer, String> insnNames = new HashMap<>();
//    for(BasicBlock b : blocks) {
//      if(b.isEmpty()) continue;
//      for(Instruction insn = b.instructions().head();
//          insn != null;
//          insn = insn.next()) {
//        insnNames.put(insn.id(), insn.toString());
//      }
//    }
//    InferenceVisualizer
//    .create(this, new ShellSettings("graph", 1200, 1200))
//    .buildViz(colorings, insnNames)
//    .visualize();
//  }

}
